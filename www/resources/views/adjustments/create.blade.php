@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Add Adjustments for Payroll #{{$payroll_id}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('payrolls.index') }}">All Payrolls</a>
                </li>
                <li>
                    <a href="{{ route('payrolls.show',$payroll_id) }}">Payroll #{{$payroll_id}}</a>
                </li>
                <li class="active">
                    <strong>Add Adjustments</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content">
                @include('layouts.error')
                {!! Form::open(array('route' => 'adjustments.store','method'=>'POST')) !!}
                <input type="hidden" value="{{$payroll_id}}" name="payroll_id">
                <input type="hidden" value="{{$user_id}}" name="user_id">
                <input type="hidden" value="{{\App\PayrollAdjustment::$status_pending}}" name="status">
                <div class="row">
                    <div class="col-xs-8 col-sm-8 col-md-8">
                        <div class="form-group">
                            <strong>Description:</strong>
                            {!! Form::text('description', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <div class="form-group">
                            <strong>Operator:</strong>
                            {!! Form::select('operator', array('+'=>'Add','-'=>'Minus'),[], array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <div class="form-group">
                            <strong>Value:</strong>
                            {!! Form::number('value', null, array('placeholder' => 'Value','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection