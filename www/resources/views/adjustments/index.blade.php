@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Users</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>User List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    {{--<div class="col-lg-12 margin-tb clearfix">--}}
                        {{--<div class="pull-left padding-tb20">--}}

                                {{--<a class="btn btn-w-m btn-primary btn-sm" href="{{ route('users.create') }}"> Create New--}}
                                    {{--User</a>--}}

                        {{--</div>--}}

                    {{--</div>--}}
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9 m-b-xs text-left padding-tb20">
                                <a class="btn btn-w-m btn-primary btn-sm" href="{{ route('users.create') }}"> Create New
                                    User</a>
                            </div>
                            <div class="col-md-3 text-right padding-tb20">
                                <form method="GET" action="{{ route('users.index') }}" role="search">
                                    <div class="input-group">
                                        <input type="text" placeholder="Search"
                                               name="q" class="input-sm form-control"> <span
                                                class="input-group-btn">
                                                    <button type="submit"
                                                            class="btn btn-sm btn-primary">Go!</button></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table id="users-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th width="280px">Action</th>
                    </tr>
                    </thead>
                    @foreach ($data as $key => $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if(!empty($user->roles))
                                    @foreach($user->roles as $v)
                                        <label class="label label-success">{{ $v->display_name }}</label>
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-xs btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                                <a class="btn btn-xs btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
                <div class="col-md-12 text-right padding-tb20">{{ $data->appends(Request::only('q'))->links('vendor.pagination.bootstrap-4') }}</div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@endsection