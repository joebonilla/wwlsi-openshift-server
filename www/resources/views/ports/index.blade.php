@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Ports</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Ports List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Ports table</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-9 m-b-xs text-left">
                                    <div class="row">
                                        <a href="{{ route('ports.create') }}" class="btn btn-primary btn-xs">Add
                                            Port</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table id="users-table" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th width="280px">Action</th>
                            </tr>
                            </thead>
                            @foreach($ports as $port)
                                <tr>
                                    <td>{{ $port->name }}</td>

                                    <td>{{$port->address->address_name}}</td>

                                    <td>
                                        <a class="btn btn-xs btn-info"
                                           href="{{ route('ports.show', $port->id) }}">Show</a>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('ports.edit',$port->id) }}">Edit</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['ports.destroy',$port->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{--<div class="col-md-12 text-right">{{ $ports->appends(Request::only('q'))->links('vendor.pagination.bootstrap-4') }}</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection