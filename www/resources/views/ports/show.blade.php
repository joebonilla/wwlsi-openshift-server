@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('ports.index') }}">All Clients</a>
                </li>
                <li class="active">
                    <strong>Port Profile</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Port Details</h5>
                        <div class="pull-right">
                            <ul id="btn-list" class="list-unstyled list-inline">
                                <li>
                                    <a href="{{ route('ports.edit', $port->id) }}"
                                       class="btn btn-primary btn-xs "><i
                                                class="fa fa-pencil"></i> Edit</a>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div class="ibox-content profile-content">
                        <h4><strong>{{ $port->name }}</strong></h4>
                        <p><i class="fa fa-map-marker"></i>
                            {{ $port->address->address_name }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
