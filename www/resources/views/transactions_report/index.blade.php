@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Generate Transaction Report</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Transaction Report</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Generate Transaction Report</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" action="{{ route('transactions_report.generate') }}">
                            {{ csrf_field() }}
                            <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <select name="status" class="form-control">
                                        <option value="">All Status</option>
                                        <option value="0">New</option>
                                        <option value="1">On-Going</option>
                                        <option value="2">Completed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Client</label>
                                <div class="col-sm-10">
                                    <select name="client_id" class="form-control transaction-field">
                                        <option value="">All Clients</option>
                                        @foreach($clients as $client)
                                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Customer</label>
                                <div class="col-sm-10">
                                    <select name="customer_id" class="form-control transaction-field">
                                        <option value="">All Customer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Driver</label>
                                <div class="col-sm-10">
                                    <select name="driver_id" class="form-control">
                                        <option value="">All Drivers</option>
                                        @foreach($drivers as $driver)
                                            <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Helper</label>
                                <div class="col-sm-10">
                                    <select name="helper_id" class="form-control">
                                        <option value="">All Helper</option>
                                        @foreach($helpers as $helper)
                                            <option value="{{ $helper->id }}">{{ $helper->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Truck</label>
                                <div class="col-sm-10">
                                    <select name="truck_id" class="form-control">
                                        <option value="">All Truck</option>
                                        @foreach($trucks as $truck)
                                            <option value="{{ $truck->id }}">{{ $truck->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Created From</label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="created_from">
                                </div>

                                <label class="col-sm-2 control-label">Created To</label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name="created_to">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('trucks.index') }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Generate</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection