<div class="modal fade" id="accountingReportModal" tabindex="-1" role="dialog"
     aria-labelledby="accountingReportModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" class="form-horizontal" action="{{route('accounting_report')}}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Generate Report</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            {{--<div class="form-group">--}}
                            {{--<select class="form-control" id="type_report">--}}
                            {{--<option value="1">Monthly</option>--}}
                            {{--<option value="2">Per Client</option>--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            <div class="form-group select-form-client">
                                <select class="form-control" id="select-client" name="client[]" multiple required>

                                </select>
                            </div>
                            <div class="form-group">
                                <input type="date" class="form-control" name="date_from"
                                       placeholder="Date From" required>
                            </div>
                            <div class="form-group">
                                <input type="date" class="form-control" name="date_end"
                                       placeholder="Date to" required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Generate</button>
                </div>
            </form>
        </div>
    </div>
</div>