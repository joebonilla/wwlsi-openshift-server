<style>
    @import url("https://fonts.googleapis.com/css?family=Roboto:400,300,500,700");

    table {
        font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }

    .table-bordered {
        border: 1px solid #EBEBEB;
    }

    .table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th {
        border-top: 0;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: 1px solid #e7eaec;
        line-height: 1.42857;
        padding: 8px;
        vertical-align: top;
    }

    .table > thead > tr > th {
        border-bottom: 1px solid #DDDDDD;
        vertical-align: bottom;
    }

    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #e7e7e7;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #F5F5F6;
        border-bottom-width: 1px;
    }

    th {
        text-align: center;
        font-weight: bold;
    }

    table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    tbody {
        display: table-row-group;
        vertical-align: middle;
        border-color: inherit;
    }

    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #e7e7e7;
    }
</style>
<div style="width: 100%;text-align:center;font-family: Raleway, sans-serif">
    <h3>Westwind Logistic Solutions Inc.</h3>
    <h4>Income Report Statement Per Trip</h4>

</div>
<div style="clear: both"></div>
@foreach ($find_client_by_id->transactions->where('status',\App\Transaction::$status_complete)->where('updated_at', '>=', \Carbon\Carbon::parse($date_from))->where('updated_at','<=', \Carbon\Carbon::parse($date_to)) as $transaction)
    @if(!empty($transaction))
        <div style="float: left;">Trip Ref#: &nbsp; {{$transaction->id}}</div>
        <div style="float: right">Truck Plate#: &nbsp; {{$transaction->truck->plate_no}}</div>
        <div style="clear: both"></div>
        <div style="float: left">Client Name:
            &nbsp; {{$transaction->client->name . ' / ' .$transaction->customer->name}}</div>
        <div style="float: right">Destination:
            &nbsp; {{$transaction->customer->address->address_name}}</div>
        <div style="clear: both"></div>
        <div style="float: left">Date:
            &nbsp; {{$transaction->updated_date}}</div>
        <div style="float: right">Trucking Fee:
            &nbsp; {{money_format('%i',$transaction->customer->price)}}</div>
        <div style="clear: both"></div>
        <div>
            <table id="payroll-table" class="table table-bordered" style="width: 100%;text-align: center">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Note</th>
                    <th>Approve By</th>
                    <th>Date Approve</th>
                    <th>Cost</th>
                </tr>
                </thead>
                @php
                    $total_cost_expenses = 0;
                @endphp
                @foreach($transaction->transaction_expenses()->where('liquidation_status',\App\TransactionExpense::STATUS_APPROVED)->where('reflect_on_sales',0)->get() as $expenses)
                    <tr>
                        <td>{{$expenses->title}}</td>
                        <td>{{$expenses->note}}</td>
                        <td>{{$expenses->approved_by_user->name}}</td>
                        <td>{{$expenses->date_approved}}</td>
                        <td>{{money_format('%i',$expenses->cost)}}</td>
                    </tr>

                    @php
                        $total_cost_expenses += $expenses->cost;
                    @endphp

                @endforeach

                @php
                    $total_sum_expenses = $total_cost_expenses + $transaction->current_fuel_cost;
                @endphp


                @if(!empty($transaction->current_fuel_cost))
                    <tr>
                        <td colspan="4">Fuel Cost</td>
                        <td>{{money_format('%i',$transaction->current_fuel_cost)}}</td>

                    </tr>
                @endif
                <tr>
                    <td colspan="4">Total Expenses</td>
                    <td>{{money_format('%i',$total_sum_expenses)}}</td>

                </tr>
                <tr>

                    <td colspan="3">Income Percentage</td>
                    <td>{{money_format('%i', $transaction->customer->price - $total_sum_expenses)}}</td>
                    <td>{{money_format('%i',(($transaction->customer->price - $total_sum_expenses) / $transaction->customer->price) * 100 ) . '%'}}</td>

                </tr>

            </table>


        </div>
        <hr>
        <div style="clear: both"></div>
        <div style="margin-bottom:20px"></div>
        <br>
    @else
        <div>No Trip for {{$transaction->client->name . ' / ' .$transaction->customer->name}}</div>
        <hr>
    @endif

@endforeach

<div style="float: right">Date Generated: &nbsp; {{\Carbon\Carbon::now()->toFormattedDateString()}}</div>
