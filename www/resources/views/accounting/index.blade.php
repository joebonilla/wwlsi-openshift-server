@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-6">
            <h2>Accounting</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Accounting</strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-6">
            <a href="#myModalLabel" class="pull-right btn btn-sm btn-info margin-t30" data-toggle="modal"
               data-target="#accountingReportModal">Generate Report</a>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 m-b-md">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> <i
                                        class="fa fa-truck"></i>Transaction Expense</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false"><i
                                        class="fa fa-folder"></i>Trucks Repair</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false"><i
                                        class="fa fa-folder"></i>Payrolls</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false"><i
                                        class="fa fa-folder"></i>Payrolls Adjustments</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID#</th>
                                            <th>Transaction Ref#</th>
                                            <th>Title</th>
                                            <th>Cost</th>
                                            <th>Reflected On Sale</th>
                                            <th>Liquidation Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($transactions_expense as $expense)
                                            <tr>
                                                <td>{{ $expense->id }}</td>
                                                <td>
                                                    <a href="{{ route('transactions.show', $expense->transaction->id) }}">{{ $expense->transaction->ref_no }}</a>
                                                </td>
                                                <td>{{ $expense->title }}</td>
                                                <td>{{ $expense->cost }}</td>
                                                <td>@if($expense->reflect_on_sales) Yes @else No @endif</td>
                                                <td>@if($expense->liquidation_status == \App\TransactionExpense::STATUS_APPROVED)
                                                        Approved @else Pending @endif</td>
                                                <td>
                                                    @if($expense->liquidation_status == \App\TransactionExpense::STATUS_PENDING)
                                                        <a href="#" class="btn btn-primary btn-xs approve-cost"
                                                           data-toggle="tooltip" data-placement="top" title="Approve"
                                                           data-approve-action="{{ route('approve_cost', $expense->id) }}"><i
                                                                    class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                            Approve</a>
                                                    @else
                                                        @if(!empty($expense->approved_by_user->name))
                                                            Approved By:

                                                            {{ $expense->approved_by_user->name }}
                                                            ({{ $expense->date_approved }})
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                            {{--@empty--}}
                                            {{--<tr>--}}
                                            {{--<td class="text-center" colspan="7">No expense</td>--}}
                                            {{--</tr>--}}
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{--<div class="row text-center">--}}
                                {{--{{ $transactions_expense->appends(Request::only('q'))->links() }}--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Request by</th>
                                        <th>Description</th>
                                        <th>Cost</th>
                                        <th>Date Repaired</th>
                                        <th>Status</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($truck_repair_history as $key => $repair_history)
                                        <tr>
                                            <td>{{ $repair_history->id }}</td>
                                            <td>{{ $repair_history->users->name }}</td>
                                            <td>{{ $repair_history->repair_description }}</td>
                                            <td>{{money_format('%i',$repair_history->cost) }}</td>
                                            <td>{{ $repair_history->date_repair}}</td>
                                            <td>{{ $repair_history->repair_status}}</td>
                                            <td>
                                                {!! Form::model($repair_history,['method' => 'PATCH','route' => ['trucks.update', $repair_history->id],'style'=>'display:inline']) !!}
                                                <input type="hidden" name="update_status" value="1">
                                                <input type="hidden" name="repair_history_id"
                                                       value="{{$repair_history->id}}">
                                                @if($repair_history->status == \App\TruckRepairHistory::$status_complete)
                                                    <input type="hidden"
                                                           value="{{\App\TruckRepairHistory::$status_pending}}"
                                                           name="status">
                                                    {!! Form::submit('Pending', ['class' => 'btn btn-xs btn-danger']) !!}
                                                @else
                                                    <input type="hidden"
                                                           value="{{\App\TruckRepairHistory::$status_complete}}"
                                                           name="status">
                                                    {!! Form::submit('Complete', ['class' => 'btn btn-xs btn-success']) !!}
                                                @endif
                                                {!! Form::close() !!}
                                                <a class="btn btn-xs btn-info"
                                                   href="{{ route('trucks.show',$repair_history->truck->id) }}">Show</a>
                                            </td>
                                        </tr>

                                        {{--@empty--}}
                                        {{--<tr>--}}
                                        {{--<td class="text-center" colspan="7">No Truck Repair</td>--}}
                                        {{--</tr>--}}
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{--<div class="row text-center">--}}
                            {{--{{ $truck_repair_history->appends(Request::only('q'))->links() }}--}}
                            {{--</div>--}}

                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Request by</th>
                                            <th>For</th>
                                            <th>From - To</th>
                                            <th>Date Created</th>
                                            <th>Status</th>
                                            <th width="280px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($payrolls as $key => $payroll)
                                            <tr>
                                                <td>{{ $payroll->id }}</td>
                                                <td>{{ $payroll->users->name }}</td>
                                                <td>{{ $payroll->users_driver_helper->name }}</td>
                                                <td>{{ $payroll->start_date . ' - ' . $payroll->end_date}}</td>
                                                <td>{{ $payroll->created_date}}</td>
                                                <td>{{ $payroll->payroll_status}}</td>
                                                <td>
                                                    {!! Form::model($payroll,['method' => 'PATCH','route' => ['payrolls.update', $payroll->id],'style'=>'display:inline']) !!}
                                                    @if($payroll->status == \App\Payroll::$status_complete)
                                                        <input type="hidden" value="{{\App\Payroll::$status_pending}}"
                                                               name="status">
                                                        {!! Form::submit('Pending', ['class' => 'btn btn-xs btn-danger']) !!}
                                                    @else
                                                        <input type="hidden" value="{{\App\Payroll::$status_complete}}"
                                                               name="status">
                                                        {!! Form::submit('Complete', ['class' => 'btn btn-xs btn-success']) !!}
                                                    @endif
                                                    {!! Form::close() !!}
                                                    <a class="btn btn-xs btn-info"
                                                       href="{{ route('payrolls.show',$payroll->id) }}">Show</a>
                                                </td>
                                            </tr>

                                            {{--@empty--}}
                                            {{--<tr>--}}
                                            {{--<td class="text-center" colspan="7">No Payrolls</td>--}}
                                            {{--</tr>--}}
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{--<div class="row text-center">--}}
                                {{--{{ $payrolls->appends(Request::only('q'))->links() }}--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Operator</th>
                                            <th>Value</th>
                                            <th>Status</th>
                                            <th width="280px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($payroll_adjustments as $key => $adjustments)
                                            <tr>
                                                <td>{{$adjustments->description}}</td>
                                                <td>{{$adjustments->operator_text}}</td>
                                                <td>{{money_format('%i',$adjustments->value)}}</td>
                                                <td>{{$adjustments->payroll_status}}</td>
                                                <td>
                                                    {!! Form::model($adjustments,['method' => 'PATCH','route' => ['adjustments.update', $adjustments->id],'style'=>'display:inline']) !!}
                                                    @if($adjustments->status == \App\PayrollAdjustment::$status_complete)
                                                        <input type="hidden"
                                                               value="{{\App\PayrollAdjustment::$status_pending}}"
                                                               name="status">
                                                        {!! Form::submit('Pending', ['class' => 'btn btn-xs btn-danger']) !!}
                                                    @else
                                                        <input type="hidden"
                                                               value="{{\App\PayrollAdjustment::$status_complete}}"
                                                               name="status">
                                                        {!! Form::submit('Complete', ['class' => 'btn btn-xs btn-success']) !!}
                                                    @endif
                                                    {!! Form::close() !!}

                                                    {!! Form::open(['method' => 'DELETE','route' => ['adjustments.destroy', $adjustments->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                    <a class="btn btn-xs btn-info"
                                                       href="{{ route('payrolls.show',$adjustments->payroll_id) }}">Show</a>
                                                </td>
                                            </tr>

                                            {{--@empty--}}
                                            {{--<tr>--}}
                                            {{--<td class="text-center" colspan="7">No Adjustments</td>--}}
                                            {{--</tr>--}}
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{--<div class="row text-center">--}}
                                {{--{{ $payroll_adjustments->appends(Request::only('q'))->links() }}--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="approveExpense" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form class="form-horizontal" role="form" method="post" action="">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Approve Cost</h4>
                    </div>
                    <div class="modal-body">
                        <h3>Are you sure you want to approve this cost?</h3>
                        <input type="hidden" name="liquidation_status" value="1">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Approve Cost</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('accounting.modal_report')
@endsection

@section('scripts')
    <script src="/js/customjs/accounting.js"></script>
@endsection