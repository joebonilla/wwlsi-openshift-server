@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Users</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('users.index') }}">All Users</a>
                </li>
                <li class="active">
                    <strong>Users Profile</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Profile Detail</h5>
                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary btn-xs pull-right"><i
                                    class="fa fa-pencil"></i> Edit</a>
                    </div>
                    <div>
                        <div class="ibox-content profile-content">
                            <h4><strong>{{ $user->name }}</strong></h4>
                            <small class="text-muted">{{$user->role->name}}</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Activites</h5>
                    </div>
                    <div class="ibox-content">

                        <div>
                            <div class="feed-activity-list">
                                @foreach($audit_array as $audit)
                                    <div class="feed-element">
                                        <div class="media-body ">
                                            <small class="pull-right">{{$audit['created_at_ago']}}</small>
                                            <strong>{{ucfirst($audit['Type'])}} &nbsp;</strong>
                                            @if($audit['old'][0])
                                                @foreach($audit['old'][0] as $key => $item)
                                                    @if($key=='name')
                                                        {{$item}}
                                                    @elseif($key=='description')
                                                        {{$item}}
                                                    @endif
                                                @endforeach
                                                To &nbsp;
                                            @endif
                                            {{--<a href="{{$audit['url']}}">--}}
                                                @foreach($audit['new'][0] as $key => $item)
                                                    @if($key=='name')
                                                        {{$item}}
                                                    @elseif($key=='description')
                                                        {{$item}}
                                                    @endif
                                                @endforeach
                                            {{--</a>--}}
                                            <br>
                                            <small class="text-muted">{{$audit['created_at_date']}}</small>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection