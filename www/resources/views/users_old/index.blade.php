@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Users</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>User List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Users List</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-9 m-b-xs text-left">
                                        <a href="{{ route('users.create') }}" class="btn btn-primary">Add Users</a>
                                    </div>
                                    <div class="col-md-3 text-right">
                                        <div class="input-group"><input type="text" placeholder="Search"
                                                                        class="input-sm form-control"> <span
                                                    class="input-group-btn">
                                            <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($users as $user)
                <div class="col-lg-4">
                    <div class="contact-box">
                        <a href="{{ route('users.show', $user->id) }}">
                            <div class="col-sm-12">
                                <h3><strong>{{ $user->name }}</strong></h3>
                                <span class="">Position: {{ $user->role->name }}</span>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
@endsection