@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Add User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('payrolls.index') }}">All Payrolls</a>
                </li>
                <li class="active">
                    <strong>Generate Payrolls</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content">
                @include('layouts.error')
                {!! Form::open(array('route' => 'payrolls.store','method'=>'POST')) !!}
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <div class="form-group">
                            <strong>Date:</strong>
                            <div class="input-daterange input-group" id="datepicker">
                                {!! Form::text('start', null, array('placeholder' => 'Start','class' => 'form-control')) !!}
                                <span class="input-group-addon">to</span>
                                {!! Form::text('end', null, array('placeholder' => 'End','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <div class="form-group">
                            <strong>For:</strong>
                            {!! Form::select('users[]', $helper_driver,[], array('class' => 'form-control','multiple')) !!}
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <div class="form-group">
                            <strong>Adjustments:</strong>
                            {!! Form::number('adjustment_value', null, array('placeholder' => 'Adjustment','class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });
        });
    </script>
@endsection