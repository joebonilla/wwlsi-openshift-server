@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Payroll #{{$payroll_id}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('payrolls.index') }}">All Payrolls</a>
                </li>
                <li class="active">
                    <strong>Payroll Details</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12 margin-tb clearfix">
                        <div class="row">
                            <div class="pull-left">
                                <h2>
                                    <a class="btn btn-w-m btn-primary btn-xs inline"
                                       href="{{ route('adjustments.create') . '?payroll_id='.$payroll_id . '&user_id=' . $payroll_for_name->id }}">
                                        Add Adjustments
                                    </a>

                                    <a id="to-pdf" class="btn btn-info btn-xs inline"
                                       href="{{url('system/operations/payrolls/pdf'.'/' . $payroll_id)}}" @foreach ($payroll_users as $key => $payrolls) @if($payrolls->status == \App\Payroll::$status_pending) {{'disabled'}}@endif @endforeach><i
                                                class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        PDF
                                    </a>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <table id="payroll-table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="2">
                                {{$payroll_for_name->name}}
                            </th>
                            <th colspan="3">
                                Position: @foreach($payroll_for_name->roles as $roles){{$roles->display_name}}@endforeach
                            </th>
                        </tr>
                        <tr>
                            <th>Transaction Ref#</th>
                            <th>Truck Plate #</th>
                            <th>Transaction Cost</th>
                            <th>Net Pay</th>
                            <th>Trip Date</th>
                        </tr>
                        </thead>
                        @foreach ($payroll_users as $key => $payrolls)
                            @foreach($payrolls->payroll_users as $item)

                                <tr>
                                    <td>
                                        <a href="{{route('transactions.show',$item->transaction_id)}}">{{ $item->transaction_id }}</a>
                                    </td>

                                    <td>{{$item->transactions->truck->plate_no}}</td>
                                    <td>{{money_format('%i',$item->transactions->customer->price - $payrolls->adjustment_value)}}</td>
                                    @foreach($payroll_for_name->roles as $roles)
                                        {{--{{$roles->display_name}}--}}
                                        {{--                                    @foreach($payrolls->users->roles as $roles)--}}
                                        <td>{{money_format('%i',($item->transactions->customer->price - $payrolls->adjustment_value) * ($roles->display_name == 'Driver' ?  0.12 : 0.06))}}</td>
                                    @endforeach
                                    <td>{{$item->transactions->created_date}}</td>
                                </tr>
                            @endforeach
                        @endforeach


                        <tr>
                            <td colspan="4">Total Pay</td>
                            <td>{{money_format('%i',$total_pay)}}</td>
                        </tr>

                        @if($total_with_adjustment)
                            <tr>
                                <td colspan="4">Total Pay with Adjustments</td>
                                <td>{{money_format('%i',$total_with_adjustment + $total_pay)}}</td>
                            </tr>
                        @endif
                        <thead>
                        <tr>
                            <th colspan="5">Adjustments</th>
                        </tr>
                        </thead>
                        <tr>
                            {{--<th>Name</th>--}}
                            <th>Description</th>
                            <th>Operator</th>
                            <th>Value</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($payroll_users as $key => $payrolls)
                            @forelse ($payrolls->payroll_adjustments as $key => $adjustments)
                                <tr>
                                    <td>{{$adjustments->description}}</td>
                                    <td>{{$adjustments->operator_text}}</td>
                                    <td>{{money_format('%i',$adjustments->value)}}</td>
                                    <td>{{$adjustments->payroll_status}}</td>
                                    <td>
                                        {!! Form::model($adjustments,['method' => 'PATCH','route' => ['adjustments.update', $adjustments->id],'style'=>'display:inline']) !!}
                                        @if($adjustments->status == \App\PayrollAdjustment::$status_complete)
                                            <input type="hidden" value="{{\App\PayrollAdjustment::$status_pending}}"
                                                   name="status">
                                            {!! Form::submit('Pending', ['class' => 'btn btn-xs btn-danger']) !!}
                                        @else
                                            <input type="hidden" value="{{\App\PayrollAdjustment::$status_complete}}"
                                                   name="status">
                                            {!! Form::submit('Complete', ['class' => 'btn btn-xs btn-success']) !!}
                                        @endif
                                        <input type="hidden" value="{{$payroll_for_name->id}}" name="user_id">
                                        {!! Form::close() !!}

                                        {!! Form::open(['method' => 'DELETE','route' => ['adjustments.destroy', $adjustments->id],'style'=>'display:inline']) !!}
                                        <input type="hidden" value="{{$payroll_for_name->id}}" name="user_id">
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="5">No Adjustments</td>
                                </tr>
                            @endforelse
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection