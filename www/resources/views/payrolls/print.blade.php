<style>
    @import url("https://fonts.googleapis.com/css?family=Roboto:400,300,500,700");

    table {
        font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }

    .table-bordered {
        border: 1px solid #EBEBEB;
    }

    .table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th {
        border-top: 0;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: 1px solid #e7eaec;
        line-height: 1.42857;
        padding: 8px;
        vertical-align: top;
    }

    .table > thead > tr > th {
        border-bottom: 1px solid #DDDDDD;
        vertical-align: bottom;
    }

    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #e7e7e7;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #F5F5F6;
        border-bottom-width: 1px;
    }

    th {
        text-align: center;
        font-weight: bold;
    }

    table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    tbody {
        display: table-row-group;
        vertical-align: middle;
        border-color: inherit;
    }

    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #e7e7e7;
    }
</style>
<div style="width: 100%;text-align:center;font-family: Raleway, sans-serif">
    <h3>Westwind Logistic Solutions Inc.</h3>

</div>
<div style="clear: both"></div>
@foreach ($payroll_users as $key => $payrolls)
    <div>Scope Date: &nbsp; {{$payrolls->start_date . ' - ' . $payrolls->end_date}}</div>
@endforeach
<span>Name: {{$payroll_for_name->name}}</span><br>
<span style="float: left">Position: @foreach($payroll_for_name->roles as $roles){{$roles->display_name}}@endforeach</span>
<span style="float: right">Prepared by: &nbsp; {{$payroll_request_name->name}}</span>

<div style="clear: both"></div>
<hr>
<table id="payroll-table" class="table table-bordered" style="width: 100%;text-align: center">
    <thead>
    <tr>
        <th>Transaction Ref#</th>
        <th>Truck Plate #</th>
        <th>Transaction Cost</th>
        <th>Net Pay</th>
        <th>Trip Date</th>
    </tr>
    </thead>
    @foreach ($payroll_users as $key => $payrolls)
        @foreach($payrolls->payroll_users as $item)

            <tr>
                <td>
                    <a href="{{route('transactions.show',$item->transaction_id)}}">{{ $item->transaction_id }}</a>
                </td>

                <td>{{$item->transactions->truck->plate_no}}</td>
                <td>{{money_format('%i',$item->transactions->customer->price - $payrolls->adjustment_value)}}</td>
                @foreach($payroll_for_name->roles as $roles)
                    {{--{{$roles->display_name}}--}}
                    {{--                                    @foreach($payrolls->users->roles as $roles)--}}
                    <td>{{money_format('%i',($item->transactions->customer->price - $payrolls->adjustment_value) * ($roles->display_name == 'Driver' ?  0.12 : 0.06))}}</td>
                @endforeach
                <td>{{$item->transactions->created_date}}</td>
            </tr>
        @endforeach
    @endforeach
    <tr>
        <td colspan="4">Total Pay</td>
        <td>{{money_format('%i',$total_pay)}}</td>
    </tr>
    @if($total_with_adjustment)
        <tr>
            <td colspan="4">Total Pay with Adjustments</td>
            <td>{{money_format('%i',$total_with_adjustment + $total_pay)}}</td>
        </tr>

        <thead>
        <tr>
            <th colspan="5">Adjustments</th>
        </tr>
        </thead>
        <tr>
            {{--<th>Name</th>--}}
            <th>Description</th>
            <th>Operator</th>
            <th>Value</th>
            <th>Status</th>
        </tr>
        @foreach ($payroll_users as $key => $payrolls)
            @forelse ($payrolls->payroll_adjustments as $key => $adjustments)
                <tr>
                    <td>{{$adjustments->description}}</td>
                    <td>{{$adjustments->operator_text}}</td>
                    <td>{{money_format('%i',$adjustments->value)}}</td>
                    <td>{{$adjustments->payroll_status}}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="5">No Adjustments</td>
                </tr>
            @endforelse
        @endforeach
    @endif
</table>
<hr>
<div style="float: right">Date Generated: &nbsp; {{\Carbon\Carbon::now()->toFormattedDateString()}}</div>
