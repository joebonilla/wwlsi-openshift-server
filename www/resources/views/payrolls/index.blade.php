@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Payrolls Generated</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Payroll List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12 margin-tb clearfix">
                        <div class="pull-left">
                            <h2>
                                <a class="btn btn-w-m btn-primary btn-xs" href="{{ route('payrolls.create') }}">
                                    Generate New Payroll
                                </a>
                            </h2>
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12 m-b-md">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> <i
                                                class="fa fa-truck"></i>Pending</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="true"> <i
                                                class="fa fa-truck"></i>Approve</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <table id="payroll-pending-table" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Payroll Ref. No</th>
                                                <th>Request by</th>
                                                <th>For</th>
                                                <th>From - To</th>
                                                <th>Date Generated</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            {{--@foreach ($data as $key => $payroll)--}}
                                            {{--<tr>--}}
                                            {{--<td>{{ $payroll->id }}</td>--}}
                                            {{--<td>{{ $payroll->users->name }}</td>--}}
                                            {{--<td>{{ $payroll->users_driver_helper->name }}</td>--}}
                                            {{--<td>{{ $payroll->start_date . ' - ' . $payroll->end_date}}</td>--}}
                                            {{--<td>{{ $payroll->created_date}}</td>--}}
                                            {{--<td>{{ $payroll->payroll_status}}</td>--}}
                                            {{--<td>--}}
                                            {{--{!! Form::model($payroll,['method' => 'PATCH','route' => ['payrolls.update', $payroll->id],'style'=>'display:inline']) !!}--}}
                                            {{--@if($payroll->status == \App\Payroll::$status_complete)--}}
                                            {{--<input type="hidden"--}}
                                            {{--value="{{\App\Payroll::$status_pending}}"--}}
                                            {{--name="status">--}}
                                            {{--<input type="hidden"--}}
                                            {{--value="{{$payroll->users_driver_helper->id}}"--}}
                                            {{--name="user_id">--}}
                                            {{--{!! Form::submit('Pending', ['class' => 'btn btn-xs btn-danger']) !!}--}}
                                            {{--@else--}}
                                            {{--<input type="hidden"--}}
                                            {{--value="{{\App\Payroll::$status_complete}}"--}}
                                            {{--name="status">--}}
                                            {{--<input type="hidden"--}}
                                            {{--value="{{$payroll->users_driver_helper->id}}"--}}
                                            {{--name="user_id">--}}
                                            {{--{!! Form::submit('Complete', ['class' => 'btn btn-xs btn-success']) !!}--}}
                                            {{--@endif--}}
                                            {{--{!! Form::close() !!}--}}
                                            {{--<a class="btn btn-xs btn-info"--}}
                                            {{--href="{{ route('payrolls.show',$payroll->id). '?user_id='.$payroll->users_driver_helper->id }}">Show</a>--}}
                                            {{--{!! Form::open(['method' => 'DELETE','route' => ['payrolls.destroy', $payroll->id],'style'=>'display:inline']) !!}--}}
                                            {{--{!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}--}}
                                            {{--{!! Form::close() !!}--}}
                                            {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--@endforeach--}}
                                        </table>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <table id="payroll-complete-table" class="table table-bordered"
                                               style="width: 100%">
                                            <thead>
                                            <tr>
                                                <th>Payroll Ref. No</th>
                                                <th>Request by</th>
                                                <th>For</th>
                                                <th>From - To</th>
                                                <th>Date Generated</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(window).load(function () {
            let config = [
                {data: 'id', name: 'id'},
                {data: 'users.name', name: 'users.name', orderable: false},
                {data: 'users_driver_helper.name', name: 'users_driver_helper.name', orderable: false},
                {data: 'start', name: 'start', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at'},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},

            ];
            let btn_config = [
                {extend: 'copy'},
                {extend: 'excel', className: 'btn-sm btn-info'},
                {extend: 'pdf', className: 'btn-sm btn-success'},

                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ];
            $('#payroll-pending-table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{route('payroll_pending_data')}}',
                columns: config,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: btn_config,
            });

            $('#payroll-complete-table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{route('payroll_complete_data')}}',
                columns: config,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: btn_config,
            });

        });
    </script>
@endsection