{{--{{dd($data)}}--}}
{{--@foreach ($data as $key => $payroll)--}}

    {!! Form::model($data,['method' => 'PATCH','route' => ['payrolls.update', $data->id],'style'=>'display:inline']) !!}
    @if($data->status == \App\Payroll::$status_complete)
        <input type="hidden"
               value="{{\App\Payroll::$status_pending}}"
               name="status">
        <input type="hidden"
               value="{{$data->users_driver_helper->id}}"
               name="user_id">
        {!! Form::submit('Pending', ['class' => 'btn btn-xs btn-danger']) !!}
    @else
        <input type="hidden"
               value="{{\App\Payroll::$status_complete}}"
               name="status">
        <input type="hidden"
               value="{{$data->users_driver_helper->id}}"
               name="user_id">
        {!! Form::submit('Complete', ['class' => 'btn btn-xs btn-success']) !!}
    @endif
    {!! Form::close() !!}
    <a class="btn btn-xs btn-info"
       href="{{ route('payrolls.show',$data->id). '?user_id='.$data->users_driver_helper->id }}">Show</a>
    {!! Form::open(['method' => 'DELETE','route' => ['payrolls.destroy', $data->id],'style'=>'display:inline']) !!}
    {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
    {!! Form::close() !!}

{{--@endforeach--}}