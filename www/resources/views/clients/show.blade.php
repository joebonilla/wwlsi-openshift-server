@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('clients.index') }}">All Clients</a>
                </li>
                <li class="active">
                    <strong>Clients Profile</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Client Details</h5>
                        <div class="pull-right">
                            <ul id="btn-list" class="list-unstyled list-inline">
                                <li>
                                    <a href="{{ route('customers.create') . '?client_id=' . $client->id }}"
                                       class="btn btn-success btn-xs"><i class="fa fa-address-book-o"></i>
                                        Add
                                        Customer</a>
                                </li>
                                <li>
                                    <a href="{{ route('clients.edit', $client->id) }}"
                                       class="btn btn-primary btn-xs "><i
                                                class="fa fa-pencil"></i> Edit</a>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div class="ibox-content profile-content">
                        <h4><strong>{{ $client->name }}</strong></h4>
                        <p><i class="fa fa-map-marker"></i>
                            {{$client->address->address_name}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row animated fadeInRight">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Customers List</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">

                            <table id="users-table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Address</th>
                                    <th width="280px">Action</th>
                                </tr>
                                </thead>
                                @foreach($customer as $customers)
                                    {{--{{dd($customers->id)}}--}}
                                    <tr>
                                        <td>{{ $customers->name }}</td>
                                        <td>{{ $customers->price }}</td>

                                        <td>{{$customers->address->address_name}}</td>

                                        <td>
                                            <a class="btn btn-xs btn-info"
                                               href="{{ route('customers.show',$customers->id).'?client=' . $client->id }}">Show</a>
                                            <a class="btn btn-xs btn-primary"
                                               href="{{ route('customers.edit', $customers->id) . '?client=' . $client->id }}">Edit</a>
                                            {!! Form::open(['method' => 'DELETE','route' => ['customers.destroy',$customers->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            {{--<div class="col-md-12 text-right">{{ $customer->appends(Request::only('q'))->links('vendor.pagination.bootstrap-4') }}</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
