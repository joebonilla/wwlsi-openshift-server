@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Clients</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Client List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Clients table</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-9 m-b-xs text-left">
                                        <a href="{{ route('clients.create') }}" class="btn btn-primary btn-xs">Add
                                            Client</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table id="users-table" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th width="280px">Action</th>
                            </tr>
                            </thead>
                            @foreach($clients as $client)
                                <tr>
                                    <td>{{ $client->name }}</td>

                                    <td>{{$client->address->address_name}}</td>

                                    <td>
                                        <a class="btn btn-xs btn-info"
                                           href="{{ route('clients.show', $client->id) }}">Show</a>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('clients.edit',$client->id) }}">Edit</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['clients.destroy',$client->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{--<div class="col-md-12 text-right">{{ $clients->appends(Request::only('q'))->links('vendor.pagination.bootstrap-4') }}</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection