@extends('layouts.layout')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">Monthly</span>
                        <h5><a href="{{ route('transactions.index') }}">Transactions</a></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="no-margins">{{ $transaction_this_week_count }}</h1>
                                <small>This Week</small>
                            </div>
                            <div class="col-md-6">
                                <h1 class="no-margins">{{ $transaction_this_month_count }}</h1>
                                <small>This Month</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Total</span>
                        <h5><a href="{{ route('trucks.index') }}">Trucks</a></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-4">
                                <h1 class="no-margins">{{ $available_trucks_count }}</h1>
                                <small>Available</small>
                            </div>
                            <div class="col-md-4">
                                <h1 class="no-margins">{{ $on_going_trucks_count }}</h1>
                                <small>On going</small>
                            </div>
                            <div class="col-md-4">
                                <h1 class="no-margins">{{ $under_repair_trucks_count }}</h1>
                                <small>Under Repair</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Today</span>
                        <h5>Fuel Cost</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">₱ {{ $current_fuel_expense->cost or 0 }} <a
                                    href="{{ route('expenses.index') . '#fuel-expense-tab' }}"
                                    class="btn btn-xs btn-save"><i class="fa fa-pencil"></i> Update</a></h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">All Time</span>
                        <h5>Top Customers</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-hover no-margins no-dt">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Client</th>
                                <th>Customer</th>
                                <th>Total Transactions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($top_customers as $top_customer)
                                <tr>
                                    <td>
                                        <a href="{{ route('customers.show', $top_customer->id) }}">
                                            {{ $top_customer->id }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('clients.show', $top_customer->client->id) }}">
                                            {{ $top_customer->client->name }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('customers.show', $top_customer->id) }}">
                                            {{ $top_customer->name }}
                                        </a>
                                    </td>
                                    <td>{{ $top_customer->total_transactions }}</td>
                                </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="4">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">All Time</span>
                        <h5>Top Drivers</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-hover no-margins no-dt">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th width="25">Total Transactions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($top_drivers as $top_driver)
                                <tr>
                                    <td>
                                        <a href="{{ route('users.show', $top_driver->id) }}">
                                            {{ $top_driver->id }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('users.show', $top_driver->id) }}">
                                            {{ $top_driver->name }}
                                        </a>
                                    </td>
                                    <td>{{ $top_driver->total_transactions }}</td>
                                </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="3">No Data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">All Time</span>
                        <h5>Top Helpers</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-hover no-margins no-dt">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th width="25">Total Transactions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($top_helpers as $top_helper)
                            <tr>
                                <td>
                                    <a href="{{ route('users.show', $top_helper->id) }}">
                                        {{ $top_helper->id }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('users.show', $top_helper->id) }}">
                                        {{ $top_helper->name }}
                                    </a>
                                </td>
                                <td>{{ $top_helper->total_transactions }}</td>
                            </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="3">No Data</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
