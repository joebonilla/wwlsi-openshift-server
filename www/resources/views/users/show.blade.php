@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Users</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('users.index') }}">All Users</a>
                </li>
                <li class="active">
                    <strong>Users Profile</strong>
                </li>
            </ol>
        </div>
    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Profile Detail</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <h4><strong>{{ $user->name }}</strong></h4>
                        <h5><strong>Email: </strong> {{ $user->email }}</h5>
                        <h5><strong>Contact No.: </strong> {{ $user->contact_no }}</h5>
                        <h5>
                            <strong>Roles:</strong>
                            @if(!empty($user->roles))
                                @foreach($user->roles as $v)
                                    <label class="label label-success">{{ $v->display_name }}</label>
                                @endforeach
                            @endif
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            @if(!$user->hasRoles('driver') && !$user->hasRoles('helper'))
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Activites</h5>
                </div>
                <div class="ibox-content">
                    <div style="height: 340px; overflow-y: scroll;">
                        @forelse($user_activities as $activity)
                            @if($activity->displayAudit())
                                <div class="feed-activity-list">
                                    <div class="feed-element">
                                        <div class="media-body ">
                                            <small class="pull-right">{{ $activity->created_at->diffForHumans() }}</small>
                                            @if($activity->auditable_type == "App\\Transaction")
                                                <p><a href="{{ route('transactions.show', $activity->auditable_id) }}">{!! $activity->displayAudit() !!} On Transaction Ref# {{ $activity->auditable_id }}</a></p>
                                            @else
                                                <p>{!!  $activity->displayAudit()  !!}</p>
                                            @endif
                                            <small class="text-muted">{{ $activity->created_at->toDayDateTimeString() }}</small>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @empty
                            <div class="feed-activity-list">
                                <div class="feed-element">
                                    <div class="media-body ">
                                        <p>No activities yet.</p>
                                    </div>
                                </div>
                            </div>
                        @endforelse
                    </div>
                </div>
            </div>
            @else
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Current Transaction</h5>
                    </div>
                    <div class="ibox-content">
                        @if($user_active_transaction->count())
                            <p>Ref#: {{ $user_active_transaction->id }}</p>
                            <p>Status: {{ $user_active_transaction->status_label }}</p>
                        @else
                            <h4>No Current Transaction</h4>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection