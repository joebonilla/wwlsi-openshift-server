@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Register Chassis</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('chassis.index') }}">All Chassis</a>
                </li>
                <li class="active">
                    <strong>Register Chassis</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Register Chassis Form</h5>
                    </div>
                    <div class="ibox-content">
                        @include('layouts.error')
                        <form method="post" class="form-horizontal" action="{{ route('chassis.update', $chassis->id) }}">
                            {{ csrf_field() }}
                            <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10"><input type="text" name="name" class="form-control" placeholder="Chassis Name" value="{{ $chassis->name }}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Type</label>
                                <div class="col-sm-10">
                                	<select name="type" class="form-control" required>
                                		<option value="Flatbed" @if($chassis->type == "Flatbed") selected @endif>Flatbed</option>
                                		<option value="Combo" @if($chassis->type == "Combo") selected @endif>Combo</option>
                                		<option value="20 gooseneck" @if($chassis->type == "20 gooseneck") selected @endif>20 gooseneck</option>
                                		<option value="40 gooseneck" @if($chassis->type == "40 gooseneck") selected @endif>40 gooseneck</option>
                                	</select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Plate Number</label>
                                <div class="col-sm-10"><input type="text" name="number" class="form-control" placeholder="Plate Number" value="{{ $chassis->number }}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('chassis.index') }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection