@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Chassis</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Chassis List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 m-b-md">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#available-tab" aria-expanded="true"> <i
                                        class="fa fa-check"></i>Available</a></li>
                        <li class=""><a data-toggle="tab" href="#unavailable-tab" aria-expanded="false"><i
                                        class="fa fa-times"></i>Unavaialble</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="available-tab" class="tab-pane active">
						    <div class="panel-body">
						        <div class="row">
						            <div class="col-md-4">
						                <div class="col-md-9 m-b-xs text-left">
						                    <div class="row">
						                        <a href="{{ route('chassis.create') }}" class="btn btn-primary btn-xs">Register New Chassis</a>
						                    </div>
						                </div>
						            </div>
						        </div>
						        <div class="table-responsive">
						            <table id="available-table" class="table table-striped table-hover">
						                <thead>
							                <tr>
							                    <th>Name</th>
							                    <th>Type</th>
							                    <th>Plate Number</th>
							                    <th>Actions</th>
							                </tr>
						                </thead>
						                <tbody>
						                	@foreach($chassis_available as $chassi)
				                                <tr>
				                                    <td>{{ $chassi->name }}</td>
				                                    <td>{{ $chassi->type }}</td>
				                                    <td>{{ $chassi->number }}</td>
				                                    <td><a class="btn btn-xs btn-primary" href="{{ route('chassis.edit', $chassi->id) }}">Edit</a></td>
				                                </tr>
				                            @endforeach
						                </tbody>
						            </table>
						        </div>
						    </div>
						</div>

						<div id="unavailable-tab" class="tab-pane">
						    <div class="panel-body">
						        <div class="table-responsive">
						            <table id="unavailable-table" class="table table-striped table-hover">
						                <thead>
							                <tr>
							                    <th>Name</th>
							                    <th>Type</th>
							                    <th>Plate Number</th>
							                    <th>Current Transaction</th>
							                    <th>Actions</th>
							                </tr>
						                </thead>
						                <tbody>
						                	@foreach($chassis_unavailable as $chassi)
				                                <tr>
				                                    <td>{{ $chassi->name }}</td>
				                                    <td>{{ $chassi->type }}</td>
				                                    <td>{{ $chassi->number }}</td>
				                                    <td><a href="{{ route('transactions.show', $chassi->current_transaction->id) }}">{{ $chassi->current_transaction->ref_no }}</a></td>
				                                    <td><a class="btn btn-xs btn-primary" href="{{ route('chassis.edit', $chassi->id) }}">Edit</a></td>
				                                </tr>
				                            @endforeach
						                </tbody>
						            </table>
						        </div>
						    </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection