<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>WestWind Logistics</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/unitegallery/1.7.40/css/unite-gallery.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/unitegallery/1.7.40/themes/default/ug-theme-default.min.css"
          rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        img {
            width: 100%;
            display: block;
            margin: 0 auto;
        }

        .grid-item {
            width: 600px;
        }

        .grid-item--width2 {
            width: 400px;
        }
    </style>
</head>
<body id="page-top" class="landing-page">
<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Westwind Logistic Solution Inc.</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="page-scroll" href="#page-top">Home</a></li>
                    <li><a class="page-scroll" href="#features">Features</a></li>
                    <li><a class="page-scroll" href="#our-fleet">Our Fleet</a></li>
                    {{--<li><a class="page-scroll" href="#testimonials">Testimonials</a></li>--}}
                    {{--<li><a class="page-scroll" href="#about-us">About Us</a></li>--}}
                    <li><a class="page-scroll" href="#contact">Company Profile</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Go beyond logistics,<br/>
                        make the world go<br/>
                        round and <br/>revolution business.<br/></h1>
                    <p>Logistics through innovation, dedication, and technology.</p>
                    {{--<p>--}}
                    {{--<a class="btn btn-lg btn-primary" href="#" role="button">READ MORE</a>--}}
                    {{--<a class="caption-link" href="#" role="button">Inspinia Theme</a>--}}
                    {{--</p>--}}
                </div>
                {{--<div class="carousel-image wow zoomIn">--}}
                {{--<img src="img/westwindlogo.jpg" alt="laptop"/>--}}
                {{--</div>--}}
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back one"></div>

        </div>
        <div class="item">
            {{--<div class="container">--}}
                {{--<div class="carousel-caption blank">--}}
                    {{--<h1>We create meaningful <br/> interfaces that inspire.</h1>--}}
                    {{--<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>--}}
                    {{--<p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- Set background for slide in css -->
            <div class="header-back two"></div>
        </div>
    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


<section id="features" class="container services">
    <div class="row">
        <div class="col-sm-6">
            <h2>Vision</h2>
            <p>To be integrated transportation company with the goal to provide the best logistic needs of our customers
                and to be the leading logistics provider of choice for premier companies, offering innovative business
                solutions that will enable them to effectively manage their supply chain.</p>
            {{--<p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>--}}
        </div>
        <div class="col-sm-6">
            <h2>Mission</h2>

            <ul>
                <li><p>To eliminate the logistical problems of our customers.</p></li>
                <li><p>To be a well known and reputable company.</p></li>
                <li><p>To enrich our experience in the transportation field.</p></li>
                <li><p>To be the market leader in transportation.</p></li>
                <li><p>To accomplish cargo delivery safely and on-time.</p></li>
            </ul>
            {{--</p>--}}
            {{--<p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>--}}
        </div>
        {{--<div class="col-sm-4">--}}
        {{--<h2>Insurance</h2>--}}
        {{--<p>Our company provides have <strong>Inland Marine Cargo Insurance</strong> that provides protection for the--}}
        {{--goods while being in transit.</p>--}}
        {{--<p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>--}}
        {{--</div>--}}
        {{--<div class="col-sm-3">--}}
        {{--<h2>Advanced Forms</h2>--}}
        {{--<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies--}}
        {{--vehicula ut id elit. Morbi leo risus.</p>--}}
        {{--<p><a class="navy-link" href="#" role="button">Details &raquo;</a></p>--}}
        {{--</div>--}}
    </div>
</section>

<section class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>Tramigo T23 <br/> <span class="navy"> GPS Tracking System</span></h1>
            {{--<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>--}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center wow fadeInLeft">
            <div>
                <i class="fa fa-desktop features-icon"></i>
                <h2>Computerization</h2>
                <p>The Company utilizes integrated system that covers the entire operational elements of the company
                    such as
                    delivery status, employees data, spare parts stock records, maintenance history, customers data,
                    etc.
                    The system was developed internally and enables the company to maintain its operation more
                    efficiently.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-cogs features-icon"></i>
                <h2>Maintenance</h2>
                <p>Our company provides corrective and preventive which focuses on vehicle care, repair and restoration
                    services. We have and in-house truck mechanic to ensure high quality repairs and maintenance.</p>
            </div>
        </div>
        <div class="col-md-6 text-center  wow zoomIn">
            <img src="img/tramigo.jpg" alt="Tramigo" class="img-responsive">
        </div>
        <div class="col-md-3 text-center wow fadeInRight">
            <div>
                <i class="fa fa-university features-icon"></i>
                <h2>Insurance</h2>
                <p>Our company provides have <strong>Inland Marine Cargo Insurance</strong> that provides protection for
                    the
                    goods while being in transit.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-television features-icon"></i>
                <h2>Monitoring System</h2>
                <p>At Westwind Logistic, we believe that <b>SAFETY</b> and <b>PUNCTUALITY</b> in cargo delivery and your
                    satisfaction are our main vision that we always hold in hand when running the company. All our
                    deliveries are tightly monitored along the delivery route. Our units were equipped with <b>Tramigo
                        T23</b> worlds best selling GPS tracking facility for online monitoring in order to give the
                    best service to our customers.</p>
            </div>
        </div>
    </div>
    {{--<div class="row">--}}
    {{--<div class="col-lg-12 text-center">--}}
    {{--<div class="navy-line"></div>--}}
    {{--<h1>Discover great feautres</h1>--}}
    {{--<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="row features-block">--}}
    {{--<div class="col-lg-6 features-text wow fadeInLeft">--}}
    {{--<small>INSPINIA</small>--}}
    {{--<h2>Perfectly designed </h2>--}}
    {{--<p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully--}}
    {{--responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It--}}
    {{--has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>--}}
    {{--<a href="#" class="btn btn-primary">Learn more</a>--}}
    {{--</div>--}}
    {{--<div class="col-lg-6 text-right wow fadeInRight">--}}
    {{--<img src="img/landing/dashboard.png" alt="dashboard" class="img-responsive pull-right">--}}
    {{--</div>--}}
    {{--</div>--}}
</section>

<section id="our-fleet" class="gray-section team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Our Fleet</h1>
                <p>Westwind Logistic operates a total fleet of 15 brand new trailer trucks and 3 wingvans. In order to
                    deliver maximum results to our customers, we chose to use well-known brands. We combined these
                    tractor heads with a complete range of trailers, which are able to transport various kinds of goods
                    up to 60 tons.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-6 wow fadeInLeft">
                    <div class="team-member wow zoomIn">
                        <a href="img/HOWO.jpg" class="image-link">
                            <img src="img/HOWO.jpg" class="img-responsive img-thumbnail" alt="">
                        </a>

                        <h4><span class="navy">HOWO HW76 4x2 Tractor Head</span> (10 units)</h4>
                        {{--<p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an--}}
                        {{--soluta sensibus. </p>--}}
                        <ul class="text-left">
                            <li><p>Make: Sinotruck</p>
                            </li>
                            <li><p>Engine Power: 371 HP</p>
                            </li>
                            <li><p>Transmission: HW19710</p>
                            </li>
                            <li><p>Ratio: 3.7</p></li>

                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 wow">
                    <div class="team-member wow zoomIn">
                        <a href="img/howoa7.jpg" class="image-link">
                            <img src="img/howoa7.jpg" class="img-responsive img-thumbnail" alt="">
                        </a>
                        <h4><span class="navy">HOWO A7 4x2 Tractor Head</span> (5 units)</h4>
                        {{--<p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an--}}
                        {{--soluta sensibus.</p>--}}
                        <ul class="text-left">
                            <li><p>Make: Sinotruck</p>
                            </li>
                            <li><p>Horsepower: 371 HP</p>
                            </li>
                            <li><p>Transmission: HW19710</p>
                            </li>
                            <li><p>Ratio 3.7</p></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6 wow fadeInRight">
                    <div class="team-member">
                        <a href="img/flatbed.jpg" class="image-link">
                            <img src="img/flatbed.jpg" class="img-responsive img-thumbnail" alt="">
                        </a>

                        <h4><span class="navy">Flatbed Semi Trailer</span> (15 units)</h4>
                        {{--<p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an--}}
                        {{--soluta sensibus.</p>--}}
                        <ul class="text-left">
                            <li><p>Make: Sinotruck</p>
                            </li>
                            <li><p>Axie Fuwa, 3</p>
                            </li>
                            <li><p>Dimension: 12.5m x 2.48m</p>
                            </li>
                            <li><p>Tires: 12</p></li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-6 wow fadeInLeft">
                    <div class="team-member">
                        <a href="img/wingvan.jpg" class="image-link">
                            <img src="img/wingvan.jpg" class="img-responsive img-thumbnail" alt="">
                        </a>

                        <h4><span class="navy">Wingvan</span> (3 units)</h4>
                        {{--<p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an--}}
                        {{--soluta sensibus.</p>--}}
                        <ul class="text-left">
                            <li><p>Make: Fuso</p>
                            </li>
                            <li><p>Model FU54JV</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="row">--}}
        {{--<div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">--}}
        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non--}}
        {{--quis ad perspiciatis, totam corporis ea, alias ut unde.</p>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
</section>

{{--<section class="features">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-lg-12 text-center">--}}
{{--<div class="navy-line"></div>--}}
{{--<h1>Even more great feautres</h1>--}}
{{--<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row features-block">--}}
{{--<div class="col-lg-3 features-text wow fadeInLeft">--}}
{{--<small>INSPINIA</small>--}}
{{--<h2>Perfectly designed </h2>--}}
{{--<p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully--}}
{{--responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query.--}}
{{--It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>--}}
{{--<a href="#" class="btn btn-primary">Learn more</a>--}}
{{--</div>--}}
{{--<div class="col-lg-6 text-right m-t-n-lg wow zoomIn">--}}
{{--<img src="img/landing/iphone.jpg" class="img-responsive" alt="dashboard">--}}
{{--</div>--}}
{{--<div class="col-lg-3 features-text text-right wow fadeInRight">--}}
{{--<small>INSPINIA</small>--}}
{{--<h2>Perfectly designed </h2>--}}
{{--<p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully--}}
{{--responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query.--}}
{{--It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>--}}
{{--<a href="#" class="btn btn-primary">Learn more</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--</section>--}}

{{--<section class="timeline gray-section">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-lg-12 text-center">--}}
{{--<div class="navy-line"></div>--}}
{{--<h1>Our workflow</h1>--}}
{{--<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row features-block">--}}

{{--<div class="col-lg-12">--}}
{{--<div id="vertical-timeline" class="vertical-container light-timeline center-orientation">--}}
{{--<div class="vertical-timeline-block">--}}
{{--<div class="vertical-timeline-icon navy-bg">--}}
{{--<i class="fa fa-briefcase"></i>--}}
{{--</div>--}}

{{--<div class="vertical-timeline-content">--}}
{{--<h2>Meeting</h2>--}}
{{--<p>Conference on the sales results for the previous year. Monica please examine sales trends--}}
{{--in marketing and products. Below please find the current status of the sale.--}}
{{--</p>--}}
{{--<a href="#" class="btn btn-xs btn-primary"> More info</a>--}}
{{--<span class="vertical-date"> Today <br/> <small>Dec 24</small> </span>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="vertical-timeline-block">--}}
{{--<div class="vertical-timeline-icon navy-bg">--}}
{{--<i class="fa fa-file-text"></i>--}}
{{--</div>--}}

{{--<div class="vertical-timeline-content">--}}
{{--<h2>Decision</h2>--}}
{{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum--}}
{{--has been the industry's standard dummy text ever since.</p>--}}
{{--<a href="#" class="btn btn-xs btn-primary"> More info</a>--}}
{{--<span class="vertical-date"> Tomorrow <br/> <small>Dec 26</small> </span>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="vertical-timeline-block">--}}
{{--<div class="vertical-timeline-icon navy-bg">--}}
{{--<i class="fa fa-cogs"></i>--}}
{{--</div>--}}

{{--<div class="vertical-timeline-content">--}}
{{--<h2>Implementation</h2>--}}
{{--<p>Go to shop and find some products. Lorem Ipsum is simply dummy text of the printing and--}}
{{--typesetting industry. Lorem Ipsum has been the industry's. </p>--}}
{{--<a href="#" class="btn btn-xs btn-primary"> More info</a>--}}
{{--<span class="vertical-date"> Monday <br/> <small>Jan 02</small> </span>--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}

{{--</section>--}}

{{--<section id="testimonials" class="navy-section testimonials" style="margin-top: 0">--}}

{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-lg-12 text-center wow zoomIn">--}}
{{--<i class="fa fa-comment big-icon"></i>--}}
{{--<h1>--}}
{{--What our users say--}}
{{--</h1>--}}
{{--<div class="testimonials-text">--}}
{{--<i>"Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model--}}
{{--text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various--}}
{{--versions have evolved over the years, sometimes by accident, sometimes on purpose (injected--}}
{{--humour and the like)."</i>--}}
{{--</div>--}}
{{--<small>--}}
{{--<strong>12.02.2014 - Andy Smith</strong>--}}
{{--</small>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--</section>--}}

{{--<section class="comments gray-section" style="margin-top: 0">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-lg-12 text-center">--}}
{{--<div class="navy-line"></div>--}}
{{--<h1>What our partners say</h1>--}}
{{--<p>Donec sed odio dui. Etiam porta sem malesuada. </p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row features-block">--}}
{{--<div class="col-lg-4">--}}
{{--<div class="bubble">--}}
{{--"Uncover many web sites still in their infancy. Various versions have evolved over the years,--}}
{{--sometimes by accident, sometimes on purpose (injected humour and the like)."--}}
{{--</div>--}}
{{--<div class="comments-avatar">--}}
{{--<a href="#" class="pull-left">--}}
{{--<img alt="image" src="img/landing/avatar3.jpg">--}}
{{--</a>--}}
{{--<div class="media-body">--}}
{{--<div class="commens-name">--}}
{{--Andrew Williams--}}
{{--</div>--}}
{{--<small class="text-muted">Company X from California</small>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="col-lg-4">--}}
{{--<div class="bubble">--}}
{{--"Uncover many web sites still in their infancy. Various versions have evolved over the years,--}}
{{--sometimes by accident, sometimes on purpose (injected humour and the like)."--}}
{{--</div>--}}
{{--<div class="comments-avatar">--}}
{{--<a href="#" class="pull-left">--}}
{{--<img alt="image" src="img/landing/avatar1.jpg">--}}
{{--</a>--}}
{{--<div class="media-body">--}}
{{--<div class="commens-name">--}}
{{--Andrew Williams--}}
{{--</div>--}}
{{--<small class="text-muted">Company X from California</small>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="col-lg-4">--}}
{{--<div class="bubble">--}}
{{--"Uncover many web sites still in their infancy. Various versions have evolved over the years,--}}
{{--sometimes by accident, sometimes on purpose (injected humour and the like)."--}}
{{--</div>--}}
{{--<div class="comments-avatar">--}}
{{--<a href="#" class="pull-left">--}}
{{--<img alt="image" src="img/landing/avatar2.jpg">--}}
{{--</a>--}}
{{--<div class="media-body">--}}
{{--<div class="commens-name">--}}
{{--Andrew Williams--}}
{{--</div>--}}
{{--<small class="text-muted">Company X from California</small>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}


{{--</div>--}}
{{--</div>--}}

{{--</section>--}}

<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Our Team</h1>
                {{--<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>--}}
            </div>
        </div>
        <div class="row">
            <div id="gallery" style="display:none;">

                <img alt="Image 1 Title" src="img/westwind/img1.JPG"
                     data-image="img/westwind/img1.JPG"
                     data-description="Image 1 Description">

                <img alt="Image 2 Title" src="img/westwind/img2.JPG"
                     data-image="img/westwind/img2.JPG"
                     data-description="Image 2 Description">

                <img alt="Image 3 Title" src="img/westwind/img3.JPG"
                     data-image="img/westwind/img3.JPG"
                     data-description="Image 3 Description">

                <img alt="Image 4 Title" src="img/westwind/img4.JPG"
                     data-image="img/westwind/img4.JPG"
                     data-description="Image 4 Description">

                <img alt="Image 5 Title" src="img/westwind/img5.JPG"
                     data-image="img/westwind/img5.JPG"
                     data-description="Image 5 Description">

                <img alt="Image 6 Title" src="img/westwind/img6.JPG"
                     data-image="img/westwind/img6.JPG"
                     data-description="Image 6 Description">
                <img alt="Image 7 Title" src="img/westwind/img7.JPG"
                     data-image="img/westwind/img7.JPG"
                     data-description="Image 7 Description">

                <img alt="Image 8 Title" src="img/westwind/img8.JPG"
                     data-image="img/westwind/img8.JPG"
                     data-description="Image 8 Description">
                <img alt="Image 9 Title" src="img/westwind/img9.JPG"
                     data-image="img/westwind/img9.JPG"
                     data-description="Image 9 Description">

                <img alt="Image 10 Title" src="img/westwind/img10.JPG"
                     data-image="img/westwind/img10.JPG"
                     data-description="Image 10 Description">
                <img alt="Image 11 Title" src="img/westwind/img11.JPG"
                     data-image="img/westwind/img11.JPG"
                     data-description="Image 11 Description">

                <img alt="Image 12 Title" src="img/westwind/img12.JPG"
                     data-image="img/westwind/img12.JPG"
                     data-description="Image 12 Description">
                <img alt="Image 13 Title" src="img/westwind/img13.JPG"
                     data-image="img/westwind/img13.JPG"
                     data-description="Image 13 Description">
            </div>

        </div>
    </div>

</section>
{{--<section id="about-us" class="about-us">--}}
{{--<div class="container">--}}
{{--<div class="row m-b-lg">--}}
{{--<div class="col-lg-12 text-center">--}}
{{--<div class="navy-line"></div>--}}
{{--<h1>App Pricing</h1>--}}
{{--<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
{{--<div class="col-lg-4 wow zoomIn">--}}
{{--<ul class="pricing-plan list-unstyled">--}}
{{--<li class="pricing-title">--}}
{{--Basic--}}
{{--</li>--}}
{{--<li class="pricing-desc">--}}
{{--Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an--}}
{{--soluta sensibus.--}}
{{--</li>--}}
{{--<li class="pricing-price">--}}
{{--<span>$16</span> / month--}}
{{--</li>--}}
{{--<li>--}}
{{--Dashboards--}}
{{--</li>--}}
{{--<li>--}}
{{--Projects view--}}
{{--</li>--}}
{{--<li>--}}
{{--Contacts--}}
{{--</li>--}}
{{--<li>--}}
{{--Calendar--}}
{{--</li>--}}
{{--<li>--}}
{{--AngularJs--}}
{{--</li>--}}
{{--<li>--}}
{{--<a class="btn btn-primary btn-xs" href="#">Signup</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}

{{--<div class="col-lg-4 wow zoomIn">--}}
{{--<ul class="pricing-plan list-unstyled selected">--}}
{{--<li class="pricing-title">--}}
{{--Standard--}}
{{--</li>--}}
{{--<li class="pricing-desc">--}}
{{--Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an--}}
{{--soluta sensibus.--}}
{{--</li>--}}
{{--<li class="pricing-price">--}}
{{--<span>$22</span> / month--}}
{{--</li>--}}
{{--<li>--}}
{{--Dashboards--}}
{{--</li>--}}
{{--<li>--}}
{{--Projects view--}}
{{--</li>--}}
{{--<li>--}}
{{--Contacts--}}
{{--</li>--}}
{{--<li>--}}
{{--Calendar--}}
{{--</li>--}}
{{--<li>--}}
{{--AngularJs--}}
{{--</li>--}}
{{--<li>--}}
{{--<strong>Support platform</strong>--}}
{{--</li>--}}
{{--<li class="plan-action">--}}
{{--<a class="btn btn-primary btn-xs" href="#">Signup</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}

{{--<div class="col-lg-4 wow zoomIn">--}}
{{--<ul class="pricing-plan list-unstyled">--}}
{{--<li class="pricing-title">--}}
{{--Premium--}}
{{--</li>--}}
{{--<li class="pricing-desc">--}}
{{--Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an--}}
{{--soluta sensibus.--}}
{{--</li>--}}
{{--<li class="pricing-price">--}}
{{--<span>$160</span> / month--}}
{{--</li>--}}
{{--<li>--}}
{{--Dashboards--}}
{{--</li>--}}
{{--<li>--}}
{{--Projects view--}}
{{--</li>--}}
{{--<li>--}}
{{--Contacts--}}
{{--</li>--}}
{{--<li>--}}
{{--Calendar--}}
{{--</li>--}}
{{--<li>--}}
{{--AngularJs--}}
{{--</li>--}}
{{--<li>--}}
{{--<a class="btn btn-primary btn-xs" href="#">Signup</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row m-t-lg">--}}
{{--<div class="col-lg-8 col-lg-offset-2 text-center m-t-lg">--}}
{{--<p>*Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model--}}
{{--text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. <span--}}
{{--class="navy">Various versions</span> have evolved over the years, sometimes by accident,--}}
{{--sometimes on purpose (injected humour and the like).</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--</section>--}}

<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Contact Us</h1>
                {{--<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>--}}
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <address>
                    <strong><span class="navy">Westwind Logistic Solutions, Inc.</span></strong><br/>
                    Ground Floor Boston Tower, 1415 Severino St.<br/>
                    Sta. Cruz, Manila, Philippines<br/>
                    <abbr title="Phone">Phone:</abbr> (632) 732-6551 to 52
                    <abbr title="Phone">Mobile:</abbr> +639178804039
                </address>
            </div>
            {{--<div class="col-lg-4">--}}
            {{--<p class="text-color">--}}
            {{--Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis,--}}
            {{--totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis,--}}
            {{--totam corporis ea,--}}
            {{--</p>--}}
            {{--</div>--}}
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:westwindlogistic2016@email.com" class="btn btn-primary">Send us email</a>
                <p class="m-t-sm">
                    Or follow us on social platform
                </p>
                <ul class="list-inline social-icon">
                    {{--<li><a href="#"><i class="fa fa-twitter"></i></a>--}}
                    {{--</li>--}}
                    <li><a href="https://www.facebook.com/Westwind-Logistic-Solutions-Inc-1592948217679872/"><i
                                    class="fa fa-facebook"></i></a>
                    </li>
                    {{--<li><a href="#"><i class="fa fa-linkedin"></i></a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
        {{--<div class="row">--}}
        {{--<div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">--}}
        {{--<p><strong>&copy; 2015 Company Name</strong><br/> consectetur adipisicing elit. Aut eaque, laboriosam--}}
        {{--veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
</section>

<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>
<script src="js/plugins/wow/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/unitegallery/1.7.40/js/unitegallery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/unitegallery/1.7.40/themes/tiles/ug-theme-tiles.min.js"></script>
<script>

    $(document).ready(function () {
        $('.owl-carousel').owlCarousel({
            items: 4,
            lazyLoad: true,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true
        });
        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 80
        });

        // Page scrolling feature
        $('a.page-scroll').bind('click', function (event) {
            var link = $(this);
            $('html, body').stop().animate({
                scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
            $("#navbar").collapse('hide');
        });
    });

    var cbpAnimatedHeader = (function () {
        var docElem = document.documentElement,
            header = document.querySelector('.navbar-default'),
            didScroll = false,
            changeHeaderOn = 200;

        function init() {
            window.addEventListener('scroll', function (event) {
                if (!didScroll) {
                    didScroll = true;
                    setTimeout(scrollPage, 250);
                }
            }, false);
        }

        function scrollPage() {
            var sy = scrollY();
            if (sy >= changeHeaderOn) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }

        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }

        init();

    })();

    // Activate WOW.js plugin for animation on scrol
    new WOW().init();
    $(document).ready(function () {
        $('.image-link-gallery').magnificPopup({
            type: 'image',
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below

            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function

                // The "opener" function should return the element from which popup will be zoomed in
                // and to which popup will be scaled down
                // By defailt it looks for an image tag:
                opener: function (openerElement) {
                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            },
            gallery: {
                enabled: true
            }

        });
        $('.image-link').magnificPopup({
            type: 'image',
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below

            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function

                // The "opener" function should return the element from which popup will be zoomed in
                // and to which popup will be scaled down
                // By defailt it looks for an image tag:
                opener: function (openerElement) {
                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            },
            gallery: {
                enabled: true
            }

        });
        $("#gallery").unitegallery({
            tiles_type: "justified"
        });
    });
</script>

</body>

</html>
