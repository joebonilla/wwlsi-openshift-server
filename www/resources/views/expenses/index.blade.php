@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Expense Types</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Expense Type List</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="tabs-container tab-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> <i
                                    class="fa fa-truck"></i>Transaction Expense</a></li>
                    <li><a data-toggle="tab" href="#fuel-expense-tab" aria-expanded="true"> <i class="fa fa-truck"></i>Fuel
                            Cost</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-9 m-b-xs text-left">
                                        <div class="row">
                                            <a href="{{ route('expenses.create') }}" class="btn btn-primary btn-xs">Add
                                                Expense Type</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID#</th>
                                        <th>Title</th>
                                        <th>Cost</th>
                                        <th>Default</th>
                                        <th>Description</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($expenses as $expense)
                                        <tr>
                                            <td>{{ $expense->id }}</td>
                                            <td>
                                                <a href="{{ route('expenses.edit', $expense->id) }}">{{ $expense->title }}</a>
                                            </td>
                                            <td>{{ $expense->cost }}</td>
                                            <td>{{ $expense->default_text }}</td>
                                            <td>{{ $expense->description }}</td>
                                            <td>{{ $expense->created_at->toDayDateTimeString() }}</td>
                                            <td>
                                                <a class="btn btn-xs btn-info"
                                                   href="{{ route('expenses.edit', $expense->id) }}">Edit</a>
                                                <a class="btn btn-xs btn-danger delete-expense modal-toggle"
                                                   data-modal-target="#deleteExpense"
                                                   data-action="{{ route('expenses.destroy', $expense->id) }}" href="#">Delete</a>
                                            </td>
                                        </tr>
                                        {{--@empty--}}
                                        {{--<tr>--}}
                                        {{--<td class="text-center" colspan="7">No expense type registered.</td>--}}
                                        {{--</tr>--}}
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{--<div class="row text-center">--}}
                            {{--{{ $expenses->links() }}--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    @include('expenses.index_fuel_expense')
                </div>
            </div>
        </div>
    </div>
    @include('expenses.index_delete_expense')
@endsection