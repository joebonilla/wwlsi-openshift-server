@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Add Expense Type</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('expenses.index') }}">All Expenses</a>
                </li>
                <li class="active">
                    <strong>Add Expense Type</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Add Expense Type Form</h5>
                    </div>
                    <div class="ibox-content">
                        @include('layouts.error')
                        <form method="post" class="form-horizontal" action="{{ route('expenses.store') }}">
                            {{ csrf_field() }}
                            <div class="form-group"><label class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10"><input type="text" name="title" class="form-control" value="{{old('title')}}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Default Cost</label>
                                <div class="col-sm-10"><input type="text" name="cost" class="form-control"value="{{old('cost')}}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Default</label>
                                <div class="col-sm-10" style="padding-top: 7px;">
                                    <input type="checkbox" name="default" value="1">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10"><input type="text" name="description"
                                                              class="form-control input-lg" value="{{old('desciption')}}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('trucks.index') }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
