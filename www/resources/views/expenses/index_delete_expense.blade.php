<div class="modal fade" id="deleteExpense" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="post" action="">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Expense</h4>
                </div>
                <div class="modal-body" id="addExpenseContainer">
                    <h3>Are you sure you want to delete this expense?</h3>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Delete Expense</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>