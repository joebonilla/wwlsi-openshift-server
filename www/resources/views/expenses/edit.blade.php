@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Edi Expense Type</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Edit Expense Type</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit Expense Type Form</h5>
                    </div>
                    <div class="ibox-content">
                        @include('layouts.error')
                        <form method="post" class="form-horizontal" action="{{ route('expenses.update', $expense->id) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group"><label class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" value="{{ $expense->title }}" name="title" class="form-control">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Default Cost</label>
                                <div class="col-sm-10">
                                    <input type="text" value="{{ $expense->cost }}" name="cost" class="form-control">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Default</label>
                                <div class="col-sm-10" style="padding-top: 7px;">
                                    <input type="checkbox" @if($expense->default) checked @endif name="default" value="1">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10"><input type="text" value="{{ $expense->description }}" name="description" class="form-control input-lg"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('trucks.index') }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection