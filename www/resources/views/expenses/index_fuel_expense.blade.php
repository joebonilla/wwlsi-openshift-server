<div id="fuel-expense-tab" class="tab-pane">
    <div class="panel-body">
        <form method="post" class="form-horizontal" action="{{ route('store_fuel_cost') }}">
            {{ csrf_field() }}
            <div class="form-group"><label class="col-sm-3 control-label">Current Fuel Cost Per Litre:</label>
                <div class="col-sm-4 input-group"><span class="input-group-addon">₱</span><input type="text" name="cost" class="form-control" value="{{ $currentFuelExpense->cost or 0 }}"></div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group"><label class="col-sm-3 control-label">Last Updated By:</label>
                <div class="col-sm-4 input-group"><input type="text" name="cost" class="form-control" disabled value="{{ $currentFuelExpense->user->name or "" }}"></div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" type="submit">Update</button>
                </div>
            </div>
        </form>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>User</th>
                    <th>Cost</th>
                </tr>
                </thead>
                <tbody>
                @forelse($fuel_expenses as $fuel_expense)
                    <tr>
                        <td>{{ $fuel_expense->created_at->toDayDateTimeString() }}</td>
                        <td>{{ $fuel_expense->user->name }}</td>
                        <td>₱ {{ $fuel_expense->cost }}</td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="3">No Fuel expense registered.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        {{--<div class="row text-center">--}}
            {{--{{ $fuel_expenses->links() }}--}}
        {{--</div>--}}
    </div>
</div>