<div class="tab-pane" id="sales-tab">
    <div class="feed-activity-list">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-3"><h3>Sales</h3></div>
        </div>

        <div class="row">
            <div class="col-xs-offset-1 col-xs-3">Base Charge</div>
            <div class="col-xs-3"><span style="color: green;">+ ₱ {{ number_format($transaction->base_earn_sales, 2) }}</span></div>
        </div>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-3"><a href="{{ route('expenses.index') . '#fuel-expense-tab' }}">Fuel Expense ({{ $transaction->kilometer }} km / {{ ceil($transaction->kilometer) }} litre)</a></div>
            <div class="col-xs-3"><span style="color: red;">- ₱ {{ number_format($transaction->current_fuel_cost, 2)}}</span></div>
        </div>
        @foreach($transaction->transaction_expenses as $expense)
            @if($expense->liquidation_status == \App\TransactionExpense::STATUS_APPROVED)
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-3">{{ $expense->title }}</div>
                    <div class="col-xs-3">{!! $expense->cost_sales !!}</div>
                </div>
            @endif
        @endforeach
        @if($transaction->with_vat)
            <div class="row">
                <div class="col-xs-offset-1 col-xs-3">Base Charge VAT ({{ $transaction->vat_percentage_label }})</div>
                <div class="col-xs-3"><span style="color: green;">+ ₱ {{ number_format($transaction->total_vat, 2) }}</span></div>
            </div>
        @endif
        <div class="row">
            <div class="col-xs-offset-1 col-xs-6"><hr></div>
        </div>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-3">Total</div>
            <div class="col-xs-3">&nbsp; ₱ {{ number_format($transaction->total_earn, 2) }}</div>
        </div>
    </div>
</div>