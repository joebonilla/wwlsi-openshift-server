<div class="modal fade" id="removeCost" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="post" action="">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Remove Cost</h4>
                </div>
                <div class="modal-body" id="addExpenseContainer">
                    <h3>Are you sure you want to remove this cost?</h3>
                    <input type="hidden" name="transaction_id" value="{{ $transaction->id }}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Remove Cost</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>