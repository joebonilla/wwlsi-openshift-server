<div class="modal fade" id="expenseModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="post" action="{{ route('add_expense', $transaction->id) }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Expense</h4>
                </div>
                <div class="modal-body" id="addExpenseContainer">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Expense Type</label>
                        <div class="col-sm-6">
                            <select id="selectExpense" class="form-control" required>
                                <option value="">Select Expense Type</option>
                                @foreach($expenses as $expense)
                                    <option value="{{ $expense->id }}">{{ $expense->title }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="title">
                        </div>
                        <div class="col-sm-3" style="padding-top: 7px;">
                            <a class="btn btn-primary btn-xs" href="{{ route('expenses.create') }}">Add New Type</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cost</label>
                        <div class="col-sm-9">
                            <input type="text" name="cost" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Reflect on sales</label>
                        <div class="col-sm-9" style="padding-top: 7px;">
                            <input type="checkbox" name="reflect_on_sales" value="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Note</label>
                        <div class="col-sm-9">
                            <input type="text" name="note" class="form-control">
                        </div>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Expense</button>
                </div>
            </div>
        </form>
    </div>
</div>