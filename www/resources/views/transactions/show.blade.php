@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Transaction</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('transactions.index') }}">All Transactions</a>
                </li>
                <li class="active">
                    <strong>Transaction Details</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="ibox">
                    <div class="ibox-content">
                        @include('transactions.show_detail')
                        <div class="row m-t-sm">
                            <div class="col-lg-12">
                                <div class="panel blank-panel tab-container">
                                    <div class="panel-heading">
                                        <div class="panel-options">
                                            <ul class="nav nav-tabs">
                                                @if($transaction->status == \App\Transaction::$status_complete)
                                                    <li class="active"><a href="#remarks-tab" data-toggle="tab"
                                                                          aria-expanded="true">Remarks</a></li>
                                                    <li class=""><a href="#note-tab" data-toggle="tab"
                                                                    aria-expanded="false">Note</a></li>
                                                @else
                                                    <li class="active"><a href="#note-tab" data-toggle="tab"
                                                                          aria-expanded="false">Note</a></li>
                                                @endif
                                                <li class=""><a href="#expense-tab" data-toggle="tab"
                                                                aria-expanded="true">Expenses</a></li>
                                                <li class=""><a href="#activity-tab" data-toggle="tab"
                                                                aria-expanded="true">Activity</a></li>
                                                <li class=""><a href="#sales-tab" data-toggle="tab"
                                                                aria-expanded="true">Sales</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            @if($transaction->status == \App\Transaction::$status_complete)
                                                @include('transactions.show_remarks')
                                            @endif

                                            @include('transactions.show_notes')
                                            @include('transactions.show_expense')
                                            @include('transactions.show_activity')
                                            @include('transactions.show_sales')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('transactions.show_modal_expense')
    @include('transactions.show_modal_status')
    @include('transactions.show_modal_note')
    @include('transactions.show_modal_remarks')
    @include('transactions.show_modal_removecost')
    @include('transactions.show_modal_release_chassis')
@endsection

@section('scripts')
    <script src="/js/customjs/transactions.js"></script>
@endsection