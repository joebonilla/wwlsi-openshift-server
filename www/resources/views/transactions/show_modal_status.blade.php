<div class="modal fade" id="statusModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="post" action="{{ route('update_status', $transaction->id) }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Change Status</h4>
                </div>
                <div class="modal-body" id="addExpenseContainer">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-9">
                            <select id="selectExpense" name="status" class="form-control" required>
                                <option value="0" @if($transaction->status == \App\Transaction::$status_new) selected @endif>{{ $transaction->convertStatusToText(\App\Transaction::$status_new) }}</option>
                                <option value="1" @if($transaction->status == \App\Transaction::$status_on_going) selected @endif>{{ $transaction->convertStatusToText(\App\Transaction::$status_on_going) }}</option>
                                <option value="2" @if($transaction->status == \App\Transaction::$status_complete) selected @endif>{{ $transaction->convertStatusToText(\App\Transaction::$status_complete) }}</option>
                            </select>
                            <span class="help-block m-b-none">Warning: This transaction cannot change status anymore once you complete this transaction.</span>
                        </div>
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Status</button>
                </div>
            </div>
        </form>
    </div>
</div>