<div class="tab-pane @if($transaction->status == \App\Transaction::$status_complete) active @endif" id="remarks-tab">
    <div class="feed-activity-list">
        <div class="col-md-12 m-b-xs text-right">
            <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#addRemarksModal">Add Remarks</a>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Date</th>
                <th>User</th>
                <th>Note</th>
            </tr>
            </thead>
            <tbody>
            @forelse($transaction->transaction_remarks as $transaction_remark)
                <tr>
                    <td>{{ $transaction_remark->created_at->toDayDateTimeString() }}</td>
                    <td>{{ $transaction_remark->user->name }}</td>
                    <td>{{ $transaction_remark->note }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="3">No remarks for this transaction</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>