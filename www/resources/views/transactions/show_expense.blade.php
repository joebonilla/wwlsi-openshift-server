<div class="tab-pane" id="expense-tab">
    <div class="expense-list">
        <div class="col-md-2 m-b-xs text-right">
            <span class="label label-primary" style="background-color: #d9edf7;">&nbsp;</span> Reflect on sales
        </div>
        <div class="col-md-10 m-b-xs text-right">
            <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#expenseModal">Request Expense</a>
            <a href="{{url('system/operations/exportExpenseToPdf/' . $transaction->id)}}" class="btn btn-xs btn-info"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
        </div>
        <table class="table" id="transaction-expense">
            <thead>
            <tr>
                <th>Date Added</th>
                <th>Title</th>
                <th>Cost</th>
                <th>Liquidation Status</th>
                <th>Note</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($transaction->transaction_expenses as $transaction_expense)
                <tr @if($transaction_expense->reflect_on_sales) class="info" @endif>
                    <td>{{ $transaction_expense->created_at->toDayDateTimeString() }}</td>
                    <td>{{ $transaction_expense->title }}</td>
                    <td><span style="color: red">- ₱ {{ $transaction_expense->cost }}</span></td>
                    <td>@if($transaction_expense->liquidation_status == \App\TransactionExpense::STATUS_APPROVED) Approved @else Pending @endif</td>
                    <td>{{ $transaction_expense->note }}</td>
                    <td>
                        @if($transaction_expense->liquidation_status != \App\TransactionExpense::STATUS_APPROVED)
                            <a href="#" class="btn btn-link remove-cost" data-delete-action="{{ route('remove_cost', $transaction_expense->id) }}">
                                <span><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <hr />
        <div class="col-md-12">
            <h4>Total Requested Expense: <strong>₱ {{ $total_expense_requests }}</strong></h4>
            <h4>Total Liquidated Expense: <strong>₱ {{ $total_liquidated_expense }}</strong></h4>
        </div>
    </div>
</div>