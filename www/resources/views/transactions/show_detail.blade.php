<div class="row">
    <div class="col-lg-12">
        <div class="m-b-md">
            <a href="{{ route('transactions.edit', $transaction->id )}}" class="btn btn-white btn-xs pull-right">Edit
                details</a>
            <h2>Ref# {{ $transaction->ref_no }}</h2>
        </div>
        <dl class="dl-horizontal">
            <dt>Status:</dt>
            <dd>
                <span class="label label-primary" data-toggle="modal"
                      @if(!$transaction->isComplete()) data-target="#statusModal" @endif>
                    {{ $transaction->status_label }}
                </span>
                @if(!$transaction->isComplete())
                    <small>(click to change status)</small>
                @endif
            </dd>
        </dl>
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        <dl class="dl-horizontal">
            <dt>Created by:</dt>
            <dd>
                <a href="{{ route('users.show', $transaction->created_by->id) }}">{{ $transaction->created_by->name }}</a>
            </dd>
            <dt>Client:</dt>
            <dd><a href="{{ route('clients.show', $transaction->client->id) }}">{{ $transaction->client->name }}</a>
            </dd>
            <dt>Customer:</dt>
            <dd>
                <a href="{{ route('customers.show', $transaction->customer->id) }}">{{ $transaction->customer->name }}</a>
            </dd>
            <dt>Driver:</dt>
            <dd><a href="{{ route('users.show', $transaction->driver->id) }}">{{ $transaction->driver->name }}</a></dd>
            <dt>Helper:</dt>
            <dd><a href="{{ route('users.show', $transaction->helper->id) }}">{{ $transaction->helper->name }}</a></dd>
            <dt>Truck:</dt>
            <dd><a href="{{ route('trucks.show', $transaction->truck->id) }}">{{ $transaction->truck->name }}</a></dd>
            <dt>Transport type:</dt>
            <dd>  {{ $transaction->transport_type }}</dd>
        </dl>
    </div>
    <div class="col-lg-7" id="cluster_info">
        <dl class="dl-horizontal">
            @if($transaction->container)
                <dt>Container Ref#:</dt>
                <dd>  {{ $transaction->container->container_ref_id }}</dd>
                <dt>Container type:</dt>
                <dd>
                    <a href="{{ route('containerTypes.show', $transaction->container->containerType->id) }}">{{ $transaction->container->containerType->name }}</a>
                </dd>
                @if($transaction->isComplete())
                    @if(!empty($transaction->transaction_chassis))
                        @if($transaction->transaction_chassis->status == \App\TransactionChassis::ON_USE)
                            <dt>Chassis:</dt>
                            <dd>{{ $transaction->transaction_chassis->chassis->name }} <a href="#" class="btn btn-primary btn-xs" data-toggle="modal"
                                   data-target="#releaseChassis">Release</a></dd>
                        @else
                            <dt>Chassis:</dt>
                            <dd>{{ $transaction->transaction_chassis->chassis->name }} (Released at {{ $transaction->transaction_chassis->updated_at->toDateString() }})</dd>
                        @endif
                    @endif
                @else

                    @if(!empty($transaction->transaction_chassis))
                        <dt>Chassis:</dt>
                        <dd>{{ @$transaction->transaction_chassis->chassis->name }}</dd>
                    @endif
                @endif
            @endif
            <dt>Origin:</dt>
            <dd>{{ $transaction->origin_address }}</dd>
            <dt>Destination:</dt>
            <dd>{{ $transaction->destination_address }}</dd>
            <dt>Last Updated:</dt>
            <dd>{{ $transaction->updated_at->toDayDateTimeString() }}</dd>
            <dt>Created:</dt>
            <dd>{{ $transaction->created_at->toDayDateTimeString() }}</dd>
            @if($transaction->pre_advice_status != \App\Transaction::PREADVICE_OUTSIDE_CY)
                <dt>Pre-advice:</dt>
                <dd>
                    <form action="{{ route('update_pre_advice', $transaction->id) }}" method="post">
                        {{ csrf_field() }}
                        @if($transaction->pre_advice_status == \App\Transaction::PREADVICE_NO)
                            <input type="hidden" name="pre_advice_status" value="{{ \App\Transaction::PREADVICE_YES }}">
                            <button type="submit" class="btn btn-warning btn-xs change-preadvice-status">No</button>
                        @else
                            <input type="hidden" name="pre_advice_status" value="{{ \App\Transaction::PREADVICE_NO }}">
                            <button type="submit" class="btn btn-primary btn-xs change-preadvice-status">Yes</button>
                        @endif
                    </form>
                </dd>
            @endif
        </dl>
    </div>
    @if(!empty($transaction->note))
        <div class="col-lg-12">
            <dl class="dl-horizontal">
                <dt>Note:</dt>
                <dd>{{ $transaction->note }}</dd>
            </dl>
        </div>
    @endif
</div>