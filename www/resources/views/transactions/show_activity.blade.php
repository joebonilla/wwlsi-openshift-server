<div class="tab-pane" id="activity-tab">
    <div class="feed-activity-list">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Date</th>
                <th>User</th>
                <th>Note</th>
            </tr>
            </thead>
            <tbody>
            @forelse($transaction_note_audit as $audit)
                @if($audit->displayAudit())
                    <tr>
                        <td>{{ $audit->created_at->toDayDateTimeString() }}</td>
                        <td>{{ $audit->user->name }}</td>
                        <td>{!! $audit->displayAudit() !!}</td>
                    </tr>
                @endif
            @empty
                <tr>
                    <td class="text-center" colspan="2">No activities for this transaction</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>