<div class="tab-pane @if(!$transaction->isComplete()) active @endif" id="note-tab">
    <div class="feed-activity-list">
        <div class="col-md-12 m-b-xs text-right">
            <a href="#" class="btn btn-xs btn-primary @if($transaction->isComplete()) disabled @endif" data-toggle="modal" data-target="#addNoteModal">Add Note</a>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Time Added</th>
                <th>User</th>
                <th>Note</th>
            </tr>
            </thead>
            <tbody>
            @forelse($transaction->transaction_notes as $transaction_note)
                <tr>
                    <td>{{ $transaction_note->created_at->toDayDateTimeString() }}</td>
                    <td>{{ $transaction_note->user->name }}</td>
                    <td>{{ $transaction_note->note }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="3">No notes for this transaction</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>