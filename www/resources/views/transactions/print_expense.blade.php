<style>
    @import url("https://fonts.googleapis.com/css?family=Roboto:400,300,500,700");

    table {
        font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }

    .table-bordered {
        border: 1px solid #EBEBEB;
    }

    .table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th {
        border-top: 0;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: 1px solid #e7eaec;
        line-height: 1.42857;
        padding: 8px;
        vertical-align: top;
    }

    .table > thead > tr > th {
        border-bottom: 1px solid #DDDDDD;
        vertical-align: bottom;
    }

    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #e7e7e7;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #F5F5F6;
        border-bottom-width: 1px;
    }

    th {
        text-align: center;
        font-weight: bold;
    }

    table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    tbody {
        display: table-row-group;
        vertical-align: middle;
        border-color: inherit;
    }

    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #e7e7e7;
    }
</style>
<div style="width: 100%;text-align:center;font-family: Raleway, sans-serif">
    <h3>Westwind Logistic Solutions Inc.</h3>

</div>
<div style="clear: both"></div>
<span>Ref #: {{$transaction->id}}</span><br>
<span>Client: {{$transaction->client->name}}</span><br>
<span>Customer: {{$transaction->customer->name}}</span><br>
<span>Driver: {{$transaction->driver->name}}</span><br>
<span>Helper: {{$transaction->helper->name}}</span><br>
<span>Truck: {{$transaction->truck->name}}</span><br>

<div style="clear: both"></div>
<hr>
<table id="payroll-table" class="table table-bordered" style="width: 100%;text-align: center">
    <thead>
    <tr>
        <th>Date Added</th>
        <th>Title</th>
        <th>Cost</th>
        <th>Liquidation Status</th>
    </tr>
    </thead>
	@foreach($transaction->transaction_expenses as $transaction_expense)
	    <tr>
	        <td>{{ $transaction_expense->created_at->toDayDateTimeString() }}</td>
	        <td>{{ $transaction_expense->title }}</td>
	        <td><span style="color: red">- {{ $transaction_expense->cost }}</span></td>
	        <td>@if($transaction_expense->liquidation_status == \App\TransactionExpense::STATUS_APPROVED) Approved @else Pending @endif</td>
	    </tr>
	@endforeach
    <tr>
        <td colspan="3">Total Expense</td>
        <td><span style="color: red">- {{ $total_expense }}</span></td>
    </tr>
    <tr>
        <td colspan="3">Total Liquidated Expense</td>
        <td><span style="color: red">- {{ $total_liquidated_expense }}</span></td>
    </tr>
</table>
<hr>
<div style="float: right">Date Generated: &nbsp; {{\Carbon\Carbon::now()->toFormattedDateString()}}</div>
