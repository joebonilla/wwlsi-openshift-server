<div class="modal fade" id="releaseChassis" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="post" action="{{ route('release_chassis', $transaction->id) }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Release Chassis</h4>
                </div>
                <div class="modal-body">
                	<h3>Are you sure you want to release this chassis?</h3>
                	<p>Note: This procedure cannot be reverted.</p>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Release</button>
                </div>
            </div>
        </form>
    </div>
</div>