<div id="complete-tab" class="tab-pane">
    <div class="panel-body">
        <div class="row">
            <form method="POST" id="search-form" class="form-inline pull-right" role="form">
                <div class="form-group " id="data_5">
                    <div class="input-daterange input-group" id="datepicker">
                        <input type="text" class="input-sm form-control" name="start_search" value="" required/>
                        <span class="input-group-addon">to</span>
                        <input type="text" class="input-sm form-control" name="end_search" value="" required/>
                    </div>
                </div>
                <button id="btn-submit" type="submit" class="btn btn-primary btn-sm">Search</button>
            </form>
        </div>

        <div class="table-responsive">

            <table id="complete-table-transaction" class="table table-striped table-hover"
                   style="width: 100%!important;">
                <thead>
                <tr>
                    <th>Ref#</th>
                    <th>Client</th>
                    <th>Customer</th>
                    <th>Container Ref. ID / Name</th>
                    <th>Destination</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Date Completed</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
