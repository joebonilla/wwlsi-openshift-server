<div id="ongoing-tab" class="tab-pane active">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="col-md-9 m-b-xs text-left">
                    <div class="row">
                        <a href="{{ route('transactions.create') }}" class="btn btn-primary btn-xs">Add Transaction</a>
                        <a href="#" class="btn btn-primary btn-xs hidden" data-toggle="modal"
                           data-target="#importTransaction">Import Transaction</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 m-b-xs">
                <div data-toggle="buttons" class="btn-group" id="on-going-route" data-ogdt-route="{{ route('transactions_ongoing_data') }}">
                    <label class="btn btn-sm btn-white range-picker" data-start="{{ $dates['day']['start'] }}" data-end="{{ $dates['day']['end'] }}"> <input type="radio" id="day" name="range-picker" value="1"> Day </label>
                    <label class="btn btn-sm btn-white range-picker" data-start="{{ $dates['week']['start'] }}" data-end="{{ $dates['week']['end'] }}"> <input type="radio" id="week" name="range-picker" value="2"> Week </label>
                    <label class="btn btn-sm btn-white range-picker" data-start="{{ $dates['month']['start'] }}" data-end="{{ $dates['month']['end'] }}"> <input type="radio" id="month" name="range-picker" value="3"> Month </label>
                    <label class="btn btn-sm btn-white range-picker active"> <input type="radio" id="all" name="range-picker" value="4"> All</label>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="ongoing-table" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Ref#</th>
                    <th>Client</th>
                    <th>Customer</th>
                    <th>Container Ref. ID / Name</th>
                    <th>Destination</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Date Added</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>