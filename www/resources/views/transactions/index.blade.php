@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Transactions</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('transactions.index') }}">All Transactions</a>
                </li>
                <li class="active">
                    <strong>Transactions</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Total</span>
                        <h5>Transactions</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ $analytics_data['total_transactions_count'] }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Total</span>
                        <h5>New/On-going</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ $analytics_data['total_ongoing_transactions_count'] }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">Monthly</span>
                        <h5>Completed</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ $analytics_data['total_completed_transactions_count'] }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Today</span>
                        <h5>Fuel Cost</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">₱ {{ $analytics_data['current_fuel_expense']->cost or 0 }} <a
                                    href="{{ route('expenses.index') . '#fuel-expense-tab' }}"
                                    class="btn btn-xs btn-save"><i class="fa fa-pencil"></i> Update</a></h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 m-b-md">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#ongoing-tab" aria-expanded="true"> <i
                                        class="fa fa-truck"></i>New/On-Going</a></li>
                        <li class=""><a data-toggle="tab" href="#complete-tab" aria-expanded="false"><i
                                        class="fa fa-folder"></i>Completed</a></li>
                    </ul>
                    <div class="tab-content">
                        @include('transactions.index_ongoing')
                        @include('transactions.index_complete_tab')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('transactions.index_modal_import_transaction')
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
            });

            let config_complete = [
                {data: 'id', name: 'id'},
                {data: 'client.name', name: 'client.name', orderable: false},
                {data: 'customer.name', name: 'customer.name', orderable: false},
                {data: 'container', name: 'container', orderable: false, searchable: false},
                {data: 'destination_address', name: 'destination_address', orderable: false,},
                {data: 'type', name: 'type'},
                {data: 'status', name: 'status', orderable: false, searchable: false},
                {data: 'date_completed', name: 'date_completed'},
            ];

            let btn_config = [
                {extend: 'copy'},
                {extend: 'excel', className: 'btn-sm btn-info'},
                {extend: 'pdf', className: 'btn-sm btn-success'},

                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ];

            let oTable = $('#complete-table-transaction').DataTable({
                serverSide: true,
                processing: true,
                order: [0, 'desc'],
                pagingType:'full_numbers',
                ajax: {
                    url: '{{route('transactions_complete_data')}}',
                    data: function (d) {
                        d.start_search = $('input[name=start_search]').val();
                        d.end_search = $('input[name=end_search]').val();
                    }
                },
                columns: config_complete,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: btn_config,
            });
            $('#btn-submit').on('click', function () {
                $('#search-form').on('submit', function (e) {
                    oTable.draw();
                    e.preventDefault();
                });
            });
        });
    </script>

    <script src="/js/customjs/transactions.js"></script>
@endsection