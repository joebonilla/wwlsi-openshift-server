@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Edit Transactions</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('transactions.index') }}">All Transactions</a>
                </li>
                <li class="active">
                    <strong>Edit Transactions</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit Transaction Form</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" action="{{ route('transactions.update', $transaction->id) }}">
                            {{ csrf_field() }}
                            <div class="row">
                                @include('layouts.error')
                            </div>
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group"><label class="col-sm-2 control-label">Client</label>
                                <div class="col-sm-10">
                                    <select name="client_id" class="form-control">
                                        <option value="">Select Client</option>
                                        @foreach($clients as $client)
                                            <option value="{{ $client->id }}" @if($transaction->client->id == $client->id) selected @endif>{{ $client->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Origin</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="port_id">
                                        <option value="">Select Port Area</option>
                                        @foreach($ports as $port)
                                            <option value="{{ $port->id }}" @if($transaction->port_id == $port->id) selected @endif> {{ $port->name }}</option>
                                        @endforeach
                                    </select>
                                    <input type="text" name="origin_address" placeholder="Origin" class="form-control" readonly value="{{ $transaction->origin_address }}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Customer</label>
                                <div class="col-sm-10">
                                    <select name="customer_id" class="form-control">
                                        <option value="">Select Customer</option>
                                        @foreach($transaction->client->customers as $customer)
                                            <option value="{{ $customer->id }}" @if($customer->id == $transaction->customer_id) selected @endif>{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                    <input type="text" name="destination_address" placeholder="Destination" class="form-control" readonly value="{{ $transaction->destination_address }}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Driver</label>
                                <div class="col-sm-10">
                                    <select name="driver_id" class="form-control">
                                        <option value="">Select Driver</option>
                                        @foreach($drivers as $driver)
                                            <option value="{{ $driver->id }}" @if($transaction->driver->id == $driver->id) selected @endif>{{ $driver->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Helper</label>
                                <div class="col-sm-10">
                                    <select name="helper_id" class="form-control">
                                        <option value="">Select Helper</option>
                                        @foreach($helpers as $helper)
                                            <option value="{{ $helper->id }}" @if($transaction->helper->id == $helper->id) selected @endif>{{ $helper->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Truck</label>
                                <div class="col-sm-10">
                                    <select name="truck_id" class="form-control">
                                        <option value="">Select Truck</option>
                                        @foreach($trucks as $truck)
                                            <option value="{{ $truck->id }}" @if($transaction->truck_id == $truck->id) selected @endif>{{ $truck->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">With Pre-advice</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-10" style="padding-top: 7px;">
                                        <input type="checkbox" name="pre_advice_status" @if($transaction->pre_advice_status != \App\Transaction::PREADVICE_OUTSIDE_CY) checked @endif value="{{ \App\Transaction::PREADVICE_NO }}">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Transport Type</label>
                                <div class="col-sm-10">
                                    <select name="type" class="form-control">
                                        <option value="{{ \App\Transaction::LCL }}">LCL</option>
                                        <option value="{{ \App\Transaction::FCL }}"  @if($transaction->isFCL()) selected @endif>FCL</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group tc-container"><label class="col-sm-2 control-label">Transport Container</label>
                                <div class="col-sm-10">
                                    <input type="text" name="container_ref_id" placeholder="Container Ref#" value="{{ $transaction->container->container_ref_id or '' }}" class="form-control m-b-none">
                                    <span class="help-block m-b">If there are more than one container, separate them with forward slash (/).</span>
                                    <select class="form-control" name="container_type_id">
                                        <option value="">Select Container Type</option>
                                        @foreach($containerTypes as $containerType)
                                            <option value="{{ $containerType->id }}" @if(isset($transaction->container->id) && ($transaction->container->container_type_id == $containerType->id)) selected @endif>{{ $containerType->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block"><a href="{{ route('containerTypes.create') }}">Click here to add new container type.</a></span>
                                    <select class="form-control m-b-none" name="chassis_id">
                                        <option value="">Select Chassis</option>
                                        @foreach($chassis as $chasse)
                                            <option value="{{ $chasse->id }}" @if(isset($transaction->transaction_chassis->id) && ($transaction->transaction_chassis->chassis_id == $chasse->id)) selected @endif>{{ $chasse->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed tc-container"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">With VAT</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-10" style="padding-top: 7px;">
                                        <input type="checkbox" name="with_vat" value="1" @if($transaction->with_vat) checked @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group vat-container"><label class="col-sm-2 control-label">VAT Percent</label>
                                <div class="col-sm-10">
                                    <select name="vat_percent" class="form-control">
                                        <option value=0 @if($transaction->vat_percent == 0) selected @endif>0%</option>
                                        <option value=0.06 @if($transaction->vat_percent == 0.06) selected @endif>6%</option>
                                        <option value=0.12 @if($transaction->vat_percent == 0.12) selected @endif>12%</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed vat-container"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Note</label>
                                <div class="col-sm-10"><input type="text" value="{{ $transaction->note }}" name="note" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('transactions.show', $transaction->id) }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/customjs/transactions.js"></script>
@endsection