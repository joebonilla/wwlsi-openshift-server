@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Truck Information</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('trucks.index') }}">All Trucks</a>
                </li>
                <li class="active">
                    <strong>Truck Profile</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Truck Detail</h5>
                        <div class="pull-right">
                            <a href="{{ route('trucks.edit', $truck->id) }}"
                               class="btn btn-primary btn-xs "><i
                                        class="fa fa-pencil"></i> Edit</a>
                            <a href="#"
                               class="btn btn-info btn-xs" data-toggle="modal" data-target="#repairModal"><i
                                        class="fa fa-plus"></i> Add Repair</a>
                        </div>
                    </div>
                    <div class="ibox-content profile-content">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="m-b-xs">
                                    <span>Name:</span> <span class="">{{ $truck->name }}</span>
                                </h4>
                                <h4 class="m-b-xs">
                                    <span>Manufacturer:</span> <span class="">{{ $truck->manufacturer }}</span>
                                </h4>
                                <h4 class="m-b-xs">
                                    <span>Plate Number:</span> <span class="">{{ $truck->plate_no }}</span>
                                </h4>
                                <h4 class="m-b-xs">
                                    <span>Description:</span> <span class="">{{ $truck->description }}</span>
                                </h4>
                                <h4 class="m-b-xs">
                                    <span>Status:</span> <span
                                            class="label label-primary">{{ $truck->status->name }}</span>
                                </h4>
                                <hr>
                            </div>

                            @if($total_cost > 0)
                                <div class="col-md-12">
                                    <h3 class="m-b-xs">
                                        <span>Repair History:</span></h3><span class="text-muted"><i>(Click the row you want to edit)</i></span>
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Repair Description</th>
                                            <th>Date Repair</th>
                                            <th>Requested by</th>
                                            <th>Contractor Name</th>
                                            <th>Status</th>
                                            <th>Cost</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($truck->repair_histories as $repair_history)
                                            <tr class="truckEdit" data-toggle="modal"
                                                data-target="#repairEditModal"
                                                data-id="{{ $repair_history->id }}"
                                                data-repair="{{ $repair_history->repair_description }}"
                                                data-date="{{ $repair_history->date_repair }}"
                                                data-contractor="{{ $repair_history->contractor_name }}"
                                                data-cost="{{ $repair_history->cost }}">

                                                <td>{{ $repair_history->id }}</td>
                                                <td>{{ $repair_history->repair_description }}</td>
                                                <td>{{ $repair_history->repair_date}}</td>
                                                <td>{{ $repair_history->users->name}}</td>
                                                <td>{{ $repair_history->contractor_name }}</td>
                                                <td>{{ $repair_history->repair_status }}</td>
                                                <td>{{ money_format('%i', $repair_history->cost)  }}</td>

                                            </tr>
                                        @endforeach
                                        {{--<tr>--}}
                                        {{--<td colspan="6">Total Cost</td>--}}
                                        {{--<td class="">{{money_format('%i',$total_cost)}}</td>--}}
                                        {{--</tr>--}}
                                        </tbody>
                                    </table>
                                    <h3 class="m-b-xs pull-right">
                                    <span>Total Repair: <strong>{{money_format('%i',$total_cost)}}</strong>
                                    </h3>
                                </div>

                            @endif

                            @if(!$transaction_history->isEmpty())
                                <div class="col-md-12">
                                    <h3 class="m-b-xs">
                                        <span>Transaction History:</span>
                                    </h3>
                                    <table class="table table-hover table-bordered">
                                        <tbody>
                                        @foreach($transaction_history as $transactions)
                                            <tr>
                                                <td>
                                                    <a href="{{route('transactions.show', $transactions->id)}}">Ref
                                                        # {{ $transactions->id }}</a>
                                                </td>
                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                    {{--{{$transaction_history->appends(Request::only('q'))->links()}}--}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="repairModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" class="form-horizontal" action="{{ route('truckrepairs.store') }}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Truck Repair</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="repair_description" class="form-control"
                                           placeholder="Repair Description" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="contractor_name" class="form-control"
                                           placeholder="Contractor Name" required>
                                </div>
                                <div class="form-group">
                                    <input type="number" name="cost" class="form-control" placeholder="Cost" required>
                                </div>
                                <div class="form-group">
                                    <input type="date" class="form-control" name="date_repair"
                                           placeholder="Date Repaired" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Save</button>
                    </div>
                    <input type="hidden" name="truck_id" value="{{$truck->id}}">
                    <input type="hidden" name="status" value="{{\App\TruckRepairHistory::$status_pending}}">
                    <input type="hidden" name="request_user_id" value="{{Auth()->id()}}">
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="repairEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Truck Repair
                    </h4>
                    <form method="post" id="form-delete" class="form-horizontal" action="">
                        {{ csrf_field() }}
                        <input type="hidden" name="truck_id" value="{{$truck->id}}">
                        <input name="_method" type="hidden" value="DELETE">
                        <button type="submit" class="btn btn-danger btn-xs">Delete Record</button>
                    </form>

                </div>
                <form method="post" id="form-edit" class="form-horizontal" action="">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="repair_description" class="form-control"
                                           placeholder="Repair Description" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="contractor_name" class="form-control"
                                           placeholder="Contractor Name" required>
                                </div>
                                <div class="form-group">
                                    <input type="number" name="cost" class="form-control" placeholder="Cost" required>
                                </div>
                                <div class="form-group">
                                    <input type="date" class="form-control" name="date_repair"
                                           placeholder="Date Repaired" required>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="truck_id" value="{{$truck->id}}">
                        <input type="hidden" name="status" value="{{\App\TruckRepairHistory::$status_pending}}">
                        <input type="hidden" name="request_user_id" value="{{Auth()->id()}}">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Update Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <style>
        .truckEdit {
            cursor: pointer;
        }
    </style>
@endsection
@section('scripts')
    <script src="/js/customjs/trucks.js"></script>
@endsection