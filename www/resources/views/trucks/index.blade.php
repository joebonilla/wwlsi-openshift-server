@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Trucks</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Truck List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Trucks table</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-9 m-b-xs text-left">
                                        <a href="{{ route('trucks.create') }}" class="btn btn-primary btn-xs">Add
                                            Truck</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif


                        <table id="users-table" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Manufacturer</th>
                                <th>Plate number</th>
                                <th>Status</th>
                                <th width="280px">Action</th>
                            </tr>
                            </thead>
                            @foreach($trucks as $truck)
                                <tr>
                                    <td>{{ $truck->name }}</td>

                                    <td>{{ $truck->manufacturer }}</td>
                                    <td>{{ $truck->plate_no }}</td>
                                    <td><span class="label label-primary">{{ $truck->status->name }}</span>

                                    <td>
                                        <a class="btn btn-xs btn-info"
                                           href="{{ route('trucks.show', $truck->id) }}">Show</a>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('trucks.edit',$truck->id) }}">Edit</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['trucks.destroy',$truck->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{--<div class="col-md-12 text-right">{{ $trucks->appends(Request::only('q'))->links('vendor.pagination.bootstrap-4') }}</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection