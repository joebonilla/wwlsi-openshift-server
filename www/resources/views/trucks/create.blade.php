@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Add Truck</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('trucks.index') }}">All Trucks</a>
                </li>
                <li class="active">
                    <strong>Add Truck</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Add Truck Form</h5>
                    </div>
                    <div class="ibox-content">
                        @include('layouts.error')
                        <form method="post" class="form-horizontal" action="{{ route('trucks.store') }}">
                            {{ csrf_field() }}
                            <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10"><input type="text" name="name" class="form-control"
                                                              placeholder="Truck Name" value="{{old('name')}}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Manufacturer</label>
                                <div class="col-sm-10"><input type="text" name="manufacturer" class="form-control"
                                                              placeholder="Manufacturer"
                                                              value="{{old('manufacturer')}}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Plate No.</label>
                                <div class="col-sm-10"><input type="text" name="plate_no" class="form-control"
                                                              placeholder="Plate Number" value="{{old('plate_no')}}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10"><input type="text" name="description"
                                                              class="form-control input-lg"
                                                              placeholder="Truck Description"
                                                              value="{{old('description')}}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            {{--<div id="repair-history-parent">--}}
                                {{--<div class="child-repair-history form-group">--}}
                                    {{--<label class="col-sm-2 control-label">Repair History</label>--}}
                                    {{--<div class="col-sm-5">--}}
                                        {{--<input type="text" name="repair_description[]" class="form-control"--}}
                                               {{--placeholder="Repair Description">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-2">--}}
                                        {{--<input type="number" name="cost[]" class="form-control" placeholder="Cost">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-2 date">--}}
                                        {{--<input type="text" class="form-control" name="date_repair[]"--}}
                                               {{--placeholder="Date Repaired">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-1">--}}
                                        {{--<button id="btn-add" type="button" class="btn btn-sm btn-circle btn-info">--}}

                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('trucks.index') }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/customjs/trucks.js"></script>
@endsection