@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Edit Truck</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('trucks.index') }}">All Trucks</a>
                </li>
                <li class="active">
                    <strong>Edit Truck</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit Truck Form</h5>
                    </div>
                    <div class="ibox-content">
                        @include('layouts.error')
                        <form method="post" class="form-horizontal" action="{{ route('trucks.update', $truck->id) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10"><input type="text" name="name" class="form-control"
                                                              value="{{ $truck->name }}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Manufacturer</label>
                                <div class="col-sm-10"><input type="text" name="manufacturer" class="form-control"
                                                              value="{{ $truck->manufacturer }}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Plate No.</label>
                                <div class="col-sm-10"><input type="text" name="plate_no" class="form-control"
                                                              value="{{ $truck->plate_no }}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10"><input type="text" name="description"
                                                              class="form-control input-lg"
                                                              value="{{ $truck->description }}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Status</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="truck_status_id" id="truck_status_id">
                                        @foreach($status as $statuses)
                                            <option value="{{$statuses->id}}"
                                                    @if($statuses->name == $truck->status->name) selected @endif>{{$statuses->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{--<div class="hr-line-dashed"></div>--}}
                            {{--@if(count($truck->repair_histories) < 1)--}}
                                {{--<div id="repair-history-parent">--}}
                                    {{--<div class="child-repair-history form-group">--}}
                                        {{--<label class="col-sm-2 control-label">Repair History</label>--}}
                                        {{--<div class="col-sm-5">--}}
                                            {{--<input type="text" name="repair_description[]" class="form-control"--}}
                                                   {{--placeholder="Repair Description">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-2">--}}
                                            {{--<input type="number" name="cost[]" class="form-control" placeholder="Cost">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-2 date">--}}
                                            {{--<input type="text" class="form-control" name="date_repair[]"--}}
                                                   {{--placeholder="Date Repaired">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-1">--}}
                                            {{--<button id="btn-add" type="button" class="btn btn-sm btn-circle btn-info">--}}

                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endif--}}
                            {{--<div id="repair-history-parent">--}}
                                {{--@foreach($truck->repair_histories as $repair_history)--}}

                                    {{--<div class="child-repair-history form-group">--}}
                                        {{--@if($loop->iteration == 1)--}}
                                            {{--<label class="col-sm-2 control-label">Repair History</label>--}}
                                        {{--@else--}}
                                            {{--<label class="col-sm-2 control-label"></label>--}}
                                        {{--@endif--}}
                                        {{--<div class="col-sm-5">--}}
                                            {{--<input type="text" name="repair_description[]" class="form-control"--}}
                                                   {{--placeholder="Repair Description"--}}
                                                   {{--value="{{$repair_history->repair_description}}">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-2">--}}
                                            {{--<input type="number" name="cost[]" class="form-control" placeholder="Cost"--}}
                                                   {{--value="{{$repair_history->cost}}">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-2 date">--}}
                                            {{--<input type="text" class="form-control" name="date_repair[]"--}}
                                                   {{--placeholder="Date Repaired" value="{{$repair_history->date_repair}}">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-1">--}}
                                            {{--@if($loop->iteration == 1)--}}
                                                {{--<button id="btn-add" type="button"--}}
                                                        {{--class="btn btn-sm btn-circle btn-info">--}}
                                                {{--</button>--}}
                                            {{--@else--}}
                                                {{--<button id="btn-minus" type="button"--}}
                                                        {{--class="btn btn-sm btn-circle btn-danger">--}}
                                                {{--</button>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                {{--@endforeach--}}
                            {{--</div>--}}
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('trucks.index') }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/customjs/trucks.js"></script>
@endsection