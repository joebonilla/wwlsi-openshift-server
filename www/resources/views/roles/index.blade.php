@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Roles</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('system/dashboard')}}">Roles Management</a>
                </li>
                {{--<li class="active">--}}
                {{--<strong>Layouts</strong>--}}
                {{--</li>--}}
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            @if(Entrust::can('role-create'))
                                <h2>
                                    <a class="btn btn-w-m btn-primary btn-xs" href="{{ route('roles.create') }}"> Create New
                                        Role</a>
                                </h2>
                            @endif
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table id="roles-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th width="280px">Action</th>
                    </tr>
                    </thead>
                    @foreach ($roles as $key => $role)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $role->display_name }}</td>
                            <td>{{ $role->description }}</td>
                            <td>
                                <a class="btn btn-info btn-xs" href="{{ route('roles.show',$role->id) }}">Show</a>
                                @if(Entrust::can('role-edit'))
                                    <a class="btn btn-primary btn-xs" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                                @endif
                                @if(Entrust::can('role-delete'))
                                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
                {{--{!! $roles->render() !!}--}}
            </div>
        </div>
    </div>
@endsection