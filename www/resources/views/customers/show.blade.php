@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('clients.index') }}">All Clients</a>
                </li>
                <li>
                    <a href="{{ route('clients.show', $client->id) }}">Clients Profile</a>
                </li>
                <li class="active">
                    <strong>Customers Profile</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Customer Detail</h5>
                        <a href="{{ route('customers.edit', $customer->id) . '?client=' . $client->id }}"
                           class="btn btn-primary btn-xs pull-right"><i
                                    class="fa fa-pencil"></i> Edit</a>

                    </div>
                    <div>

                        <div class="ibox-content profile-content">
                            <h4>Customer: <strong>{{ $customer->name }}</strong></h4>
                            <h4>Client:<strong>{{ $client->name }}</strong></h4>
                            <p>
                                <i class="fa fa-map-marker"></i> {{$customer->address->address_name}}
                            </p>
                            <h4>Price:{{money_format('%i', $customer->price)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection