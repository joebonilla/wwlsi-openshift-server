@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Clients</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Customers List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Customers table</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-9 m-b-xs text-left">
                                    <a href="{{ route('customers.create') }}" class="btn btn-primary btn-xs">Add Client</a>
                                </div>
                                <div class="col-md-3 text-right">
                                    <div class="input-group"><input type="text" placeholder="Search"
                                                                    class="input-sm form-control"> <span
                                                class="input-group-btn">
                                            <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="row">
                            @foreach($customers as $customer)
                                <div class="col-lg-4">
                                    <div class="contact-box">
                                        <a href="{{ route('customers.show', $customer->id) }}">
                                            <div class="col-sm-12">
                                                <h3><strong>{{ $customer->name }}</strong></h3>
                                                <p><i class="fa fa-map-marker"></i> Riviera State 32/106</p>
                                                <address>
                                                    <strong>Twitter, Inc.</strong><br>
                                                    795 Folsom Ave, Suite 600<br>
                                                    San Francisco, CA 94107<br>
                                                    <abbr title="Phone">P:</abbr> (123) 456-7890
                                                </address>
                                            </div>
                                        </a>
                                        <div class="col-sm-12">
                                            <form method="post" class="form-horizontal"
                                                  action="{{ route('clients.destroy',$customer->id) }}">
                                                <input type="hidden" name="_method" value="delete">
                                                {{ csrf_field() }}
                                                <button class="btn btn-danger pull-right">Delete</button>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection