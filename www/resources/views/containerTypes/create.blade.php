@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Add Container Types</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('containerTypes.index') }}">All Container Types</a>
                </li>
                <li class="active">
                    <strong>Add Container Types</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Add Container Types Form</h5>
                    </div>
                    <div class="ibox-content">
                        @include('layouts.error')
                        <form method="post" class="form-horizontal" action="{{ route('containerTypes.store') }}">
                            {{ csrf_field() }}
                            <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10"><input type="text" name="name" class="form-control"
                                                              value="{{old('name')}}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10"><input type="text" name="description" class="form-control"
                                                              value="{{old('description')}}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Payloads</label>
                                <div class="col-sm-10"><input type="number" name="payloads" class="form-control"
                                                              value="{{old('payloads')}}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Cubic Capacity</label>
                                <div class="col-sm-10"><input type="number" name="cubic_capacity" class="form-control"
                                                              value="{{old('cubic_capacity')}}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Length</label>
                                <div class="col-sm-3"><input type="number" placeholder="Length" name="interior_length"
                                                             class="form-control" value="{{old('interior_length')}}">
                                </div>
                                <div class="col-sm-3"><input type="number" placeholder="Width" name="interior_width"
                                                             class="form-control" value="{{old('interior_width')}}">
                                </div>
                                <div class="col-sm-3"><input type="number" placeholder="Height" name="interior_height"
                                                             class="form-control" value="{{old('interior_height')}}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('containerTypes.index') }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
