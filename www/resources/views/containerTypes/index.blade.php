@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Container Types</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Container Types</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Container Types table</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-9 m-b-xs text-left">
                                    <div class="row">
                                        <a href="{{ route('containerTypes.create') }}" class="btn btn-primary btn-xs">Add
                                            Container Types</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>ID#</th>
                                    <th>Name</th>
                                    <th>Payloads</th>
                                    <th>Cubic Capacity</th>
                                    <th>Date Added</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($containerTypes as $containerType)
                                    <tr>
                                        <td>{{ $containerType->id }}</td>
                                        <td>
                                            <a href="{{ route('containerTypes.show', $containerType->id) }}">{{ $containerType->name }}</a>
                                        </td>
                                        <td>{{ $containerType->payloads }}</td>
                                        <td>{{ $containerType->cubic_capacity }}
                                        <td>{{ $containerType->created_at }}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection