@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Edit Container Types</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('containerTypes.index') }}">All Container Types</a>
                </li>
                <li class="active">
                    <strong>Edit Container Types</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit Container Types Form</h5>
                    </div>
                    <div class="ibox-content">
                        @include('layouts.error')
                        <form method="post" class="form-horizontal" action="{{ route('containerTypes.update', $containerType->id) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10"><input type="text" value="{{ $containerType->name }}" name="name" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10"><input type="text" name="description" class="form-control" value="{{ $containerType->description }}"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Payloads</label>
                                <div class="col-sm-10"><input type="text" value="{{ $containerType->payloads }}" name="payloads" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Cubic Capacity</label>
                                <div class="col-sm-10"><input type="text" value="{{ $containerType->cubic_capacity }}" name="cubic_capacity" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Length</label>
                                <div class="col-sm-3"><input type="text" value="{{ $containerType->interior_length }}" placeholder="Length" name="interior_length" class="form-control"></div>
                                <div class="col-sm-3"><input type="text" value="{{ $containerType->interior_width }}" placeholder="Width" name="interior_width" class="form-control"></div>
                                <div class="col-sm-3"><input type="text" value="{{ $containerType->interior_height }}" placeholder="Height" name="interior_height" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a href="{{ route('containerTypes.index') }}" class="btn btn-white">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
