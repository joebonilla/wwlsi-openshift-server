@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>All Clients</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('containerTypes.index') }}">All Container Types</a>
                </li>
                <li class="active">
                    <strong>Container Profile</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Container Type Detail</h5>
                        <a href="{{ route('containerTypes.edit', $containerType->id) }}"
                           class="btn btn-primary btn-xs pull-right"><i
                                    class="fa fa-pencil"></i> Edit</a>

                    </div>
                    <div>

                        <div class="ibox-content profile-content">
                            <h3><strong>{{ $containerType->name }}</strong></h3>
                            <p>
                                {{ $containerType->description }}
                            </p>
                            <h5><b>Payloads: </b>{{ $containerType->payloads }}</h5>
                            <h5><b>Cubic capacity: </b>{{ $containerType->cubic_capacity }}</h5>
                            <h5><b>Length: </b>{{ $containerType->interior_length }}</h5>
                            <h5><b>Width: </b>{{ $containerType->interior_width }}</h5>
                            <h5><b>Height: </b>{{ $containerType->interior_height }}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection