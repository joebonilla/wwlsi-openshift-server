@extends('layouts.layout')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Search Results</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('system/dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Results</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h2>

                            {{$count}} results found for: <span class="text-navy">"{{$topsearch}}
                                "</span>
                        </h2>
                        <div class="hr-line-dashed"></div>
                        <div class="search-result">
                            @foreach($search as $key_url => $searches)
                                @foreach($searches as $item)
                                    @foreach($item->getAttributes() as $key => $attribute)
                                        @if(!in_array($key,$notallowed))
                                            @if($key == 'name')
                                                <h3>
                                                    <a href="{{url($key_url.'/'.$item->id)}}">{{ucfirst($key) . ': '. ucfirst($attribute)}}</a>
                                                </h3>
                                            @else

                                                <p>
                                                    <strong>{{ucfirst($key)}} : </strong>{{ucfirst($attribute)}}
                                                </p>
                                            @endif
                                        @endif

                                    @endforeach
                                    <div class="hr-line-dashed"></div>
                                @endforeach

                            @endforeach

                        </div>
                        <div class="hr-line-dashed"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/customjs/trucks.js"></script>
@endsection