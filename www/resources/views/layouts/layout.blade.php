<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'WestWind Trucking System') }}</title>
    {{ Html::style('css/bootstrap.min.css') }}
    {{ Html::style('font-awesome/css/font-awesome.css') }}
    {{ Html::style('css/plugins/toastr/toastr.min.css') }}
    {{ Html::style('css/plugins/select2/select2.min.css') }}
    {{ Html::style('css/plugins/chosen/chosen.css') }}
    {{ Html::style('js/plugins/gritter/jquery.gritter.css') }}
    {{ Html::style('css/plugins/datapicker/datepicker3.css') }}
    {{ Html::style('css/plugins/dataTables/datatables.min.css') }}
    {{ Html::style('css/animate.css') }}
    {{ Html::style('css/style.css') }}
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body data-url="{{url('/')}}">
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="profile-element text-white">
                        <a href="{{ route('users.show', Auth::user()->id) }}">
                        <span class="block m-t-xs"> Hi {{ Auth::user()->name }}!</span>
                        @foreach(Auth::user()->roles as $roles)
                            <span class="text-muted text-xs block">{{$roles->name}}</span>
                        @endforeach
                        </a>
                    </div>
                    <div class="logo-element">
                        WW
                    </div>
                </li>

                <li class="nav-items" data-nav-name="dashboard">
                    <a href="{{ url('system/dashboard') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                </li>

                <li class="nav-items" data-nav-name="operations">
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Operations</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="nav-items" data-nav-name="transactions"><a href="{{ route('transactions.index') }}">Transactions</a></li>
                        <li class="nav-items" data-nav-name="containerTypes"><a href="{{ route('containerTypes.index') }}">Container Types</a></li>
                        <li class="nav-items" data-nav-name="expenses"><a href="{{ route('expenses.index') }}">Expenses</a></li>
                        <li class="nav-items" data-nav-name="payrolls"><a href="{{ route('payrolls.index') }}">Payroll</a></li>
                        <li class="nav-items" data-nav-name="transactionReport"><a href="{{ route('transactions_report.index') }}">Report</a></li>
                    </ul>
                </li>

                <li class="nav-items" data-nav-name="maintenance">
                    <a href="#"><i class="fa fa-truck"></i> <span class="nav-label">Maintenance</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="nav-items" data-nav-name="trucks"><a href="{{ route('trucks.index') }}">Trucks</a></li>
                        <li class="nav-items" data-nav-name="chassis"><a href="{{ route('chassis.index') }}">Chassis</a></li>

                    </ul>
                </li>

                <li class="nav-items" data-nav-name="marketing">
                    <a href="#"><i class="fa fa-address-book" aria-hidden="true"></i>
                        <span class="nav-label">Marketing</span>
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="nav-items" data-nav-name="clients">
                            <a href="{{ route('clients.index') }}"><span
                                        class="nav-label">Clients</span></a>
                        </li>
                        <li class="nav-items" data-nav-name="ports"><a
                                    href="{{ route('ports.index') }}"><span
                                        class="nav-label">Ports</span></a>
                        </li>
                    </ul>
                </li>

                <li class="nav-items" data-nav-name="acccounting">
                    <a href="#"><i class="fa fa-money"></i> <span class="nav-label">Accounting</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="nav-items" data-nav-name="accounting"><a
                                    href="{{ route('accounting.index') }}">Accounting</a></li>
                    </ul>
                </li>

                <li class="nav-items" data-nav-name="admin">
                    <a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> <span class="nav-label">Admin</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="nav-items" data-nav-name="users"><a href="{{ route('users.index') }}">Users</a></li>
                        <li class="nav-items" data-nav-name="roles"><a href="{{ route('roles.index') }}">Roles</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
        <div id="content">
            @yield('content')
        </div>
        <div class="footer">
            <div>
                <strong>Copyright</strong> WestWind © 2016
            </div>
        </div>
    </div>
</div>
<!-- Mainly scripts -->
{{ Html::script('js/jquery-3.1.1.min.js') }}
{{ Html::script('js/jquery-migrate-1.4.1.min.js') }}
{{ Html::script('js/bootstrap.min.js') }}
{{ Html::script('js/plugins/metisMenu/jquery.metisMenu.js') }}
{{ Html::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }}

<script src="/js/customjs/main.js"></script>

<!-- Flot -->
{{--<script src="/js/plugins/flot/jquery.flot.js"></script>--}}
{{--<script src="/js/plugins/flot/jquery.flot.tooltip.min.js"></script>--}}
{{--<script src="/js/plugins/flot/jquery.flot.spline.js"></script>--}}
{{--<script src="/js/plugins/flot/jquery.flot.resize.js"></script>--}}
{{--<script src="/js/plugins/flot/jquery.flot.pie.js"></script>--}}

<!-- Peity -->
{{--<script src="/js/plugins/peity/jquery.peity.min.js"></script>--}}
{{--<script src="/js/demo/peity-demo.js"></script>--}}

<!-- Custom and plugin javascript -->
{{ Html::script('js/inspinia.js') }}
{{ Html::script('js/plugins/pace/pace.min.js') }}

<!-- jQuery UI -->
{{--<script src="/js/plugins/jquery-ui/jquery-ui.min.js"></script>--}}

<!-- GITTER -->
{{--<script src="/js/plugins/gritter/jquery.gritter.min.js"></script>--}}

<!-- Sparkline -->
{{--<script src="/js/plugins/sparkline/jquery.sparkline.min.js"></script>--}}

<!-- Sparkline demo data  -->
{{--<script src="/js/demo/sparkline-demo.js"></script>--}}

<!-- ChartJS-->
{{--<script src="/js/plugins/chartJs/Chart.min.js"></script>--}}

<!-- Toastr -->
{{ Html::script('js/plugins/toastr/toastr.min.js') }}
{{ Html::script('js/plugins/chosen/chosen.jquery.js') }}
{{ Html::script('js/plugins/datapicker/bootstrap-datepicker.js') }}
{{ Html::script('js/plugins/dataTables/datatables.min.js') }}

{{--<script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>--}}

{{ Html::script('js/main.js') }}
@yield('scripts')
<script type="text/javascript">
    $('.nav-items').each(function(key, elem){
        var url = window.location.href;
        var target_elem = $(elem);
        if(url.indexOf(target_elem.attr('data-nav-name')) > 0){
            target_elem.addClass('active');
        }
    });
</script>
@yield('footer')

</body>

</html>
