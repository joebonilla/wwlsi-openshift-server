$(document).ready(function () {
    let Accounting = {
        elems: [],
        init: function () {
            this.elems.approveCostBtn = $('.approve-cost');
            this.elems.typeReport = $('#type_report');
            this.elems.dateFrom = $('[name="date_from"]');
            this.elems.dateEnd = $('[name="date_end"]');
            this.elems.selectFormClient = $('.select-form-client');
            this.initEvents();
        },

        initEvents: function () {
            this.elems.approveCostBtn.on('click', function (e) {
                e.preventDefault();
                Accounting.supplyApproveCostModal($(this));
            });
            Accounting.getTypeReport($(this));
        },

        supplyApproveCostModal: function (approveCostBtn) {
            let modal = $('#approveExpense');
            let form = modal.find('form');

            form.attr('action', approveCostBtn.attr('data-approve-action'));
            modal.modal('show');
        },
        getTypeReport: function () {
            this.elems.selectFormClient.show();
            Accounting.getAllClient();
        },
        getAllClient: function () {
            let localElems = this.elems;
            let customerSelect = localElems.selectFormClient;
            let selectTagElem = customerSelect.find('select');

            $.ajax({
                method: "GET",
                url: "/marketing/clients",
                dataType: 'json',
                success: function (response) {
                    for (let prop in response) {
                        selectTagElem.append('<option value="' + response[prop].id + '">' + response[prop].name + '</option>');
                    }
                    selectTagElem.trigger('chosen:updated');
                }

            });
            selectTagElem.trigger('chosen:updated');
        }

    };

    Accounting.init();
});