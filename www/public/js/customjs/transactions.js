$(document).ready(function(){
    let Transactions = {
        elems : [],
        init : function(){
            this.elems.typeSelectTag = $('[name="type"]');
            this.elems.clientSelectTag = $('[name="client_id"]');
            this.elems.customerSelectTag = $('[name="customer_id"]');
            this.elems.expenseSelectTag = $('#selectExpense');
            this.elems.portSelectTag = $('[name="port_id"]');
            this.elems.removeCostBtn = $('.remove-cost');
            this.elems.updatePreAdviceBtn = $('.change-preadvice-status');
            this.elems.withVatCheckbox = $('[name="with_vat"]');

            this.elems.rangePicker = $('.range-picker');

            this.initEvents();

            this.elems.typeSelectTag.trigger('change');
            this.elems.withVatCheckbox.trigger('load-check');
        },

        initEvents : function(){
            let localElems = this.elems;
            let onGoingDataTable = Transactions.onGoingTabDataTable;

            localElems.typeSelectTag.on('change', function(){
                Transactions.toggleContainerSectionHide($(this));
            });

            localElems.withVatCheckbox.on('click load-check', function(){
                Transactions.toggleVatContainerHide($(this));
            });

            localElems.clientSelectTag.on('change', function(){
                Transactions.clientSelectTagAjax($(this));
                localElems.customerSelectTag.trigger('change');
            });

            localElems.customerSelectTag.on('change', function(){
                Transactions.customerSelectTagAjax($(this));
            });

            localElems.expenseSelectTag.on('change', function(){
                Transactions.expenseSelectTagAjax($(this));
            });

            localElems.portSelectTag.on('change', function(){
                Transactions.portSelectTagAjax($(this));
            });

            localElems.removeCostBtn.on('click', function(){
                Transactions.supplyRemoveExpenseModal($(this));
            });

            localElems.rangePicker.on('click', function(){
                Transactions.processDataTable($(this));
            });

            onGoingDataTable.init();

            localElems.updatePreAdviceBtn.on('click', function(e){
                let btnText = ($(this).text() == 'No') ? 'Yes' : 'No';
                let yes = confirm('Are you sure you want to change the pre-advice status to ' + btnText + '?');

                if(!yes){
                    e.preventDefault();
                }
            });

        },

        toggleContainerSectionHide : function(typeSelectTag){
            let container = $('.tc-container');
            let inputs = container.find('.form-control');
            let typeSelectVal = typeSelectTag.val();

            if(typeSelectVal == 0){
                container.slideUp(function(){
                    inputs.attr('disabled', 'disabled');
                });
            } else {
                inputs.removeAttr('disabled');
                container.slideDown();
            }
        },

        toggleVatContainerHide : function(vatCheckbox){
            let container = $('.vat-container');

            if(vatCheckbox.is(':checked')){
                container.slideDown();
            } else {
                container.slideUp();
            }
        },

        expenseSelectTagAjax : function(expenseSelectTag){
            let expenseId = expenseSelectTag.val();
            let parentContainer = expenseSelectTag.parents('#addExpenseContainer');
            let titleField = parentContainer.find('input[name="title"]');
            let costField = parentContainer.find('input[name="cost"]').val("");

            $.ajax({
                method: "GET",
                url: "/system/operations/getExpense/" + expenseId,
                dataType : 'json',
                success : function(response){
                    titleField.val(response.title);
                    costField.val(response.cost);
                }
            })
        },

        clientSelectTagAjax : function(expenseSelectTag){
            let clientId = expenseSelectTag.val();
            let localElems = this.elems;
            let customerSelect = localElems.customerSelectTag;

            customerSelect.html('<option value="">Select Customer</option>');

            $.ajax({
                method: "GET",
                url: "/system/operations/getClientCustomers/" + clientId,
                dataType : 'json',
                success : function(response){
                    for(let prop in response){
                        customerSelect.append('<option value="' + response[prop].id + '">'+ response[prop].name +'</option>');
                    }
                    customerSelect.trigger('chosen:updated');
                }
            });
            customerSelect.trigger('chosen:updated');
        },

        customerSelectTagAjax : function(customerSelectTag){
            let customerId = customerSelectTag.val();
            let destinationField = customerSelectTag.siblings('[name="destination_address"]').val('');

            $.ajax({
                method: "GET",
                url: "/system/operations/getCustomerAddress/" + customerId,
                dataType : 'json',
                success : function(response){
                    destinationField.val(response);
                }
            })
        },

        portSelectTagAjax : function(portSelectTag){
            let portId = portSelectTag.val();
            let originAddressField = portSelectTag.siblings('[name="origin_address"]').val('');

            $.ajax({
                method: "GET",
                url: "/system/operations/getPortAddress/" + portId,
                dataType : 'json',
                success : function(response){
                    originAddressField.val(response);
                }
            })
        },

        supplyRemoveExpenseModal : function(removeBtn){
            let modal = $('#removeCost');
            let form = modal.find('form');

            form.attr('action', removeBtn.attr('data-delete-action'));
            modal.modal('show');
        },

        processDataTable : function(range_btn){
            let onGoingTabDt = Transactions.onGoingTabDataTable;

            onGoingTabDt.start_date = null;
            onGoingTabDt.end_date = null;

            if(range_btn){
                onGoingTabDt.start_date = range_btn.attr('data-start');
                onGoingTabDt.end_date = range_btn.attr('data-end');
            }

            onGoingTabDt.table.draw();
        },

        onGoingTabDataTable : {
            start_date : null,
            end_date : null,
            table : null,
            init : function(){
                let me = Transactions.onGoingTabDataTable;
                let ogdt_url = $('#on-going-route').attr('data-ogdt-route');

                me.table = $('#ongoing-table').DataTable({
                    serverSide: true,
                    processing: true,
                    ajax: {
                        url: ogdt_url,
                        data: function (param) {
                            param.start_search = me.start_date;
                            param.end_search = me.end_date;
                        }
                    },

                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'client.name', name: 'client.name', orderable: false},
                        {data: 'customer.name', name: 'customer.name', orderable: false},
                        {data: 'container', name: 'container', orderable: false, searchable: false},
                        {data: 'destination_address', name: 'destination_address', orderable: false,},
                        {data: 'type', name: 'type'},
                        {data: 'status', name: 'status', orderable: false, searchable: false},
                        {data: 'created_at', name: 'created_at'},
                    ],

                    dom: '<"html5buttons"B>lTfgitp',
                    order: [0, 'desc'],
                    pagingType:'full_numbers',
                    buttons: [
                        {extend: 'copy'},
                        {extend: 'excel', className: 'btn-sm btn-info'},
                        {extend: 'pdf', className: 'btn-sm btn-success'},

                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ],
                });
            }
        },
    };

    Transactions.init();
});