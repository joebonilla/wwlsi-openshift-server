$(document).ready(function(){
    let Main = {
        elems : [],
        init : function(){
            this.elems.modalToggler = $('.modal-toggle');
            this.elems.transactionClientField = $('.transaction-field[name="client_id"]');
            this.elems.transactionCustomerField = $('.transaction-field[name="customer_id"]');
            this.initEvents();
            this.activateTabFromUrl();

            this.elems.transactionClientField.trigger('change');
        },

        initEvents : function(){
            let localElems = this.elems;

            localElems.modalToggler.on('click', function(e){
                e.preventDefault();
                Main.supplyModalData($(this));
            });

            localElems.transactionClientField.on('change', function(){
                Main.transactionClientSelectTagAjax($(this));
                localElems.transactionCustomerField.trigger('change');
            });
        },

        activateTabFromUrl : function(){
            let url = window.location.href;
            let tabId = $('[href="#' + url.split('#')[1] + '"]');
            let tabContentId = $('#' + url.split('#')[1]);
            let parent = tabId.parent();
            let tabContent = tabId.parents('.tab-container').find('.tab-content');
            let activeTabContent = tabContent.find('.tab-pane.active');
            let currentActive = tabId.parents('ul.nav').find('li.active');

            currentActive.removeClass('active');
            parent.addClass('active');

            activeTabContent.removeClass('active');
            tabContentId.addClass('active');
        },

        supplyModalData : function(modalToggler){
            let modal = $(modalToggler.attr('data-modal-target'));
            let form = modal.find('form');

            form.attr('action', modalToggler.attr('data-action'));
            modal.modal('show');
        },

        transactionClientSelectTagAjax : function(expenseSelectTag){
            let clientId = expenseSelectTag.val();
            let localElems = this.elems;
            let customerSelect = localElems.transactionCustomerField;
            let url = '/system/operations/';

            url += (clientId != "") ? "getClientCustomers/" + clientId : "getCustomers";

            customerSelect.html('<option value="">All Customer</option>');

            $.ajax({
                method: "GET",
                url: url,
                dataType : 'json',
                success : function(response){
                    for(let prop in response){
                        customerSelect.append('<option value="' + response[prop].id + '">'+ response[prop].name +'</option>');
                    }
                    customerSelect.trigger('chosen:updated');
                }
            });
            customerSelect.trigger('chosen:updated');
        },
    };

    Main.init();
});