$(document).ready(function () {
    let trucks = {
        init: function () {
            this.init_events();
        },
        init_events: function () {
            let parent_elem = $('#repair-history-parent'),
                child_elem = '<div class="child-repair-history form-group"><label class="col-sm-2 control-label"></label>' +
                    '<div class="col-sm-5">' +
                    '<input type="text" name="repair_description[]" class="form-control"placeholder="Repair Description"></div>' +
                    '<div class="col-sm-2"> <input type="number" name="cost[]" class="form-control" placeholder="Cost"></div>' +
                    '<div class="col-sm-2 date"><input type="text" class="form-control" name="date_repair[]" placeholder="Date Repaired"></div>' +
                    '<div class="col-sm-1"><button id="btn-minus" type="button" class="btn btn-sm btn-circle btn-danger"> </button> </div></div>',
                btn_add = $('#btn-add'), body = $('body'), btn_minus = $('#btn-minus'),
                sum = 0,
                num = 0;

            btn_add.on('click', function () {
                parent_elem.append(child_elem);
            });

            body.on('click', '#btn-minus', function () {
                $(this).closest('.child-repair-history').remove();
            });

            $(".cost").each(function () {
                num = parseInt($(this).text());
                if (isNaN(num)) {
                    num = 0;
                }
                sum += num;
            });

            $('.total_cost').text('PHP ' + sum.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

            body.on('focus', '.date input', function () {
                $('.date input').datepicker({
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: true,
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                });
            });
            body.on('click', '.truckEdit', function () {

                let repair_id = $(this).data('id'),
                    repair_description = $(this).data('repair'),
                    date_repair = $(this).data('date'),
                    contractor_name = $(this).data('contractor'),
                    cost_repair = $(this).data('cost'),
                    date = new Date(date_repair),
                    d = ("0" + date.getDate()).slice(-2),
                    m = ("0" + (date.getMonth() + 1)).slice(-2),
                    y = date.getFullYear(),
                    converted_date = y + '-' + m + '-' + d;

                $('#repairEditModal').on('show.bs.modal', function () {
                    $(this).find('input[name=repair_description]').val(repair_description);
                    $(this).find('input[name=contractor_name]').val(contractor_name);
                    $(this).find('input[name=date_repair]').val(converted_date);
                    $(this).find('input[name=cost]').val(cost_repair);
                    $(this).find('#form-edit').attr('action', body.data('url') + "/system/maintenance/truckrepairs/" + repair_id);
                    $(this).find('#form-delete').attr('action', body.data('url') + "/system/maintenance/truckrepairs/" + repair_id);
                })
            });
        }
    };
    trucks.init();
});
