<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by_id');
            $table->integer('client_id');
            $table->integer('customer_id');
            $table->integer('type');
            $table->boolean('status');
            $table->boolean('pre_advice_status');
            $table->integer('truck_id');
            $table->integer('driver_id');
            $table->integer('helper_id');
            $table->integer('port_id');
            $table->string('destination_address');
            $table->string('origin_address');
            $table->float('kilometer');
            $table->float('base_earn', 10, 2);
            $table->float('fuel_cost');
            $table->boolean('with_vat');
            $table->float('vat_percent')->default(0);
            $table->string('note')->nullable();
            $table->date('date_completed')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
