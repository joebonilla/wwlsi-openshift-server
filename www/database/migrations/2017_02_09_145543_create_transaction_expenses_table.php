<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id');
            $table->string('title');
            $table->integer('cost');
            $table->boolean('reflect_on_sales');
            $table->boolean('liquidation_status');
            $table->integer('approved_by');
            $table->date('date_approved')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_expenses');
    }
}
