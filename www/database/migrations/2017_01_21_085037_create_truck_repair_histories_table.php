<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckRepairHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('truck_repair_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('truck_id');
            $table->integer('request_user_id');
            $table->text('repair_description')->nullable();
            $table->text('contractor_name')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('status');
            $table->timestamp('date_repair');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('truck_repair_histories');
    }
}
