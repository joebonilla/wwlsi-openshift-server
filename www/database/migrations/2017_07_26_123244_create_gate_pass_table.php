<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatePassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('gate_pass', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('customer_id');
            $table->string('booking_ref_no');
            $table->dateTime('zone');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('gate_pass');
    }
}
