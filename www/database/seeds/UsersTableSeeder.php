<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $user = [
            [
                'name' => 'Admin',
                'email' => 'admin@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Maintenance',
                'email' => 'maintenance@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Operations',
                'email' => 'operations@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Accountant',
                'email' => 'accountant@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Marketing',
                'email' => 'marketing@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Helper 1',
                'email' => 'helper1@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Driver 1',
                'email' => 'driver1@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Helper 2',
                'email' => 'helper2@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Driver 2',
                'email' => 'driver2@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ], [
                'name' => 'Helper 3',
                'email' => 'helper3@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Driver 3',
                'email' => 'driver3@email.com',
                'password' => bcrypt('admin123'),
                'user_role_id' => 1,
                'contact_no' => '12345',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],


        ];
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
