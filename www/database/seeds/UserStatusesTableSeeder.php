<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $truck_statuses = [[
            'name' => 'Available',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], [
            'name' => 'On Going',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]];

        foreach ($truck_statuses as $truck_status) {
            DB::table('user_statuses')->insert($truck_status);
        }
    }
}
