<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $role = [
            [
                'name' => 'admin',
                'display_name' => 'System Admin',
                'description' => 'Has the Ability to view all the pages but has a limit on data'
            ],
            [
                'name' => 'maintenance',
                'display_name' => 'Maintenance',
                'description' => 'Has the Ability to view maintenance page and archive transaction'
            ],
            [
                'name' => 'operations',
                'display_name' => 'Operations',
                'description' => 'Has the Ability to view maintenance page and archive transaction'
            ],
            [
                'name' => 'accountant',
                'display_name' => 'Accountant',
                'description' => 'Has the Ability to view accounting page and archive transaction'
            ],
            [
                'name' => 'marketing',
                'display_name' => 'Marketing',
                'description' => 'Has the Ability to view marketing page and manipulate data inside'
            ],
            [
                'name' => 'helper',
                'display_name' => 'Helper',
                'description' => 'User that has no ability to manipulate and view pages within the system but include in transaction'
            ],
            [
                'name' => 'driver',
                'display_name' => 'Driver',
                'description' => 'User that has no ability to manipulate and view pages within the system but include in transaction'
            ]

        ];
        foreach ($role as $key => $value) {
            Role::create($value);
        }
    }
}
