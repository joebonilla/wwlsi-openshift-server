<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_roles = [[
            'name' => 'Admin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'name' => 'Accountant',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'name' => 'Normal User',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'name' => 'Marketing Manager',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'name' => 'Accountant',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'name' => 'Helper',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],[
            'name' => 'Driver',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]];

        foreach($user_roles as $user_role){
            DB::table('user_roles')->insert($user_role);
        }
    }
}
