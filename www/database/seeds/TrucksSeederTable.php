<?php

use App\Truck;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TrucksSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $user = [
            [
                'name' => 'Truck 1',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'PLA123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 2',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'TTT123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 3',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'LLL123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 4',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'KKK123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 5',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'JJJ123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 6',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'FFF123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 7',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'EEE123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 8',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'DDD123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 9',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'CCC123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck 10',
                'manufacturer' => 'Izuzu',
                'truck_status_id' => 1,
                'plate_no' => 'BBB123',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => Carbon::now(),
                'deleted_at' => null,
                'updated_at' => Carbon::now()
            ],


        ];
        foreach ($user as $key => $value) {
            Truck::create($value);
        }
    }
}
