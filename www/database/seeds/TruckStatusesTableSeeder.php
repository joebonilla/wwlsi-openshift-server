<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TruckStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $truck_statuses = [[
            'name' => 'Available',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], [
            'name' => 'On Going',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], [
            'name' => 'Under Repair',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], [
            'name' => 'Retired',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]];

        foreach ($truck_statuses as $truck_status) {
            DB::table('truck_statuses')->insert($truck_status);
        }
    }
}
