<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::middleware('auth')->prefix('system')->group(function () {
    Route::get('/', 'DashboardController@index');
    Route::get('dashboard', 'DashboardController@index');
});

Route::middleware(['auth', 'role:admin'])->prefix('system/admin')->group(function () {
    Route::resource('users', 'UserController');

    Route::get('roles', 'RoleController@index')->middleware(['permission:role-list|role-create|role-edit|role-delete'])->name('roles.index');
    Route::get('roles/create', 'RoleController@create')->middleware(['permission:role-create'])->name('roles.create');
    Route::post('roles/create', 'RoleController@store')->middleware(['permission:role-create'])->name('roles.store');
    Route::get('roles/{id}', 'RoleController@show')->name('roles.show');
    Route::get('roles/{id}/edit', 'RoleController@edit')->middleware(['permission:role-edit'])->name('roles.edit');
    Route::patch('roles/{id}', 'RoleController@update')->middleware(['permission:role-edit'])->name('roles.update');
    Route::delete('roles/{id}', 'RoleController@destroy')->middleware(['permission:role-delete'])->name('roles.destroy');
});
Route::middleware(['auth', 'role:admin|marketing|operations'])->prefix('system/marketing')->group(function () {
    Route::resource('clients', 'ClientController');
    Route::resource('customers', 'CustomersController');
    Route::resource('ports', 'PortController');
});
Route::middleware(['auth', 'role:maintenance|admin|operations'])->prefix('system/maintenance')->group(function () {
    Route::resource('trucks', 'TruckController');
    Route::resource('chassis', 'ChassisController');
    Route::resource('truckrepairs', 'TruckRepairController');
    Route::post('chassis/{id}', 'ChassisController@update');
    Route::any('trucks/query', 'TruckController@TruckSearch')->name('truck_search');
});

Route::middleware(['auth', 'role:operations|marketing|admin'])->prefix('system/operations')->group(function () {
    Route::resource('transactions', 'TransactionController');
    Route::resource('containerTypes', 'ContainerTypeController');
    Route::resource('expenses', 'ExpenseController');
    Route::resource('payrolls', 'PayrollController');
    Route::resource('adjustments', 'AdjustmentController');

    Route::post('addTransactionExpense/{transaction_id}', 'TransactionController@addTransactionExpense')->name('add_expense');
    Route::post('updateTransactionStatus/{transaction_id}', 'TransactionController@updateTransactionStatus')->name('update_status');
    Route::post('addTransactionNote/{transaction_id}', 'TransactionController@addTransactionNote')->name('add_note');
    Route::post('addTransactionRemarks/{transaction_id}', 'TransactionController@addTransactionRemarks')->name('add_remarks');
    Route::post('removeCost/{transaction_expense_id}', 'TransactionController@removeCost')->name('remove_cost');
    Route::post('updateTransactionPreAdviceStatus/{transaction_id}', 'TransactionController@updateTransactionPreAdviceStatus')->name('update_pre_advice');
    Route::post('releaseChassis/{transaction}', 'TransactionController@releaseChassis')->name('release_chassis');
    Route::get('exportExpenseToPdf/{transaction}', 'TransactionController@exportExpenseToPdf');

    Route::get('getCustomerAddress/{customer_id}', 'TransactionController@getCustomerAddress');
    Route::get('getPortAddress/{port_id}', 'TransactionController@getPortAddress');
    Route::get('getClientCustomers/{client_id}', 'TransactionController@getClientCustomers');
    Route::get('getExpense/{expense_id}', 'ExpenseController@getExpense');
    Route::get('payrolls/pdf/{payroll_id}', 'PayrollController@printPayrollPdf');

    Route::get('transaction/payroll-pending-data', 'PayrollController@getPayrollPendingData')->name('payroll_pending_data');
    Route::get('transaction/payroll-complete-data', 'PayrollController@getPayrollCompleteData')->name('payroll_complete_data');

    Route::post('storeFuelCost', 'ExpenseController@storeFuelCost')->name('store_fuel_cost');
    Route::get('getCustomers', 'TransactionController@getCustomers');

    Route::get('transaction/on-going-data', 'TransactionController@getTransactionOngoingData')->name('transactions_ongoing_data');
    Route::get('transaction/completed-data', 'TransactionController@getTransactionCompleteData')->name('transactions_complete_data');

    Route::get('transactionsReport', 'TransactionReport@index')->name('transactions_report.index');
    Route::post('transactionsReport', 'TransactionReport@generate')->name('transactions_report.generate');
    Route::post('transactionsImport', 'TransactionReport@import')->name('transactions_report.import');
    Route::get('downloadTemplate', 'TransactionReport@downloadTemplate')->name('transactions_report.download_template');


});

Route::middleware(['auth', 'role:accountant|admin'])->prefix('system/accounting')->group(function () {
    Route::get('/', 'AccountingController@index')->name('accounting.index');
    Route::post('reportGenerator', 'AccountingController@generateReport')->name('accounting_report');

    Route::post('approveCost/{expense_id}', 'AccountingController@approveCost')->name('approve_cost');
});

Route::get('/', 'FrontController@index');

Route::middleware('auth')->prefix('search')->group(function () {
    Route::get('/', 'SearchController@search');
});
