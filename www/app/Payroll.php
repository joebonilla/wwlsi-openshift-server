<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payroll extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['request_user_id', 'for_user_id', 'start', 'end', 'user_id', 'adjustment_value'];

    public static $status_pending = 1;
    public static $status_complete = 2;

    public function users() {
        return $this->belongsTo('App\User', 'request_user_id');
    }

    public function users_driver_helper() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function payroll_users() {
        return $this->hasMany('App\PayrollUsers');
    }

    public function payroll_adjustments() {
        return $this->hasMany('App\PayrollAdjustment');
    }

    public function getStartDateAttribute() {
        return Carbon::parse($this->attributes['start'])->format('m/d/Y');
    }

    public function getEndDateAttribute() {
        return Carbon::parse($this->attributes['end'])->format('m/d/Y');
    }

    public function getCreatedDateAttribute() {
        return Carbon::parse($this->created_at)->format('m/d/Y');
    }

    public function getPayrollStatusAttribute() {
        return ($this->status == self::$status_pending ? 'Pending' : 'Approved');
    }
}
