<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class GatePass extends Model
{
    use SoftDeletes;
    use Auditable;

    public static $unseen = 0;
    public static $seen = 1;
    public static $status_complete = 2;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'client_id',
        'customer_id',
        'booking_ref_no',
        'zone',
        'status',
    ];
}
