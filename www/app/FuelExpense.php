<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuelExpense extends Model
{
    protected $fillable = ['cost', 'user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function scopeCurrentFuelCost($query){
        return $query->orderBy('created_at', 'desc')->first();
    }
}
