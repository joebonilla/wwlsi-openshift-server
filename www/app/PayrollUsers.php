<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PayrollUsers extends Model
{
    protected $fillable = ['transaction_id', 'payroll_id', 'status'];



    public function payrolls() {
        return $this->belongsTo('App\Payroll');
    }

    public function users() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function transactions() {
        return $this->belongsTo('App\Transaction', 'transaction_id');
    }
}
