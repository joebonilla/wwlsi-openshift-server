<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class TransactionExpense extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = ['transaction_id', 'title', 'approved_by' ,'reflect_on_sales', 'cost', 'note'];

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;

    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }

    public function approved_by_user(){
        return $this->belongsTo('App\User', 'approved_by');
    }

    public function request(){
        return $this->hasOne('App\TransactionExpenseRequest');
    }

    public function getCostSalesAttribute(){
        if($this->reflect_on_sales){
            return "<span style=\"color: green;\">+ ₱ " . number_format($this->cost, 2) . "</span>";
        } else {
            return "<span style=\"color: red;\">- ₱  " . number_format($this->cost, 2) . "</span>";
        }
    }

    public function scopeAllReflectOnSales($query){
        return $query->where('liquidation_status', self::STATUS_APPROVED)->where('reflect_on_sales', 1);
    }

    public function scopeAllApprovedExpense($query){
        return $query->where('liquidation_status', self::STATUS_APPROVED);
    }

    public function scopeAllExpenseSales($query){
        return $query->where('liquidation_status', self::STATUS_APPROVED)->where('reflect_on_sales', 0);
    }
}
