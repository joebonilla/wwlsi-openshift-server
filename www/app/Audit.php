<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    public $keyLabel = [
        'note' => 'Transaction Note',
        'status' => 'Status',
        'type' => 'Transport Type',
        'pre_advice_status' => 'Pre-advice status',
        'vat_percent' => 'Vat Percent',
        'with_vat' => 'With Vat',
    ];

    public $displayValue = [
        'status' => [
            0 => 'New',
            1 => 'On Going',
            2 => 'Complete',
        ],

        'pre_advice_status' => [
            0 => 'Outside CY',
            1 => 'No',
            2 => 'Yes',
        ],

        'type' => [
            0 => 'LCL',
            1 => 'FCL'
        ],

        'with_vat' => [
            0 => 'No',
            1 => 'Yes'
        ],
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function auditable(){
        return $this->morphTo();
    }

    public function displayAudit(){
        $auditType = $this->type;
        $oldval = (array) json_decode($this->old);
        $newval = (array) json_decode($this->new);
        $keys = array_keys($newval);
        $returnText = '';
        
        if($auditType == 'updated'){
            foreach($keys as $key){
                if(isset($this->keyLabel[$key])){
                    $returnText .= "Changed <b>" . $this->keyLabel[$key] . "</b> 
                                from \"" . $this->transformVal($key, $oldval[$key]) . "\" to 
                                \"" . $this->transformVal($key, $newval[$key]) . "\" <br>";
                }
            }
        } else if ($auditType == 'deleted'){
            switch ($this->auditable_type){
                case 'App\TransactionContainer' :
                    $returnText .= $this->displayDeletedContainer($oldval);
                    break;
                case 'App\TransactionExpense' :
                    $returnText .= $this->displayDeletedCost($oldval);
                    break;
            }
        } else {
            switch ($this->auditable_type){
                case 'App\Transaction' :
                    $returnText .= $this->displayCreatedTransaction($newval);
                    break;
                case 'App\TransactionNote' :
                    $returnText .= $this->displayCreatedNote($newval);
                    break;
                case 'App\TransactionExpense' :
                    $returnText .= $this->displayAddedExpense($newval);
                    break;
                case 'App\TransactionContainer' :
                    $returnText .= $this->displayAddedContainer($newval);
                    break;
                case 'App\TransactionChassis' :
                    $returnText .= $this->displayAddedChasis($newval);
                    break;
            }
        }

        return $returnText;
    }

    public function transformVal($key, $val){
        if($key == "vat_percent"){
            switch ($val){
                case 0:
                    $val = "0%";
                    break;
                case 0.06:
                    $val = "6%";
                    break;
                case 0.12:
                    $val = "12%";
                    break;
            }
        }

        return isset($this->displayValue[$key][$val]) ? $this->displayValue[$key][$val] : $val;
    }

    public function displayCreatedTransaction($newval){
        return 'Created a transaction';
    }

    public function displayCreatedNote($newval){
        return 'Added note';
    }

    public function displayAddedExpense($newval){
        return 'Added <b>' . $newval['title'] . '</b> expense';
    }

    public function displayAddedChasis($newval){
        $chassis = Chassis::find($newval['chassis_id']);
        return 'Assigned Chassis <b>' . $chassis->name . '</b>.';
    }
    
    public function displayAddedContainer($newval){
        $container = ContainerType::find($newval['container_type_id']);
        return 'Added Container Type <b>' . $container->name . '</b> with ref# of '. $newval['container_ref_id'];
    }

    public function displayDeletedContainer($oldval){
        $container = ContainerType::find($oldval['container_type_id']);
        return 'Remove Container Type <b>' . $container->name . '</b> with ref# of '. $oldval['container_ref_id'];
    }

    public function displayDeletedCost($oldval){
        return 'Removed <b>' . $oldval['title'] . '</b> expense';
    }

}
