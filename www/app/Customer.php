<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class Customer extends Model
{
    use Auditable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['client_id','name','price'];

    public function address()
    {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function client(){
        return $this->belongsTo('App\Client');
    }
}
