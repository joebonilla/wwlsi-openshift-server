<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class Truck extends Model
{
    use SoftDeletes;
    use Auditable;

    public static $status_available = 1;
    public static $status_on_going = 2;
    public static $status_under_repair = 3;
    public static $status_retired = 4;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'manufacturer', 'plate_no', 'description', 'truck_status_id'];

    public function status()
    {
        return $this->belongsTo('App\TruckStatus', 'truck_status_id');
    }

    public function repair_histories()
    {
        return $this->hasMany('App\TruckRepairHistory');
    }

    public static function scopeAvailable($query)
    {
        return $query->where('truck_status_id', self::$status_available)->get();
    }

    public static function scopeUnderRepair($query)
    {
        return $query->where('truck_status_id', self::$status_under_repair)->get();
    }

    public static function scopeOnGoing($query)
    {
        return $query->where('truck_status_id', self::$status_on_going)->get();
    }

    public static function scopeAvailableAndCurrTruck($query, $transaction_id)
    {
        return $query->leftJoin('transactions', 'trucks.id', '=', 'transactions.truck_id')
            ->where('truck_status_id', self::$status_available)->orWhere('transactions.id', $transaction_id)->select('trucks.*')->get();

    }

    public function transaction()
    {
        return $this->hasMany('App\Transaction');

    }
}
