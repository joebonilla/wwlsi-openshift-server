<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class Expense extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = ['title', 'cost', 'default', 'description'];

    public function getDefaultTextAttribute(){
        return ($this->default == 1) ? 'Yes' : 'No';
    }

    public function scopeDefault($query){
        $query->where('default', '1');
    }
}
