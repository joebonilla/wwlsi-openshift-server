<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class TruckStatus extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    public function trucks()
    {
        return $this->hasMany('App\Truck');
    }
}
