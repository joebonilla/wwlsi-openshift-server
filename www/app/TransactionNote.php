<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;

class TransactionNote extends Model
{
    use Auditable;

    protected $fillable = ['note', 'transaction_id', 'user_id'];

    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }

    public function audits(){
        return $this->morphMany('App\Audit', 'auditable');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
