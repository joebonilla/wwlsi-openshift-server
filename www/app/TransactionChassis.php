<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;

class TransactionChassis extends Model
{
	use Auditable;

	const ON_USE = 0;
	const FINISH = 1;

	protected $table = 'transaction_chassis';
	protected $fillable = ['status', 'chassis_id'];

    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }

    public function chassis(){
        return $this->belongsTo('App\Chassis');
    }
}
