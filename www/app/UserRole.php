<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class UserRole extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
