<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Auditable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    use Auditable;
    public static $status_available = 1;
    public static $status_on_going = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'contact_no', 'user_role_id', 'user_status_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles() {
        return $this->belongsToMany('App\Role'); //,'assigned_roles'
    }

    public static function scopeGetUserByRole($query, $role) {
        return $query->whereHas('roles', function ($query) use ($role) {
            $query->where('name', 'like', '%' . $role . '%');
        });
    }

    public static function scopeGetAvailableDriver($query, $transaction_id = 0) {
        return $query->whereHas('roles', function ($query) {
            $query->where('name', 'driver');
        })->whereNotIn('users.id',function($query) use ($transaction_id) {
            $query->select('driver_id')->from('transactions')
                ->where('id', '!=', $transaction_id)
                ->where('status', Transaction::$status_new)
                ->orWhere('status', Transaction::$status_on_going);

        })->groupBy('users.id')->get();
    }

    public static function scopeGetAvailableHelper($query, $transaction_id = 0) {
        return $query->whereHas('roles', function ($query) {
            $query->where('name', 'helper');
        })->whereNotIn('users.id',function($query) use ($transaction_id) {
            $query->select('helper_id')->from('transactions')
                ->where('id', '!=', $transaction_id)
                ->where('status', Transaction::$status_new)
                ->orWhere('status', Transaction::$status_on_going);
        })->groupBy('users.id')->get();
    }

    public function hasRoles($role){
        return in_array($role,$this->roles->pluck('name')->toArray());
    }

    public function getRoleAttribute(){
        return $this->roles()->first()->name;
    }

    public function audits() {
        return $this->hasMany('App\Audit');
    }

    public function payrolls() {
        return $this->hasOne('App\Payroll');
    }

    public function payroll_users() {
        return $this->hasMany('App\PayrollUsers', 'user_id');
    }

    public function transaction_driver() {
        return $this->hasMany('App\Transaction', 'driver_id');
    }

    public function transaction_helper() {
        return $this->hasMany('App\Transaction', 'helper_id');
    }

    public function truck_repair_history() {
        return $this->hasMany('App\TruckRepairHistory', 'request_user_id');
    }
}
