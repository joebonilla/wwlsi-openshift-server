<?php

namespace App\Http\Controllers;

use App\Port;
use Illuminate\Http\Request;

class PortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $ports = Port::all();

        return view('ports.index', compact('ports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('ports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'address_name' => 'required',
        ]);

        $id = Port::create($request->all());
        $port = Port::find($id->id);
        $port->address()->create($request->all());
        return redirect(route('ports.index'))->with('success', 'Port Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Port $port
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $port = Port::find($id);
        return view('ports.show', compact('port'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Port $port
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $port = Port::find($id);

        return view('ports.edit', compact('port'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Port $port
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'address_name' => 'required',
        ]);
        Port::find($id)->update($request->all());

        return redirect(route('ports.show', $id))
            ->with('success', 'Ports Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Port $port
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Port::find($id)->delete();
        return redirect()->route('ports.index')
            ->with('success', 'Ports deleted successfully');
    }
}
