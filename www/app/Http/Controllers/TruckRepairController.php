<?php

namespace App\Http\Controllers;

use App\TruckRepairHistory;
use Illuminate\Http\Request;

class TruckRepairController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'repair_description' => 'required',
            'cost' => 'required',
            'date_repair' => 'required',
            'contractor_name' => 'required',
        ]);
        TruckRepairHistory::create($request->all());
        return redirect(route('trucks.show', $request->truck_id))->with('success', 'Truck Repair Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'repair_description' => 'required',
            'cost' => 'required',
            'date_repair' => 'required',
            'contractor_name' => 'required',
        ]);
        $truck_repair_history = TruckRepairHistory::find($id);
        $truck_repair_history->update($request->all());
        return redirect(route('trucks.show', $request->truck_id))->with('success', 'Truck Repair Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request) {
        $truck_repair_history = TruckRepairHistory::find($id);
        $truck_repair_history->delete();
        return redirect(route('trucks.show', $request->truck_id))->with('success', 'Truck Repair Deleted Successfully');
    }
}
