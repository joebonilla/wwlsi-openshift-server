<?php

namespace App\Http\Controllers;

use App\Expense;
use App\FuelExpense;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $expenses = Expense::all();
        $currentFuelExpense = FuelExpense::orderBy('created_at', 'desc')->first();
        $fuel_expenses = FuelExpense::orderBy('created_at', 'desc')->get();

        return view('expenses.index', compact('expenses', 'currentFuelExpense', 'fuel_expenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('expenses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'cost' => 'required',
            'description' => 'required',
        ]);

        $expense = new Expense($request->all());
        $expense->save();

        return redirect(route('expenses.index'))->with('success', 'Expenses save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //no show because when user view expense
        //it will automatically go to edit
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $expense = Expense::find($id);

        return view('expenses.edit', compact('expense'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'cost' => 'required',
            'description' => 'required',
        ]);

        $expense = Expense::find($id);
        $expense->title = $request->title;
        $expense->cost = $request->cost;
        $expense->default = ($request->default) ? 1 : 0;
        $expense->description = $request->description;
        $expense->update();

        return redirect(route('expenses.index'))->with('success', 'Expenses updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Expense::find($id)->delete();

        return redirect(route('expenses.index'))->with('success', 'Expenses deleted successfully');
    }

    public function storeFuelCost(Request $request) {
        $inputs = $request->all();
        $inputs['user_id'] = $request->user()->id;

        FuelExpense::create($inputs);

        return redirect(route('expenses.index') . '#fuel-expense-tab')->with('success', 'Fuel cost updated successfully');
    }

    //ajax
    public function getExpense($id) {
        return response()->json(Expense::find($id));
    }
}
