<?php

namespace App\Http\Controllers;

use App\Truck;
use App\TruckRepairHistory;
use App\TruckStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TruckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $trucks = Truck::all();

        return view('trucks.index', compact('trucks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('trucks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:trucks,name',
            'manufacturer' => 'required',
            'plate_no' => 'required|unique:trucks,plate_no',
            'description' => 'required',
        ]);

//        $count = 0;
        Truck::create($request->all());
//        $request_user_id = Auth()->id();

//        if (!empty($request->input('repair_description')[0])) {
//
//            foreach ($request->input('repair_description') as $repair) {
//                $input = [
//                    'truck_id' => $trucks->id,
//                    'request_user_id' => $request_user_id,
//                    'repair_description' => $repair,
//                    'status' => TruckRepairHistory::$status_pending,
//                    'cost' => $request->input('cost')[$count],
//                    'date_repair' => new Carbon($request->input('date_repair')[$count]),
//                    'created_at' => Carbon::now(),
//                    'updated_at' => Carbon::now(),
//                ];
//
//                $count++;
//
//                $trucks->repair_histories()->insert($input);
//            }
//        }

        return redirect(route('trucks.index'))->with('success', 'Truck Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $truck = Truck::find($id);
        $transaction_history = $truck->transaction()->get();
        $total_cost = 0;
        foreach ($truck->repair_histories as $truck_repair_cost) {
            $total_cost += $truck_repair_cost->cost;
        }
        return view('trucks.show', compact('truck', 'transaction_history', 'total_cost'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $truck = Truck::find($id);
        $status = TruckStatus::all();

        return view('trucks.edit', compact('truck', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        if (!empty($request->update_status)) {
            $truck_repair_history = TruckRepairHistory::find($request->repair_history_id);
            $truck_repair_history->update($request->all());
            return redirect()->route('accounting.index');
        }

        $this->validate($request, [
            'name' => 'required',
            'manufacturer' => 'required',
            'plate_no' => 'required',
            'description' => 'required',
        ]);
        $trucks = Truck::find($id);
        $trucks->update($request->all());
        return redirect()->route('trucks.show', $id)->with('success', 'Truck updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Truck::find($id)->delete();
        TruckRepairHistory::where('truck_id', $id)->delete();
        return redirect()->route('trucks.index')
            ->with('success', 'Truck deleted successfully');
    }

}
