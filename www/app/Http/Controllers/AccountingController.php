<?php

namespace App\Http\Controllers;

use App\Client;
use App\Payroll;
use App\PayrollAdjustment;
use App\Transaction;
use App\TransactionExpense;
use App\Truck;
use App\TruckRepairHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AccountingController extends Controller
{
    public function index() {

        $transactions = Transaction::all();
        $transactions_expense = TransactionExpense::has('transaction')->get();
        $payrolls = Payroll::where('status', Payroll::$status_pending)->get();
        $payroll_adjustments = PayrollAdjustment::where('status', PayrollAdjustment::$status_pending)->get();
        $truck_repair_history = TruckRepairHistory::where('status', TruckRepairHistory::$status_pending)->get();
        return view('accounting.index', compact('transactions', 'transactions_expense', 'payrolls', 'payroll_adjustments', 'truck_repair_history'));
    }

    public function approveCost(Request $request, $expense_id) {
        $expense = TransactionExpense::find($expense_id);
        $expense->liquidation_status = $request->liquidation_status;
        $expense->approved_by = $request->user()->id;
        $expense->date_approved = new \DateTime();
        $expense->update();

        return redirect(route('accounting.index'));
    }

    public function generateReport(Request $request) {
        $total_cost_expenses = 0;
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        if (!empty($request->client)) {
            foreach ($request->client as $client) {
                $find_client_by_id = Client::find($client);
//                    ->whereBetween('updated_at', array($request->date_from, $request->date_to))->get();
//                dd($find_client_by_id);
                foreach ($find_client_by_id->transactions as $transaction) {
                    foreach ($transaction->transaction_expenses->where('liquidation_status', TransactionExpense::STATUS_APPROVED) as $expenses) {
                        $total_cost_expenses += $expenses->cost;
                    }

                }

                $pdf = App::make('dompdf.wrapper');
                $pdf->loadView('accounting.print_per_client', compact('find_client_by_id', 'total_cost_expenses', 'date_from', 'date_to'))->setPaper('a4', 'landscape');
                return $pdf->download($find_client_by_id->name . '_income_statement_report.pdf');
            }
        } else {
            return redirect()->back();
        }
    }
}
