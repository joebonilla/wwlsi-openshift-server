<?php

namespace App\Http\Controllers;

use App\ContainerType;
use Illuminate\Http\Request;

class ContainerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $containerTypes = ContainerType::all();

        return view('containerTypes.index', compact('containerTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('containerTypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'payloads' => 'required',
            'cubic_capacity' => 'required',
            'interior_length' => 'required',
            'interior_width' => 'required',
            'interior_height' => 'required',
        ]);
        ContainerType::create($request->all());

        return redirect(route('containerTypes.index'))->with('success', 'Container save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $containerType = ContainerType::find($id);

        return view('containerTypes.show', compact('containerType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $containerType = ContainerType::find($id);

        return view('containerTypes.edit', compact('containerType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'payloads' => 'required',
            'cubic_capacity' => 'required',
            'interior_length' => 'required',
            'interior_width' => 'required',
            'interior_height' => 'required',
        ]);
        ContainerType::find($id)->update($request->all());

        return redirect(route('containerTypes.show', $id))->with('success', 'Container updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}