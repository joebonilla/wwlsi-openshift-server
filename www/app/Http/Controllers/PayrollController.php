<?php

namespace App\Http\Controllers;

use App\Payroll;
use App\PayrollAdjustment;
use App\PayrollUsers;
use App\Transaction;
use App\User;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;

class PayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = Payroll::orderBy('created_at', 'DESC')->get();

        return view('payrolls.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $user = Transaction::with(['driver', 'helper'])->get();
        $driver = [];
        $helper = [];
        foreach ($user as $users) {
            $driver[$users->driver->id] = $users->driver->name;
            $helper[$users->helper->id] = $users->helper->name;
        }
        $helper_driver = $driver + $helper;
        return view('payrolls.create', compact('user', 'helper_driver'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'start' => 'required',
            'end' => 'required',
        ]);

        $request_user_id = Auth()->id();
        $start = new Carbon($request->start);
        $end = new Carbon($request->end);
        $param = [];
        $input = [];
        $payroll_report = '';
        $users_role = User::find($request->users);
        foreach ($users_role as $roles) {

            foreach ($roles->roles as $role) {
                $param[] = $role->name . '_id';
            }
        }

        foreach ($param as $parameter) {
            $payroll_report = Transaction::wherein($parameter, $request->users)->whereBetween('created_at', [$start, $end])->TransactionComplete();

        }

        if ($payroll_report->isEmpty()) {
            return Redirect::back()->withErrors(['Empty Result', 'Please select another date range']);
            exit();
        }


        foreach ($request->users as $users) {

            $payroll_input = [
                'request_user_id' => $request_user_id,
                'start' => $start,
                'end' => $end,
                'user_id' => $users,
                'status' => Payroll::$status_pending,
                'adjustment_value' => $request->adjustment_value,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            $payroll = Payroll::create($payroll_input);

            foreach ($payroll_report as $report) {

                if ($report->driver_id == $users or $report->helper_id == $users) {

                    $input[] = [
                        'payroll_id' => $payroll->id,
                        'transaction_id' => $report->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];

                }
            }
        }
        PayrollUsers::insert($input);
        return redirect()->route('payrolls.index')
            ->with('success', 'Payroll Generated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request) {
        $payroll_users = Payroll::where('id', $id)->with(['payroll_users', 'payroll_adjustments'])->get();
        $payroll_id = $id;
        $payroll_for_name = User::find($request->user_id);
        $total_pay = 0;

        $total_with_adjustment = 0;

        foreach ($payroll_users as $item) {
            foreach ($item->users->roles as $role) {
                foreach ($item->payroll_users as $data) {
                    $total_pay += $data->transactions->customer->price * ($role->display_name == 'Driver' ? 0.12 : 0.06);
                }
            }
        }

        foreach ($payroll_users as $payroll) {
            foreach ($payroll->payroll_adjustments as $adjustments) {
                if ($adjustments->status == PayrollAdjustment::$status_complete) {
                    if ($adjustments->operator == '+') {
                        $total_with_adjustment += $adjustments->value;
                    } else {
                        $total_with_adjustment -= $adjustments->value;
                    }
                }
            }
        }
        return view('payrolls.show', compact('payroll_id', 'payroll_for_name', 'payroll_users', 'total_pay', 'total_with_adjustment', 'payroll_users_show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $payroll = Payroll::find($id);
        if ($request->status) {
            $payroll->where('user_id', $request->user_id)->update(['status' => $request->status]);
        } else {
            $payroll->update($request->all());
        }


        return redirect()->route('payrolls.index')
            ->with('success', 'Payroll Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function printPayrollPdf($id) {

        $payroll_users = Payroll::where('id', $id)->with(['payroll_users', 'payroll_adjustments'])->get();
        $payroll_id = $id;
        foreach ($payroll_users as $item) {
            $payroll_for_name = User::find($item->user_id);
            $payroll_request_name = User::find($item->request_user_id);
        }

        $total_pay = 0;
        $total_with_adjustment = 0;

        foreach ($payroll_users as $item) {
            foreach ($item->users->roles as $role) {
                foreach ($item->payroll_users as $data) {
                    $total_pay += ($data->transactions->customer->price - $item->adjustment_value) * ($role->display_name == 'Driver' ? 0.12 : 0.06);
                }
            }
        }

        foreach ($payroll_users as $payroll) {
            foreach ($payroll->payroll_adjustments as $adjustments) {
                if ($adjustments->status == PayrollAdjustment::$status_complete) {
                    if ($adjustments->operator == '+') {
                        $total_with_adjustment += $adjustments->value;
                    } else {
                        $total_with_adjustment -= $adjustments->value;
                    }
                }
            }
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('payrolls.print', compact('payroll', 'payroll_id', 'payroll_for_name', 'payroll_users', 'total_pay', 'total_with_adjustment', 'payroll_request_name'))->setPaper('a4', 'landscape');

        return $pdf->download($payroll_for_name->name . '_payslip.pdf');
    }

    public function getColumns() {
        return $columns = [
            'id',
            'request_user_id',
            'user_id',
            'status',
            'start',
            'end',
            'created_at',
        ];
    }

    public function getPayrollCompleteData() {

        $query = Payroll::with('users', 'users_driver_helper')->where('status', Payroll::$status_complete)
            ->select($this->getColumns());
        return $this->dataTableBuild($query);
    }

    public function getPayrollPendingData() {
        $query = Payroll::with('users', 'users_driver_helper')->where('status', Payroll::$status_pending)
            ->select($this->getColumns());
        return $this->dataTableBuild($query);
    }

    public function dataTableBuild($query) {

        return Datatables::of($query)
            ->editColumn('status', function ($query) {
                return $this->transformStatus($query);
            })
            ->editColumn('created_at', function ($query) {
                return $this->transformCreatedAt($query);
            })
            ->editColumn('start', function ($query) {
                return $this->StartEnd($query);
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%m/%d/%Y') like ?", ["%$keyword%"]);
            })
            ->addColumn('action', function ($query) {
                $data = $query;
                return view('payrolls.buttons', compact('data'))->render();
            })
            ->make(true);
    }

    public function StartEnd($query) {
        return Carbon::parse($query->start)->format('m/d/Y') . ' - ' . Carbon::parse($query->end)->format('m/d/Y');
    }

    public function transformType($query) {
        return ($query->type == Transaction::LCL) ? 'LCL' : 'FCL';
    }

    public function transformStatus($query) {
        return ($query->status == Payroll::$status_pending ? 'Pending' : 'Approved');
    }

    public function transformCreatedAt($query) {
        return Carbon::parse($query->created_at)->format('m/d/Y');
    }

    public function createActions() {

    }
}
