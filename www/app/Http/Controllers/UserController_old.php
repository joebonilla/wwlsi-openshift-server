<?php

namespace App\Http\Controllers;

use App\Truck;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Object_;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        User::create($request->all());

        return redirect(route('users.index'));
    }

    public function create()
    {
        $user_roles = UserRole::all();

        return view('users.create', compact('user_roles'));
    }

    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function delete()
    {
        return 'Cannot delete users for now';
    }

    public function update(Request $request, $id)
    {
        $updated_user = User::find($id);

        $updated_user->update($request->all());

        return redirect(route('users.show', $id));
    }

    public function edit($id)
    {
        $user = User::find($id);

        $user_roles = UserRole::all();

        return view('users.edit', compact('user', 'user_roles'));
    }

    public function show($id)
    {
        $user = User::find($id);
        $audit_array = [];
        foreach ($user->audits as $audit) {
            $audit_new = json_decode($audit->new);
            $audit_old = json_decode($audit->old);
            $audit_array[] = [
                'Type' => $audit->type,
                'new' => [
                    $audit_new
                ],
                'old' => [
                    $audit_old
                ],
                'url' => $audit->route,
                'created_at_date' => Carbon::parse($audit->created_at)->toDateString(),
                'created_at_ago' => Carbon::parse($audit->created_at)->diffForHumans(),
                'id' => $audit->auditable_id
            ];
        }
        return view('users.show', compact('user', 'audit_array'));
    }
}
