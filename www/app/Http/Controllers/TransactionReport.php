<?php

namespace App\Http\Controllers;

use App\Client;
use App\ContainerType;
use App\Customer;
use App\Port;
use App\Transaction;
use App\Truck;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\TransactionReportRepositories;

class TransactionReport extends Controller
{
    private $transactionReportRepository;

    public function __construct(TransactionReportRepositories $transactionReportRepository) {
        $this->transactionReportRepository = $transactionReportRepository;
    }

    public function index(){
        $clients = Client::all();
        $drivers = User::getUserByRole('Driver')->get();
        $helpers = User::getUserByRole('Helper')->get();
        $trucks = Truck::all();
        return view('transactions_report.index', compact(
            'clients', 'drivers', 'helpers', 'trucks'
        ));
    }

    public function generate(){
        $export_data = [];
        $inputs = request([
            'status',
            'client_id',
            'customer_id',
            'driver_id',
            'helper_id',
            'truck_id',
            'created_from',
            'created_to'
        ]);

        $transactions = Transaction::select('*');
        foreach($inputs as $key => $value){
            if($value){
                if($key == 'created_from'){
                    $transactions->whereDate('created_at', '>=',$value);
                } elseif ($key == 'created_to') {
                    $transactions->whereDate('created_at', '<=', $value);
                } else {
                    $transactions->where($key, $value);
                }
            }
        }

        $extracted_data = $transactions->get();

        foreach ($extracted_data as $transaction){
            $data['Date Added'] = $transaction->created_at;
            $data['Ref#'] = $transaction->id;
            $data['Truck No. / Plate No.'] = $transaction->truck->id . ' / ' . $transaction->truck->plate_no;
            $data['Status'] = $transaction->status_label;
            $data['Driver'] = $transaction->driver->name;
            $data['Helper'] = $transaction->helper->name;
            $data['Client'] = $transaction->client->name;
            $data['Customer'] = $transaction->customer->name;
            $data['Port of Origin'] = $transaction->port->name;
            $data['Destination'] = $transaction->destination_address;
            $data['Total Expense'] = (int) $transaction->transaction_expenses()->allApprovedExpense()->sum('cost');
            $data['Fuel Expense'] = $transaction->currentFuelCost;
            $data['Date Completed'] = $transaction->date_completed;

            $export_data[] = $data;
        }

        Excel::create('transaction_report_' . date('Ymd'), function($excel) use ($export_data) {

            $excel->sheet('transaction report', function($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });

        })->export('xls');
    }

    public function import(Request $request){
        Excel::load($request->transactions->path() , function($reader) use ($request) {
            $ongoing_transactions = Transaction::ongoing()->get();
            $unavailable_drivers = $ongoing_transactions->pluck('driver_id')->toArray();
            $unavailable_helpers = $ongoing_transactions->pluck('helper_id')->toArray();
            $unavailable_trucks = $ongoing_transactions->pluck('truck_id')->toArray();

            $transaction_data = $reader->toArray()[0];

            foreach($transaction_data as $data){
                if(!in_array( (int)$data['driver_id'], $unavailable_drivers) && !in_array( (int)$data['truck_id'], $unavailable_trucks) && !in_array( (int)$data['helper_id'], $unavailable_helpers)){
                    $transaction = new Transaction;

                    $transaction->created_by_id = $request->user()->id;
                    $transaction->truck_id = (int) $data['truck_id'];
                    $transaction->driver_id = (int) $data['driver_id'];
                    $transaction->helper_id = (int) $data['helper_id'];
                    $transaction->client_id = (int) $data['client_id'];
                    $transaction->customer_id = (int) $data['customer_id'];
                    $transaction->port_id = (int) $data['port_id'];
                    $transaction->vat_percent = (float) $data['vat_percent'];
                    $transaction->note = $data['note'];

                    $transaction->save();

                    $unavailable_drivers[] = $data['driver_id'];
                    $unavailable_helpers[] = $data['helper_id'];
                    $unavailable_trucks[] = $data['truck_id'];
                }
            }
        });
    }

    public function downloadTemplate(){
        $headers[] = $this->transactionReportRepository->getTemplateHeaders();
        $available_utilities['trucks'] = $this->transactionReportRepository->getAvailableTrucksTemplate();
        $available_utilities['drivers'] = $this->transactionReportRepository->getAvailableDriversTemplate();
        $available_utilities['helpers'] = $this->transactionReportRepository->getAvailableHelpersTemplate();

        $transaction_data['clients'] = Client::select('clients.id as Client ID', 'clients.name as Client Name', 'customers.id as Customer ID', 'customers.name as Customer Name')
                                        ->leftJoin('customers', 'clients.id', 'customers.client_id')->get()->toArray();

        $transaction_data['container_types'] = ContainerType::select('id', 'name')->get()->toArray();
        $transaction_data['ports'] = Port::select('id', 'name')->get()->toArray();

        Excel::create('transaction_template', function($excel) use ($headers, $available_utilities, $transaction_data) {
            $excel->sheet('Transaction Import', function($sheet) use ($headers) {
                $sheet->fromArray($headers);
            });

            $excel->sheet('Available Utilities', function($sheet) use ($available_utilities) {
                if(!empty($available_utilities['trucks'])){
                    $sheet->fromArray(['Trucks' => ['Trucks']]);
                    $sheet->fromArray($available_utilities['trucks']);
                }

                if(!empty($available_utilities['drivers'])){
                    $sheet->fromArray(['Drivers' => ['Drivers']]);
                    $sheet->fromArray($available_utilities['drivers']);
                }

                if(!empty($available_utilities['helpers'])){
                    $sheet->fromArray(['Helpers' => ['Helpers']]);
                    $sheet->fromArray($available_utilities['helpers']);
                }
            });

            $excel->sheet('Transaction Data', function($sheet) use ($transaction_data) {
                if(!empty($transaction_data['clients'])){
                    $sheet->fromArray(['Clients' => ['Clients']]);
                    $sheet->fromArray($transaction_data['clients']);
                }

                if(!empty($transaction_data['container_types'])){
                    $sheet->fromArray(['Container Types' => ['Container Types']]);
                    $sheet->fromArray($transaction_data['container_types']);
                }

                if(!empty($transaction_data['ports'])){
                    $sheet->fromArray(['Ports' => ['Ports']]);
                    $sheet->fromArray($transaction_data['ports']);
                }
            });
        })->export('xls');
    }
}
