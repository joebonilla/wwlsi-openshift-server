<?php

namespace App\Http\Controllers;

use App\Client;
use App\ContainerType;
use App\Customer;
use App\Expense;
use App\Port;
use App\Transaction;
use App\TransactionAudit;
use App\TransactionExpense;
use App\TransactionChassis;
use App\Truck;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TransactionRepository;
use App\Repositories\ChassisRepository;
use App\Http\Requests\StoreTransaction;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

class TransactionController extends Controller
{
    private $transactionRepository;
    private $chassisRepository;

    public function __construct(TransactionRepository $transactionRepository, ChassisRepository $chassisRepository) {
        $this->transactionRepository = $transactionRepository;
        $this->chassisRepository = $chassisRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $analytics_data = $this->transactionRepository->getTransactionAnalyticsData();

        $dates['day']['start'] = Carbon::now()->startOfDay();
        $dates['day']['end'] = Carbon::now()->endOfDay();

        $dates['week']['start'] = Carbon::now()->startOfWeek();
        $dates['week']['end'] = Carbon::now()->endOfWeek();

        $dates['month']['start'] = Carbon::now()->startOfMonth();
        $dates['month']['end'] = Carbon::now()->endOfMonth();

        return view('transactions.index', compact('analytics_data', 'dates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $clients = Client::all();
        $drivers = User::getAvailableDriver();
        $helpers = User::getAvailableHelper();
        $containerTypes = ContainerType::all();
        $ports = Port::all();
        $trucks = Truck::available();
        $chassis = $this->chassisRepository->getAvailableChassis();

        return view('transactions.create', compact(
            'clients', 'drivers', 'helpers', 'containerTypes', 'ports', 'trucks', 'chassis'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransaction $request) {
        $inputs = $request->all();
        $inputs['created_by_id'] = $request->user()->id;
        $inputs['reference_no'] = $this->transactionRepository->generateReferenceNo();
        $inputs['kilometer'] = $this->transactionRepository->calculateKilometers($inputs['origin_address'], $inputs['destination_address']);

        if ($request->type == 1) { //is fcl
            $this->validate($request, [
                'container_ref_id' => 'required',
                'container_type_id' => 'required',
                'chassis_id' => 'required',
            ]);

            $transaction = Transaction::create($inputs);

            $transaction->container()->create($request->all());
            $transaction->transaction_chassis()->create([
                'chassis_id' => $request->chassis_id,
                'status' => TransactionChassis::ON_USE
            ]);
        } else {
            $transaction = Transaction::create($inputs);
        }

        $this->transactionRepository->addDefaultTransactionExpense($transaction);
        $this->transactionRepository->updateUtilitiesStatusToOngoing($request);

        return redirect(route('transactions.show', $transaction->id))->with('success', 'Transaction save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $transaction = Transaction::find($id);
        $total_expense_requests = $transaction->transaction_expense_requests()->sum('transaction_expense_requests.cost');
        $total_liquidated_expense = $transaction->transaction_expenses()->allApprovedExpense()->sum('cost');
        $expenses = Expense::all();
        $current_user = Auth::user();

        $transaction_note_audit = $this->transactionRepository->getTransactionAudits($transaction);

        return view('transactions.show', compact('transaction', 'expenses', 'transaction_note_audit', 'current_user', 'total_expense_requests', 'total_liquidated_expense'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $clients = Client::all();
        $drivers = User::getAvailableDriver($id);
        $helpers = User::getAvailableHelper($id);
        $containerTypes = ContainerType::all();
        $transaction = Transaction::find($id);
        $ports = Port::all();
        $trucks = Truck::availableAndCurrTruck($id);
        $chassis = $this->chassisRepository->getAvailableAndCurrChassis($id);

        return view('transactions.edit', compact(
            'transaction', 'clients', 'drivers', 'helpers', 'containerTypes', 'ports', 'trucks', 'chassis'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTransaction $request, $id) {
        $inputs = $request->all();

        $inputs['pre_advice_status'] = (isset($inputs['pre_advice_status'])) ? $inputs['pre_advice_status'] : Transaction::PREADVICE_OUTSIDE_CY;
        $inputs['with_vat'] = (isset($inputs['with_vat'])) ? $inputs['with_vat'] : 0;
        $inputs['kilometer'] = $this->transactionRepository->calculateKilometers($inputs['origin_address'], $inputs['destination_address']);

        $transaction = Transaction::find($id);

        $this->transactionRepository->updateUtilitiesStatusToAvailable($transaction);

        $this->transactionRepository->updateUtilitiesStatusToOngoing($request);

        $this->transactionRepository->updateTransactionContainer($request, $transaction);

        $transaction->update($inputs);

        return redirect(route('transactions.show', $id))->with('success', 'Transaction updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @todo for deliberation
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function removeCost(Request $request, $transaction_expense_id) {
        TransactionExpense::find($transaction_expense_id)->delete();

        $audit = new TransactionAudit;
        $audit->transaction_id = $request->transaction_id;
        $audit->audit_id = $transaction_expense_id;
        $audit->audit_type = 'App\TransactionExpense';
        $audit->save();

        return redirect(route('transactions.show', $request->transaction_id))->with('success', 'Transaction updated successfully');
    }

    public function addTransactionExpense(Request $request, $transaction_id) {
        $transaction = Transaction::find($transaction_id);

        $transaction->transaction_expenses()
                    ->create($request->all())
                    ->request()
                    ->create($request->all());

        return redirect(route('transactions.show', $transaction_id))->with('success', 'Transaction updated successfully');
    }

    public function addTransactionNote(Request $request, $transaction_id) {
        Transaction::find($transaction_id)
            ->transaction_notes()
            ->create($request->all());

        return redirect(route('transactions.show', $transaction_id))->with('success', 'Transaction updated successfully');
    }

    public function addTransactionRemarks(Request $request, $transaction_id) {
        Transaction::find($transaction_id)
            ->transaction_remarks()
            ->create($request->all());

        return redirect(route('transactions.show', $transaction_id))->with('success', 'Transaction updated successfully');
    }

    public function updateTransactionStatus(Request $request, $transaction_id) {
        $transaction = Transaction::find($transaction_id);

        if ($request->status == Transaction::$status_complete) {
            $this->transactionRepository->updateUtilitiesStatusToAvailable($transaction);
            $transaction->base_earn = $transaction->customer->price;
            $transaction->fuel_cost = $transaction->current_fuel_cost;
            $transaction->date_completed = new \DateTime();
        }

        $transaction->status = $request->status;

        $transaction->update();

        return redirect(route('transactions.show', $transaction_id))->with('success', 'Transaction updated successfully');
    }

    public function updateTransactionPreAdviceStatus(Request $request, $transaction_id) {
        $transaction = Transaction::find($transaction_id);
        $transaction->pre_advice_status = $request->pre_advice_status;
        $transaction->update();

        return redirect(route('transactions.show', $transaction_id))->with('success', 'Transaction updated successfully');
    }

    //ajax
    public function getClientCustomers($client_id) {
        $client = Client::find($client_id);
        $customer = (!empty($client)) ? $client->customers : [];

        return response()->json($customer);
    }

    public function getCustomers() {
        $customers = Customer::all();

        return response()->json($customers);
    }

    public function getCustomerAddress($customer_id) {
        $customer = Customer::find($customer_id);
        $customer_address = (!empty($customer)) ? $customer->address->address_name : '';

        return response()->json($customer_address);
    }

    public function getPortAddress($port_id) {
        $port = Port::find($port_id);
        $por_address = (!empty($port)) ? $port->address->address_name : '';

        return response()->json($por_address);
    }

    public function getTransactionCompleteData(Request $request) {
        $start_search = $request->start_search;
        $end_search = $request->end_search;
        $columns = [
            'id',
            'reference_no',
            'client_id',
            'customer_id',
            'type',
            'status',
            'destination_address',
            'date_completed',
        ];
        if (!empty($start_search)) {
            $query = Transaction::with(['customer', 'client', 'container'])->completed()->where('date_completed', '>=', Carbon::parse($start_search))->where('date_completed', '<=', Carbon::parse($end_search))
                ->select($columns);
        } else {
            $query = Transaction::with(['customer', 'client', 'container'])->completed()
                ->select($columns);
        }

        return $this->transactionRepository->dataTableBuildComplete($query, $start_search, $end_search);
    }

    public function getTransactionOngoingData(Request $request) {
        $start_search = $request->start_search;
        $end_search = $request->end_search;
        $columns = [
            'id',
            'reference_no',
            'client_id',
            'customer_id',
            'type',
            'status',
            'destination_address',
            'created_at',
        ];

        if (!empty($start_search)) {
            $query = Transaction::with(['customer', 'client', 'container'])->ongoing()->where('created_at', '>=', Carbon::parse($start_search))->where('created_at', '<=', Carbon::parse($end_search))
                ->select($columns);
        } else {
            $query = Transaction::with(['customer', 'client', 'container'])->ongoing()
                ->select($columns);
        }

        return $this->transactionRepository->dataTableBuildOnGoing($query);
    }

    public function exportExpenseToPdf(Transaction $transaction){
        $total_expense = $transaction->transaction_expenses()->sum('cost');
        $total_liquidated_expense = $transaction->transaction_expenses()->allApprovedExpense()->sum('cost');

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('transactions.print_expense', compact('transaction', 'total_expense', 'total_liquidated_expense'))->setPaper('a4', 'landscape');

        return $pdf->download($transaction->client->name . '_transaction_expense.pdf');
    }

    public function releaseChassis(Transaction $transaction){
        $transaction->transaction_chassis->update(['status' => TransactionChassis::FINISH]);

        return redirect(route('transactions.show', $transaction->id))->with('success', 'Transaction updated successfully');
    }
}