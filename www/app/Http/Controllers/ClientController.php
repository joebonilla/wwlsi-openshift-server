<?php

namespace App\Http\Controllers;

use App\Address;
use App\Client;
use App\Customer;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {


        if ($request->ajax()) {
            $clients = Client::has('transactions')->get();
            return response()->json($clients);
            exit();
        } else {
            $clients = Client::all();
        }
        return view('clients.index', compact('clients'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'address_name' => 'required',
        ]);

        $id = Client::create($request->all());
        $client = Client::find($id->id);
        $client->address()->create($request->all());
        return redirect(route('clients.index'))->with('success', 'Clients Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $client = Client::find($id);

        $customer = Customer::where('client_id', $id)->get();

        return view('clients.show', compact('client', 'customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $client = Client::find($id);

        return view('clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'address_name' => 'required',
        ]);
        $client = Client::find($id);
        $client->update($request->all());
        $client->address->update($request->all());
        return redirect(route('clients.show', $client))
            ->with('success', 'Clients Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Client::find($id)->delete();
        return redirect()->route('clients.index')
            ->with('success', 'Clients deleted successfully');
    }
}
