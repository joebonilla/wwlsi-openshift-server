<?php

namespace App\Http\Controllers;

use App\PayrollAdjustment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        $payroll_id = $request->payroll_id;
        $user_id = $request->user_id;
        return view('adjustments.create', compact('payroll_id', 'user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'description' => 'required',
            'operator' => 'required',
            'value' => 'required',
        ]);

        PayrollAdjustment::create($request->all());
        return redirect()->route('payrolls.show', $request->payroll_id . '?user_id=' . $request->user_id)
            ->with('success', 'Payroll Generated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $payroll_adjustments = PayrollAdjustment::find($id);
        $payroll_adjustments->update($request->all());
        return redirect()->route('payrolls.show', $payroll_adjustments->payroll_id . '?user_id=' . $request->user_id)
            ->with('success', 'Adjustment Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request) {
        $payroll_adjustments = PayrollAdjustment::find($id);
        $payroll_adjustments->delete();
        return redirect()->route('payrolls.show', $payroll_adjustments->payroll_id . '?user_id=' . $request->user_id)
            ->with('success', 'Adjustment deleted successfully');
    }
}
