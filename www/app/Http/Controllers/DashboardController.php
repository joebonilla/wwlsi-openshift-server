<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Transaction;
use App\Truck;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Driver\Driver;
use App\FuelExpense;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $data['available_trucks_count'] = Truck::available()->count();
        $data['on_going_trucks_count'] = Truck::onGoing()->count();
        $data['under_repair_trucks_count'] = Truck::underRepair()->count();

        $data['top_drivers'] = User::GetUserByRole('driver')->select('users.*', DB::raw('count(transactions.id) as total_transactions'))
                                ->join('transactions', 'transactions.driver_id', 'users.id')
                                ->groupBy('users.id')->orderBy('total_transactions', 'desc')->limit(5)->get();

        $data['top_helpers'] = User::GetUserByRole('helper')->select('users.*', DB::raw('count(transactions.id) as total_transactions'))
                                ->join('transactions', 'transactions.helper_id', 'users.id')
                                ->groupBy('users.id')->orderBy('total_transactions', 'desc')->limit(5)->get();

        $data['top_customers'] = Customer::select('customers.*', DB::raw('count(transactions.id) as total_transactions'))
                                ->join('transactions', 'transactions.customer_id', 'customers.id')
                                ->groupBy('customers.id')->orderBy('total_transactions', 'desc')->limit(5)->get();

        $data['transaction_this_week_count'] = Transaction::thisWeek()->count();
        $data['transaction_this_month_count'] = Transaction::thisMonth()->count();

        $data['current_fuel_expense'] = FuelExpense::currentFuelCost();

        return view('home', $data);
    }
}
