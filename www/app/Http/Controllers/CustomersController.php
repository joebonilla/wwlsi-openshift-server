<?php

namespace App\Http\Controllers;

use App\Client;
use App\Customer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id) {
        $clients = Client::where('client_id', $id)->first();
        return view('customers.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        $clients = Client::find($request->query('client_id'));
        return view('customers.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'address_name' => 'required',
        ]);
        $customer = Customer::create($request->all());
        $customer->address()->create($request->all());
        $clients = Client::find($request->input('client_id'));
        return redirect(route('clients.show', compact('clients')))->with('success', 'Customer Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {
        $customer = Customer::find($id);
        $client = Client::find($request->client);
        return view('customers.show', compact('client', 'customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
        $customer = Customer::find($id);
        $client = Client::find($request->client);
        return view('customers.edit', compact('client', 'customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'address_name' => 'required',
        ]);
        $customer = Customer::find($id);
        $customer->update($request->all());
        $customer->address->update($request->all());
        $client = Client::find($request->client);
        return redirect(route('customers.show', compact('client', 'customer')))
            ->with('success', 'Clients Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
