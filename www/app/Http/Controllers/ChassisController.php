<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chassis;
use App\Repositories\ChassisRepository;

class ChassisController extends Controller
{
    private $transactionRepository;

    public function __construct(ChassisRepository $chassisRepository){
        $this->chassisRepository = $chassisRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chassis_available = $this->chassisRepository->getAvailableChassis();
        $chassis_unavailable = $this->chassisRepository->getUnavailableChassis();

        return view('chassis.index', compact('chassis_available', 'chassis_unavailable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('chassis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'number' => 'required',
        ]);

        Chassis::create($request->all());

        return redirect(route('chassis.index'))->with('success', 'Chassis Registered successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chassis = Chassis::where('id', $id)->first();

        return view('chassis.edit', compact('chassis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'number' => 'required',
        ]);

        Chassis::where('id', $id)->update([
            'name' => $request->name,
            'type' => $request->type,
            'number' => $request->number,
        ]);

        return redirect(route('chassis.index'))->with('success', 'Chassis Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
