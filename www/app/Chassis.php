<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chassis extends Model
{
	protected $fillable = ['name', 'type', 'number'];

    public function transaction_chassis() {
        return $this->hasMany('App\TransactionChassis');
    }

    public function getCurrentTransactionAttribute(){
    	return $this->transaction_chassis()->where('status', TransactionCHassis::ON_USE)->first()->transaction;
    }
}
