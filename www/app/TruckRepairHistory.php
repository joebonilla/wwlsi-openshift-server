<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class TruckRepairHistory extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];
    protected $table = 'truck_repair_histories';
    protected $fillable = ['truck_id', 'request_user_id', 'repair_description', 'cost', 'status', 'contractor_name'];

    public static $status_pending = 1;
    public static $status_complete = 2;

    public function users() {
        return $this->belongsTo('App\User', 'request_user_id');
    }

    public function truck() {
        return $this->belongsTo('App\Truck');
    }

    public function getRepairDateAttribute() {
        return Carbon::parse($this->date_repair)->format('m/d/Y');
    }


    public function getRepairDescriptionAttribute() {
        if (empty($this->attributes['repair_description'])) {
            return 'Not Specified';
        } else {
            return $this->attributes['repair_description'];
        }
    }

    public function getRepairStatusAttribute() {
        return ($this->status == self::$status_pending ? 'Pending' : 'Approved');
    }
}
