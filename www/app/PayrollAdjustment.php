<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollAdjustment extends Model
{
    protected $fillable = ['payroll_id', 'description', 'operator', 'value', 'status'];
    public static $status_pending = 1;
    public static $status_complete = 2;

    public function payrolls() {
        return $this->belongsTo('App\Payrolls');
    }

    public function getPayrollStatusAttribute() {
        return ($this->status == self::$status_pending ? 'Pending' : 'Approved');
    }

    public function getOperatorTextAttribute() {
        return ($this->operator == '+' ? 'Add' : 'Minus');
    }
}
