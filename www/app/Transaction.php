<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class Transaction extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'client_id',
        'customer_id',
        'type',
        'port_id',
        'driver_id',
        'helper_id',
        'truck_id',
        'destination_address',
        'origin_address',
        'kilometer',
        'note',
        'pre_advice_status',
        'created_by_id',
        'with_vat',
        'vat_percent',
        'reference_no'
    ];

    public static $status_new = 0;
    public static $status_on_going = 1;
    public static $status_complete = 2;

    const LCL = 0;
    const FCL = 1;

    const PREADVICE_OUTSIDE_CY = 0;
    const PREADVICE_NO = 1;
    const PREADVICE_YES = 2;

    public function created_by() {
        return $this->belongsTo('App\User', 'created_by_id');
    }

    public function client() {
        return $this->belongsTo('App\Client');
    }

    public function driver() {
        return $this->belongsTo('App\User', 'driver_id');
    }

    public function helper() {
        return $this->belongsTo('App\User', 'helper_id');
    }

    public function container() {
        return $this->hasOne('App\TransactionContainer');
    }

    public function customer() {
        return $this->belongsTo('App\Customer');
    }

    public function truck() {
        return $this->belongsTo('App\Truck');
    }

    public function port() {
        return $this->belongsTo('App\Port', 'port_id');
    }

    public function transaction_expenses() {
        return $this->hasMany('App\TransactionExpense');
    }

    public function transaction_chassis() {
        return $this->hasOne('App\TransactionChassis');
    }

    public function transaction_expense_requests() {
        return $this->hasManyThrough('App\TransactionExpenseRequest', 'App\TransactionExpense');
    }

    public function transaction_notes() {
        return $this->hasMany('App\TransactionNote');
    }

    public function transaction_remarks() {
        return $this->hasMany('App\TransactionRemarks');
    }

    public function transaction_audits() {
        return $this->hasMany('App\TransactionAudit');
    }

    public function audits() {
        return $this->morphMany('App\Audit', 'auditable');
    }

    public function scopeOnGoing($query) {
        return $query->where('status', '!=', self::$status_complete);
    }

    public function scopeCompleted($query) {
        return $query->where('status', self::$status_complete);
    }

    public function scopeTransactionComplete($query) {
        return $query->where('status', self::$status_complete)->get();
    }

    public function scopeThisWeek($query) {
        return $query->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()] );
    }

    public function scopeThisMonth($query) {
        return $query->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()] );
    }

    public function scopeGetUserActiveTransaction($query, $user_id, $user_role) {
        if ($user_role == 'driver') {
            $query->where('driver_id', $user_id);
        } else if ($user_role == 'helper') {
            $query->where('helper_id', $user_id);
        } else {
            $query->where('created_by_id', $user_id);
        }

        $query->where('status', self::$status_new)->orWhere('status', self::$status_on_going);

        return $query->orderBy('created_at', 'desc')->first();
    }

    public function getStatusLabelAttribute() {
        return $this->convertStatusToText($this->status);
    }

    public function getTransportTypeAttribute() {
        return ($this->type == self::LCL) ? 'LCL' : 'FCL';
    }

    public function isFCL() {
        return ($this->type == self::FCL);
    }

    public function isLCL() {
        return ($this->type == self::LCL);
    }

    public function isComplete() {
        return ($this->status == self::$status_complete);
    }

    public function convertStatusToText($status) {
        $status_label = '';

        switch ($status) {
            case self::$status_new:
                $status_label = 'New';
                break;
            case self::$status_on_going:
                $status_label = 'On going';
                break;
            case self::$status_complete:
                $status_label = 'Complete';
                break;
        }

        return $status_label;
    }

    public function getRefNoAttribute(){
        return ($this->reference_no === null) ? $this->id : $this->reference_no;
    }

    public function getBaseEarnSalesAttribute() {
        return ($this->status != self::$status_complete) ? $this->customer->price : $this->base_earn;
    }

    public function getCurrentFuelCostAttribute() {
        if ($this->status == self::$status_complete) {
            return $this->fuel_cost;
        }

        $fuel_expense = FuelExpense::currentFuelCost();

        return (isset($fuel_expense->cost)) ? ( ceil($this->kilometer) * $fuel_expense->cost) + ( 20 * $fuel_expense->cost ) : 0;
    }

    public function getTotalVatAttribute() {
        return ($this->with_vat) ? $this->getBaseEarnSalesAttribute() * $this->vat_percent : 0.00;
    }

    public function getVatPercentageLabelAttribute(){
        return round((float) $this->vat_percent * 100 ) . '%';
    }

    public function getTotalEarnAttribute() {
        return $this->getBaseEarnSalesAttribute()
            - $this->getCurrentFuelCostAttribute()
            + $this->getTotalVatAttribute()
            + $this->transaction_expenses()->allReflectOnSales()->sum('cost')
            - $this->transaction_expenses()->allExpenseSales()->sum('cost');
    }

    public function payroll_users() {
        return $this->hasMany('App\PayrollUsers');
    }

    public function getCreatedDateAttribute() {
        return Carbon::parse($this->created_at)->format('m/d/Y');
    }
    public function getUpdatedDateAttribute() {
        return Carbon::parse($this->updated_at)->format('m/d/Y');
    }
}
