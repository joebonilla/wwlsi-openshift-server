<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class ContainerType extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'description',
        'payloads',
        'cubic_capacity',
        'interior_length',
        'interior_width',
        'interior_height'
    ];
}
