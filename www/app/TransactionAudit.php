<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionAudit extends Model
{
    protected $fillable = ['audit_id', 'audit_type'];
}
