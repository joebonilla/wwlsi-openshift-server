<?php
/**
 * Created by PhpStorm.
 * User: Jehart
 * Date: 4/11/2017
 * Time: 8:11 PM
 */

namespace App\Repositories;


use App\Truck;
use App\User;

class TransactionReportRepositories
{
    public function getTemplateHeaders(){
        return [
            'Truck ID' => '',
            'Driver ID' => '',
            'Helper ID' => '',
            'Client ID' => '',
            'Customer ID' => '',
            'Port ID' => '',
            'Transport Type ID' => '',
            'Container' => '',
            'With Pre-advice' => '',
            'With Vat' => '',
            'Vat Percent' => '',
            'Note' => '',
        ];
    }

    public function getAvailableTrucksTemplate(){
        $available_trucks = [];
        $trucks = Truck::available();

        foreach ($trucks as $truck){
            $available_trucks[] = [
                'ID' => $truck->id,
                'Name' => $truck->name,
                'Plate No.' => $truck->plate_no,
            ];
        }

        return $available_trucks;
    }

    public function getAvailableDriversTemplate(){
        $available_drivers = [];
        $drivers = User::getAvailableDriver();

        foreach ($drivers as $driver){
            $available_drivers[] = [
                'ID' => $driver->id,
                'Name' => $driver->name,
            ];
        }

        return $available_drivers;
    }

    public function getAvailableHelpersTemplate(){
        $available_helpers = [];
        $helpers = User::getAvailableHelper();

        foreach ($helpers as $helper){
            $available_helpers[] = [
                'ID' => $helper->id,
                'Name' => $helper->name,
            ];
        }

        return $available_helpers;
    }
}