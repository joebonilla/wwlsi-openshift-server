<?php

namespace App\Repositories;

use App\Chassis;
use App\TransactionChassis;

class ChassisRepository{

	public function getAvailableChassis(){
		$on_use_chassis_ids = TransactionChassis::where('status', TransactionChassis::ON_USE)->select('chassis_id')->get()->toArray();

		return Chassis::whereNotIn('id', $on_use_chassis_ids)->get();
	}

	public function getAvailableAndCurrChassis($transaction_id){
		$on_use_chassis_ids = TransactionChassis::where('transaction_id', '!=', $transaction_id)->where('status', TransactionChassis::ON_USE)->select('chassis_id')->get()->toArray();

		return Chassis::whereNotIn('id', $on_use_chassis_ids)->get();
	}

	public function getUnavailableChassis(){
		$on_use_chassis_ids = TransactionChassis::where('status', TransactionChassis::ON_USE)->select('chassis_id')->get()->toArray();

		return Chassis::whereIn('id', $on_use_chassis_ids)->get();
	}
}