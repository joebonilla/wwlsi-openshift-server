<?php

namespace App\Repositories;

use App\FuelExpense;
use App\Transaction;
use App\TransactionAudit;
use App\Expense;
use App\Truck;
use App\User;
use App\Audit;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class TransactionRepository
{

    public function getTransactionAnalyticsData() {
        $analytics_data['total_transactions_count'] = Transaction::all()->count();
        $analytics_data['total_ongoing_transactions_count'] = Transaction::ongoing()->count();
        $analytics_data['total_completed_transactions_count'] = Transaction::completed()->thisMonth()->count();
        $analytics_data['current_fuel_expense'] = FuelExpense::currentFuelCost();

        return $analytics_data;
    }

    public function calculateKilometers($origin, $destination) {
        $origin = urlencode($origin);
        $destination = urlencode($destination);
        $app_key = 'AIzaSyCpTrUsEPVa4xnMQGVPBqwqa5yEtkIbqoA';

        $map = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $origin . '&destinations=' . $destination . '&mode=driving&key=' . $app_key));

        return $map->rows[0]->elements[0]->distance->value / 1000;
    }

    public function updateTransactionContainer($request, $transaction) {
        if ($transaction->isFCL()) {
            Validator::make($request->all(), [
                'container_ref_id' => 'required',
                'container_type_id' => 'required',
                'chassis_id' => 'required',
            ])->validate();

            if ($transaction->container) {
                $transaction->container()->update([
                    'container_type_id' => $request->container_type_id,
                    'container_ref_id' => $request->container_ref_id
                ]);
                $containerId = $transaction->container->id;

            } else {
                $containerData = $transaction->container()->create($request->all());
                $containerId = $containerData->id;
            }

            $audit = new TransactionAudit;
            $audit->transaction_id = $transaction->id;
            $audit->audit_id = $containerId;
            $audit->audit_type = 'App\TransactionContainer';
            $audit->save();

            if ($transaction->transaction_chassis) {
                $transaction->transaction_chassis()->update([
                    'chassis_id' => $request->chassis_id
                ]);
            } else {
                $transaction->transaction_chassis()->create([
                    'chassis_id' => $request->chassis_id
                ]);
            }

            $transaction_chassis_id = $transaction->transaction_chassis->id;

            $audit = new TransactionAudit;
            $audit->transaction_id = $transaction->id;
            $audit->audit_id = $transaction_chassis_id;
            $audit->audit_type = 'App\TransactionChassis';
            $audit->save();
        } else {
            if ($transaction->container) {
                $transaction->container->delete();
            }

            if ($transaction->transaction_chassis) {
                $transaction->transaction_chassis->delete();
            }
        }
    }

    public function addDefaultTransactionExpense($transaction) {
        $expenses = Expense::default()->get()->values();

        foreach ($expenses as $expense) {
            $expense->note = $expense->description;
            $transaction->transaction_expenses()->create($expense->toArray());
        }
    }

    public function updateUtilitiesStatusToOngoing($request) {
        $truck_input = ['truck_status_id' => Truck::$status_on_going];
        $driver_input = ['user_status_id' => User::$status_on_going];
        $helper_input = ['user_status_id' => User::$status_on_going];

        Truck::find($request->input('truck_id'))->update($truck_input);
        User::find($request->input('driver_id'))->update($driver_input);
        User::find($request->input('helper_id'))->update($helper_input);
    }

    public function updateUtilitiesStatusToAvailable($transaction) {
        $truck_input = ['truck_status_id' => Truck::$status_available];
        $driver_input = ['user_status_id' => User::$status_available];
        $helper_input = ['user_status_id' => User::$status_available];

        Truck::find($transaction->truck_id)->update($truck_input);
        User::find($transaction->driver_id)->update($driver_input);
        User::find($transaction->helper_id)->update($helper_input);
    }

    public function getTransactionAudits($transaction) {
        $audit = Audit::where('auditable_type', 'App\Transaction')
            ->where('auditable_id', $transaction->id);

        if ($transaction->transaction_notes) {
            $audit->orWhere('auditable_type', 'App\TransactionNote')
                ->whereIn('auditable_id', $transaction->transaction_notes->pluck('id'));
        }

        if ($transaction->transaction_audits) {
            $audit->orWhere('auditable_type', 'App\TransactionContainer')
                ->whereIn('auditable_id', $transaction->transaction_audits()
                    ->where('audit_type', 'App\TransactionContainer')->pluck('audit_id'));

            $audit->orWhere('auditable_type', 'App\TransactionExpense')
                ->whereIn('auditable_id', $transaction->transaction_audits()
                    ->where('audit_type', 'App\TransactionExpense')->pluck('audit_id'));

            $audit->orWhere('auditable_type', 'App\TransactionChassis')
                ->whereIn('auditable_id', $transaction->transaction_audits()
                    ->where('audit_type', 'App\TransactionChassis')->pluck('audit_id'));
        }

        if ($transaction->transaction_expenses) {
            $audit->orWhere('auditable_type', 'App\TransactionExpense')
                ->whereIn('auditable_id', $transaction->transaction_expenses->pluck('id'));
        }

        //dd($audit->orderBy('created_at', 'desc')->get());
        return $audit->orderBy('created_at', 'desc')->get();
    }

    public function dataTableBuildOnGoing($query) { 
        return Datatables::of($query)
            ->editColumn('id', function($query){
                return '<a href="' . route('transactions.show', $query->id) . '">' . $query->ref_no . '</a>';
            })
            ->editColumn('type', function ($query) {
                return $this->transformType($query);
            })
            ->editColumn('status', function ($query) {
                return $this->transformStatus($query);
            })
            ->editColumn('created_at', function ($query) {
//                return $this->transformCreatedAt($query);
                return $query->created_at ? with(new Carbon($query->created_at))->format('m/d/Y') : '';
            })
            ->editColumn('container', function ($query) {
                return $this->containerName($query);
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%m/%d/%Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('type', function ($query, $keyword) {
                if ($keyword == 'LCL') {
                    $keyword = Transaction::LCL;
                } else {
                    $keyword = Transaction::FCL;
                }
                $query->where('type', $keyword);
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function dataTableBuildComplete($query, $start_search, $end_search) {
        $datatables = Datatables::of($query)
            ->editColumn('id', function($query){
                return '<a href="' . route('transactions.show', $query->id) . '">' . $query->ref_no . '</a>';
            })
            ->editColumn('type', function ($query) {
                return $this->transformType($query);
            })
            ->editColumn('status', function ($query) {
                return $this->transformStatus($query);
            })
            ->editColumn('date_completed', function ($query) {
                return $query->date_completed ? with(new Carbon($query->date_completed))->format('m/d/Y') : '';
            })
            ->editColumn('container', function ($query) {
                return $this->containerName($query);
            })
            ->filterColumn('date_completed', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(date_completed,'%m/%d/%Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('type', function ($query, $keyword) {
                if ($keyword == 'LCL') {
                    $keyword = Transaction::LCL;
                } else {
                    $keyword = Transaction::FCL;
                }
                $query->where('type', $keyword);
            })
            ->escapeColumns([]);

        return $datatables->make(true);
    }

    public function containerName($query) {
        return ($query->container == null) ? 'N/A' : $query->container->container_ref_id . ' / ' . $query->container->containerType->name;
    }

    public function transformType($query) {
        return ($query->type == Transaction::LCL) ? 'LCL' : 'FCL';
    }

    public function transformStatus($query) {
        $status_label = '';
        switch ($query->status) {
            case Transaction::$status_new:
                $status_label = 'New';
                break;
            case Transaction::$status_on_going:
                $status_label = 'On going';
                break;
            case Transaction::$status_complete:
                $status_label = 'Complete';
                break;
        }
        return $status_label;
    }

    public function transformCreatedAt($query) {
        return Carbon::parse($query->created_at)->format('m/d/Y');
    }

    public function generateReferenceNo(){
        $today = Carbon::now();
        $year = $today->year;
        $month = str_pad($today->month, 2, 0, STR_PAD_LEFT);
        $day = str_pad($today->day, 2, 0, STR_PAD_LEFT);

        $latest_transaction_count = Transaction::whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->count();

        if($latest_transaction_count <= 0){
            return $year . $month . $day . '01';
        } else {
            return $year . $month . $day . str_pad($latest_transaction_count + 1, 2, 0, STR_PAD_LEFT);
        }
    }
}