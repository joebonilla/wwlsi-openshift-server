<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class TransactionContainer extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = ['container_ref_id', 'container_type_id'];

    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }

    public function containerType(){
        return $this->belongsTo('App\ContainerType');
    }

}
