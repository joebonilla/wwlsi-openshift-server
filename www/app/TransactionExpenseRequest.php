<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionExpenseRequest extends Model
{
    protected $fillable = ['transaction_expense_id', 'cost'];
}
