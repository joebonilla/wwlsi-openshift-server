<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;

class Client extends Model
{
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];

    public function address() {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function customers() {
        return $this->hasMany('App\Customer');
    }

    public function transactions() {
        return $this->hasMany('App\Transaction');
    }

}
